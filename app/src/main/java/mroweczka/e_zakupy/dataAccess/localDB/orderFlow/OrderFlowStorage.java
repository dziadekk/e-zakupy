package mroweczka.e_zakupy.dataAccess.localDB.orderFlow;

import java.util.List;

import mroweczka.e_zakupy.entity.orderFlowModels.OrderFlow;
import mroweczka.e_zakupy.entity.orderFlowModels.OrderFlowItem;

public interface OrderFlowStorage {
    List<OrderFlowItem> getOrderFlowItems();

    boolean saveOrderFlowItem(OrderFlowItem orderFlowItem);

    OrderFlow getOrderFlow();

    boolean saveOrderFlow(OrderFlow orderFlow);

    void clearOrderFlow();
}

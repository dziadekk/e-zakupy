package mroweczka.e_zakupy.dataAccess.localDB.orderFlow;

import android.database.sqlite.SQLiteDatabase;

import mroweczka.e_zakupy.dataAccess.localDB.LocalDbFactory;

public class OrderFlowItemFactory implements LocalDbFactory {
    public static final String TABLE_NAME = "OrderItems";
    public static final String KEY_PRODUCT_ID = "key_product_id";
    public static final String KEY_PRODUCT_CATEGORY_ID = "key_product_category_id";
    public static final String KEY_QUANTITY = "key_quantity";
    public static final String KEY_PRICE = "key_price";

    private static final String CREATE_TABLE =
            String.format("CREATE TABLE %s (" +
                            "%s INTEGER, " +
                            "%s INTEGER, " +
                            "%s DOUBLE, " +
                            "%s DOUBLE, " +
                            "PRIMARY KEY (%s))",
                    TABLE_NAME,
                    KEY_PRODUCT_ID,
                    KEY_PRODUCT_CATEGORY_ID,
                    KEY_QUANTITY,
                    KEY_PRICE,
                    KEY_PRODUCT_ID
            );

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpdate(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }
}

package mroweczka.e_zakupy.dataAccess.localDB.orderFlow;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import mroweczka.e_zakupy.app.di.qualifier.LocalDB;
import mroweczka.e_zakupy.dataAccess.DBAccess;
import mroweczka.e_zakupy.entity.orderFlowModels.OrderFlow;
import mroweczka.e_zakupy.entity.orderFlowModels.OrderFlowItem;
import mroweczka.e_zakupy.entity.orderFlowModels.OrderFlowOrderType;
import mroweczka.e_zakupy.utils.DateUtils;

public class OrderFlowLocalDbInteractor implements OrderFlowStorage {
    private DBAccess localDBAccess;

    @Inject
    public OrderFlowLocalDbInteractor(@LocalDB DBAccess localDBAccess) {
        this.localDBAccess = localDBAccess;
    }

    @Override
    public boolean saveOrderFlowItem(OrderFlowItem orderFlowItem) {
        SQLiteDatabase db = localDBAccess.getWritableDatabase();
        //override existing item
        db.delete(OrderFlowItemFactory.TABLE_NAME,
                String.format("%s=%d",
                        OrderFlowItemFactory.KEY_PRODUCT_ID, orderFlowItem.getProductId()),
                null);

        ContentValues values = new ContentValues();
        values.put(OrderFlowItemFactory.KEY_PRODUCT_ID, orderFlowItem.getProductId());
        values.put(OrderFlowItemFactory.KEY_PRODUCT_CATEGORY_ID, orderFlowItem.getProductCategoryId());
        values.put(OrderFlowItemFactory.KEY_QUANTITY, orderFlowItem.getQuantity());
        values.put(OrderFlowItemFactory.KEY_PRICE, orderFlowItem.getPrice());
        db.insert(OrderFlowItemFactory.TABLE_NAME, null, values);

        db.close();
        return true;
    }

    @Override
    public List<OrderFlowItem> getOrderFlowItems() {
        SQLiteDatabase db = localDBAccess.getReadableDatabase();

        String query = String.format("SELECT * FROM %s", OrderFlowItemFactory.TABLE_NAME);

        Cursor cursor = db.rawQuery(query, null);
        List<OrderFlowItem> orderItems = new ArrayList<OrderFlowItem>();

        if (cursor.moveToFirst()) {
            do {
                OrderFlowItem orderFlowItem = new OrderFlowItem(
                        cursor.getInt(0),
                        cursor.getInt(1),
                        cursor.getDouble(2),
                        cursor.getDouble(3));
                orderItems.add(orderFlowItem);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return orderItems;
    }

    /**
     * Save OrderFlow to db, and delete the existing one.
     *
     * @param orderFlow
     * @return true if operation successfull
     */
    @Override
    public boolean saveOrderFlow(OrderFlow orderFlow) {
        SQLiteDatabase db = localDBAccess.getWritableDatabase();
        db.delete(OrderFlowFactory.TABLE_NAME, null, null);

        ContentValues values = new ContentValues();
        values.put(OrderFlowFactory.KEY_ID, orderFlow.getOrderId());
        values.put(OrderFlowFactory.KEY_PRICE, orderFlow.getPrice());

        Integer facilityId = orderFlow.getFacilityId();
        if (facilityId != null) {
            values.put(OrderFlowFactory.KEY_FACILITY_ID, facilityId);
        }

        Integer discountId = orderFlow.getDiscountId();
        if (discountId != null) {
            values.put(OrderFlowFactory.KEY_DISCOUNT_ID, discountId);
        }

        OrderFlowOrderType orderFlowOrderType = orderFlow.getOrderFlowOrderType();
        if (orderFlowOrderType != null) {
            values.put(OrderFlowFactory.KEY_ORDER_TYPE,
                    orderFlowOrderType.getOrderType().getValue());
            if (orderFlowOrderType.getRequestedCollectDate() != null) {
                values.put(OrderFlowFactory.KEY_REQUEST_COLLECT_DATE,
                        orderFlowOrderType.getRequestedCollectDate().toString(DateUtils.COMMON_DATE_TIME_PATTERN));
            }
        }

        Integer paymentId = orderFlow.getPaymentId();
        if (paymentId != null) {
            values.put(OrderFlowFactory.KEY_PAYMENT_ID, paymentId);
        }

        db.insert(OrderFlowFactory.TABLE_NAME, null, values);
        db.close();
        return true;
    }

    @Override
    public OrderFlow getOrderFlow() {
        SQLiteDatabase db = localDBAccess.getReadableDatabase();

        String query = String.format("SELECT * FROM %s", OrderFlowFactory.TABLE_NAME);

        Cursor cursor = db.rawQuery(query, null);
        OrderFlow orderFlow = new OrderFlow();

        if (cursor.moveToFirst()) {
            if (!cursor.isNull(0)) {
                orderFlow.setOrderId(cursor.getInt(0));
                if (!cursor.isNull(1)) {
                    orderFlow.setFacilityId(cursor.getInt(1));
                    if (!cursor.isNull(2)) {
                        if (!cursor.isNull(3)) {
                            orderFlow.setOrderFlowOrderType(
                                    new OrderFlowOrderType(DateUtils.getDateTimeInCommonFormat(cursor.getString(3))));
                        } else {
                            orderFlow.setOrderFlowOrderType(new OrderFlowOrderType(null));
                        }
                        if (!cursor.isNull(4)) {
                            orderFlow.setDiscountId(cursor.getInt(4));
                            if (!cursor.isNull(5)) {
                                orderFlow.setPaymentId(cursor.getInt(5));
                            }
                        }

                    }
                }
            }
            orderFlow.setPrice(cursor.getDouble(6));
        }
        orderFlow.setOrderFlowItems(getOrderFlowItems());

        cursor.close();
        db.close();

        return orderFlow;
    }

    @Override
    public void clearOrderFlow() {
        SQLiteDatabase db = localDBAccess.getWritableDatabase();
        db.delete(OrderFlowFactory.TABLE_NAME, null, null);
        db.delete(OrderFlowItemFactory.TABLE_NAME, null, null);
        db.close();
    }
}

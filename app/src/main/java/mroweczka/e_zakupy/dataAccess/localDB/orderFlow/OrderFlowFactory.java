package mroweczka.e_zakupy.dataAccess.localDB.orderFlow;

import android.database.sqlite.SQLiteDatabase;

import mroweczka.e_zakupy.dataAccess.localDB.LocalDbFactory;

public class OrderFlowFactory implements LocalDbFactory {
    public static final String TABLE_NAME = "Orders";
    public static final String KEY_ID = "key_order_id";
    public static final String KEY_FACILITY_ID = "key_facility_id";
    public static final String KEY_ORDER_TYPE = "key_order_type";
    public static final String KEY_REQUEST_COLLECT_DATE = "key_request_collect_date";
    public static final String KEY_DISCOUNT_ID = "key_discount_id";
    public static final String KEY_PAYMENT_ID = "key_payment_id";
    public static final String KEY_PRICE = "key_price";

    private static final String CREATE_TABLE =
            String.format("CREATE TABLE %s (" +
                            "%s INTEGER, " +
                            "%s INTEGER, " +
                            "%s INTEGER, " +
                            "%s TEXT, " +
                            "%s INTEGER, " +
                            "%s INTEGER, " +
                            "%s DOUBLE, " +
                            "PRIMARY KEY (%s))",
                    TABLE_NAME,
                    KEY_ID,
                    KEY_FACILITY_ID,
                    KEY_ORDER_TYPE,
                    KEY_REQUEST_COLLECT_DATE,
                    KEY_DISCOUNT_ID,
                    KEY_PAYMENT_ID,
                    KEY_PRICE,
                    KEY_ID
            );

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpdate(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }
}

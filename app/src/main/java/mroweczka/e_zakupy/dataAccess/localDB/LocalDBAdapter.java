package mroweczka.e_zakupy.dataAccess.localDB;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import mroweczka.e_zakupy.dataAccess.DBAccess;
import mroweczka.e_zakupy.dataAccess.localDB.orderFlow.OrderFlowFactory;
import mroweczka.e_zakupy.dataAccess.localDB.orderFlow.OrderFlowItemFactory;

public class LocalDBAdapter extends SQLiteOpenHelper implements DBAccess {

    private static final String TAG = LocalDBAdapter.class.getSimpleName();

    private static final int DATABASE_VERSION = 12;
    private static final String DATABASE_NAME = "e-zakupy-local-db";

    public LocalDBAdapter(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @NonNull
    private static List<LocalDbFactory> getTablesToCreate() {
        List<LocalDbFactory> servicesToCreate = new ArrayList<>();
        servicesToCreate.add(new OrderFlowFactory());
        servicesToCreate.add(new OrderFlowItemFactory());
        return servicesToCreate;
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            //db.execSQL("PRAGMA foreign_keys = ON;");
        }
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        onCreateTables(sqLiteDatabase, getTablesToCreate());
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        List<LocalDbFactory> tablesToCreate = getTablesToCreate();
        for (mroweczka.e_zakupy.dataAccess.localDB.LocalDbFactory localDbFactory : tablesToCreate) {
            localDbFactory.onUpdate(sqLiteDatabase);
        }

        onCreateTables(sqLiteDatabase, tablesToCreate);
    }

    private void onCreateTables(SQLiteDatabase sqLiteDatabase, List<LocalDbFactory> tablesToCreate) {
        for (mroweczka.e_zakupy.dataAccess.localDB.LocalDbFactory localDbFactory : tablesToCreate) {
            localDbFactory.onCreate(sqLiteDatabase);
        }
    }

}
package mroweczka.e_zakupy.dataAccess.localDB;


import android.database.sqlite.SQLiteDatabase;

public interface LocalDbFactory {
    void onCreate(SQLiteDatabase db);

    void onUpdate(SQLiteDatabase db);
}

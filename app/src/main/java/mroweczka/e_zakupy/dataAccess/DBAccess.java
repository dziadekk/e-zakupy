package mroweczka.e_zakupy.dataAccess;

import android.database.sqlite.SQLiteDatabase;

public interface DBAccess {
    SQLiteDatabase getReadableDatabase();

    SQLiteDatabase getWritableDatabase();
}

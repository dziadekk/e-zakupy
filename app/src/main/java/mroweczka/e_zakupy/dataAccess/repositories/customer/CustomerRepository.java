package mroweczka.e_zakupy.dataAccess.repositories.customer;

import java.util.List;

import javax.inject.Inject;

import mroweczka.e_zakupy.dataAccess.api.customer.CustomerService;
import mroweczka.e_zakupy.dataAccess.localDB.orderFlow.OrderFlowStorage;

public class CustomerRepository {
    public static final int CURRENT_CUSTOMER_ID = 1;
    private static final String TAG = CustomerRepository.class.getSimpleName();
    private CustomerService customerService;

    private OrderFlowStorage orderFlowStorage;
    private List<Integer> savedCustomers;

    @Inject
    public CustomerRepository(CustomerService customerService, OrderFlowStorage orderFlowStorage) {
        this.orderFlowStorage = orderFlowStorage;
        this.customerService = customerService;
    }

    public List<Integer> downloadCustomers() {
        if (savedCustomers == null) {
            savedCustomers = customerService.getCustomers();
        }
        return savedCustomers;
    }

}

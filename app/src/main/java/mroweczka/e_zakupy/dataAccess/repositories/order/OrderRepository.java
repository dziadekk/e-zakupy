package mroweczka.e_zakupy.dataAccess.repositories.order;


import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import mroweczka.e_zakupy.dataAccess.api.order.OrderService;
import mroweczka.e_zakupy.dataAccess.api.payment.PaymentService;
import mroweczka.e_zakupy.dataAccess.localDB.orderFlow.OrderFlowStorage;
import mroweczka.e_zakupy.entity.Discount;
import mroweczka.e_zakupy.entity.Facility;
import mroweczka.e_zakupy.entity.Order;
import mroweczka.e_zakupy.entity.OrderItem;
import mroweczka.e_zakupy.entity.Payment;
import mroweczka.e_zakupy.entity.Product;
import mroweczka.e_zakupy.entity.orderFlowModels.OrderFlow;
import mroweczka.e_zakupy.entity.orderFlowModels.OrderFlowItem;

public class OrderRepository {
    private static final String TAG = OrderRepository.class.getSimpleName();

    private OrderFlowStorage orderFlowStorage;
    private OrderService orderService;
    private PaymentService paymentService;

    @Inject
    public OrderRepository(OrderFlowStorage orderFlowStorage, OrderService orderService, PaymentService paymentService) {
        this.orderFlowStorage = orderFlowStorage;
        this.orderService = orderService;
        this.paymentService = paymentService;
    }

    public boolean saveOrderFlowItem(OrderFlowItem orderFlowItem) {
        return orderFlowStorage.saveOrderFlowItem(orderFlowItem);
    }

    public List<OrderFlowItem> getOrderFlowItems() {
        return orderFlowStorage.getOrderFlowItems();
    }

    public boolean saveOrderFlow(OrderFlow orderFlow) {
        return orderFlowStorage.saveOrderFlow(orderFlow);
    }

    public int getMaksId() {
        return orderService.getMaksId();
    }

    public OrderFlow getOrderFlow() {
        return orderFlowStorage.getOrderFlow();
    }

    public Order getOrder(int orderId) {
        return orderService.getOrder(orderId);
    }

    public boolean submitOrder(OrderFlow orderFlow) {
        return orderService.submitOrder(mapOrderFlowToOrder(orderFlow));
    }


    private Order mapOrderFlowToOrder(OrderFlow orderFlow) {
        return new Order(orderFlow.getOrderId(),
                orderFlow.getPrice(),
                LocalDate.now(),
                orderFlow.getOrderFlowOrderType().getRequestedCollectDate(),
                null,
                -1,
                orderFlow.getOrderFlowOrderType().getOrderType(),
                Order.StateOfOrder.WaitingForRealisation,
                mapOrderFlowItemsToOrderItem(orderFlow.getOrderId(), orderFlow.getOrderFlowItems()),
                new Discount(orderFlow.getDiscountId()),
                new Payment(orderFlow.getPaymentId()),
                new Facility(orderFlow.getFacilityId()));
    }

    private List<OrderItem> mapOrderFlowItemsToOrderItem(int orderId, List<OrderFlowItem> orderFlowItems) {
        List<OrderItem> orderItems = new ArrayList<>(orderFlowItems.size());
        for (OrderFlowItem orderFlowItem : orderFlowItems) {
            orderItems.add(new OrderItem(orderId,
                    new Product(orderFlowItem.getProductId()),
                    orderFlowItem.getQuantity(),
                    orderFlowItem.getPrice()));
        }

        return orderItems;
    }

    public int submitPayment(Payment payment) {
        return paymentService.submitPayment(payment);
    }

    public void clearOrderFlow() {
        orderFlowStorage.clearOrderFlow();
    }

    public boolean createEmtyOrder(int id) {
        return orderService.createEmtyOrder(id);
    }

}
package mroweczka.e_zakupy.dataAccess.repositories.facility;


import android.support.annotation.Nullable;

import java.util.List;

import javax.inject.Inject;

import mroweczka.e_zakupy.dataAccess.api.facility.FacilityService;
import mroweczka.e_zakupy.dataAccess.localDB.orderFlow.OrderFlowStorage;
import mroweczka.e_zakupy.entity.Facility;

public class FacilityRepository {
    private static final String TAG = FacilityRepository.class.getSimpleName();

    private FacilityService facilityService;
    private OrderFlowStorage orderFlowStorage;

    private List<Facility> savedFacilities;

    @Inject
    public FacilityRepository(FacilityService facilityService, OrderFlowStorage orderFlowStorage) {
        this.orderFlowStorage = orderFlowStorage;
        this.facilityService = facilityService;
    }

    public List<Facility> downloadFacilities() {
        if (savedFacilities == null) {
            savedFacilities = facilityService.getFacilities();
        }
        return savedFacilities;
    }

    @Nullable
    public Facility getFacility(int facilityId) {
        List<Facility> facilities = downloadFacilities();
        for (Facility facility : facilities) {
            if (facilityId == facility.getId()) {
                return facility;
            }
        }
        return null;
    }
}

package mroweczka.e_zakupy.dataAccess.repositories.storage;

import android.support.annotation.NonNull;
import android.util.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import mroweczka.e_zakupy.dataAccess.api.storage.StorageService;
import mroweczka.e_zakupy.dataAccess.localDB.orderFlow.OrderFlowStorage;
import mroweczka.e_zakupy.entity.orderFlowModels.OrderFlowItem;

public class StorageRepository {
    private static final String TAG = StorageRepository.class.getSimpleName();

    private StorageService storageService;

    private OrderFlowStorage orderFlowStorage;

    @Inject
    public StorageRepository(StorageService storageService, OrderFlowStorage orderFlowStorage) {
        this.storageService = storageService;
        this.orderFlowStorage = orderFlowStorage;
    }

    /**
     * Maps the List<OrderFlowItems> -> List<productID>
     * and sends request to the service to check the availability of the products in the storage
     *
     * @param facilityNumber
     * @param orderItems
     * @return
     */
    public Map<Integer, Double> getProductsAvailability(int facilityNumber, List<OrderFlowItem> orderItems) {
        List<Integer> productsId = new ArrayList<>(orderItems.size());
        for (OrderFlowItem orderItem : orderItems) {
            productsId.add(orderItem.getProductId());
        }
        return storageService.getProductsAvailability(facilityNumber, productsId);
    }

    /**
     * Maps the List<Category> -> List<Pair<productId,quantity>>
     * and sends request to the service to lock the items in the storage
     *
     * @param facilityNumber
     * @param orderItems
     * @return true if operation was done successfully
     */
    public boolean lockOrderItemsInStorage(int facilityNumber, List<OrderFlowItem> orderItems) {
        return storageService.lockItems(facilityNumber, mapOrderFlowItemsForStorage(orderItems));
    }

    /**
     * Maps the List<Category> -> List<Pair<productId,quantity>>
     * and sends request to the service to lock the items in the storage
     *
     * @param facilityNumber
     * @param orderItems
     * @return true if operation was done successfully
     */
    public boolean unlockOrderItemsFromStorage(int facilityNumber, List<OrderFlowItem> orderItems) {
        return storageService.unlockItems(facilityNumber, mapOrderFlowItemsForStorage(orderItems));
    }

    /**
     * Maps the List<Category> -> List<Pair<productId,quantity>> that is required by the ServiceStorage
     *
     * @param orderItems
     * @return
     */
    @NonNull
    private List<Pair<Integer, Double>> mapOrderFlowItemsForStorage(List<OrderFlowItem> orderItems) {
        List<Pair<Integer, Double>> products = new ArrayList<>(orderItems.size());
        for (OrderFlowItem orderItem : orderItems) {
            products.add(new Pair<Integer, Double>(orderItem.getProductId(), orderItem.getQuantity()));
        }
        return products;
    }


}

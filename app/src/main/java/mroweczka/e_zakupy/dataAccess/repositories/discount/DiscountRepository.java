package mroweczka.e_zakupy.dataAccess.repositories.discount;


import java.util.List;

import javax.inject.Inject;

import mroweczka.e_zakupy.dataAccess.api.discount.DiscountService;
import mroweczka.e_zakupy.dataAccess.localDB.orderFlow.OrderFlowStorage;
import mroweczka.e_zakupy.dataAccess.repositories.customer.CustomerRepository;
import mroweczka.e_zakupy.entity.Discount;

public class DiscountRepository {
    private static final String TAG = DiscountRepository.class.getSimpleName();

    private DiscountService discountService;
    private OrderFlowStorage orderFlowStorage;

    @Inject
    public DiscountRepository(DiscountService discountService, OrderFlowStorage orderFlowStorage) {
        this.discountService = discountService;
        this.orderFlowStorage = orderFlowStorage;
    }

    public List<Discount> getDiscountsForCustomer() {
        return discountService.getDiscounts(CustomerRepository.CURRENT_CUSTOMER_ID);
    }
}
package mroweczka.e_zakupy.dataAccess.repositories.catalog;

import java.util.List;

import javax.inject.Inject;

import mroweczka.e_zakupy.dataAccess.api.catalog.CatalogService;
import mroweczka.e_zakupy.dataAccess.localDB.orderFlow.OrderFlowStorage;
import mroweczka.e_zakupy.entity.Category;
import mroweczka.e_zakupy.entity.Product;

public class CatalogRepository {
    private static final String TAG = CatalogRepository.class.getSimpleName();
    private CatalogService catalogService;

    private OrderFlowStorage orderFlowStorage;
    private List<Category> savedCategories;

    @Inject
    public CatalogRepository(CatalogService catalogService, OrderFlowStorage orderFlowStorage) {
        this.orderFlowStorage = orderFlowStorage;
        this.catalogService = catalogService;
    }

    public List<Category> downloadCategories() {
        if (savedCategories == null) {
            savedCategories = catalogService.getCategories();
        }
        return savedCategories;
    }

    public List<Product> downloadProductsCategory(int cateogryId) {
        return catalogService.getProductsCategory(cateogryId);
    }

    public Product downloadProduct(int productId) {
        return catalogService.getProduct(productId);
    }

}

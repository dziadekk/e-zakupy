package mroweczka.e_zakupy.dataAccess.api.facility;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import mroweczka.e_zakupy.dataAccess.api.ServiceFactory;
import mroweczka.e_zakupy.entity.WorkDay;
import mroweczka.e_zakupy.utils.DateUtils;


public class WorkDayFactory implements ServiceFactory {
    public static final String TABLE_NAME = "WorkDays";
    public static final String KEY_ID = "key_workday_id";
    public static final String KEY_OPEN_TIME = "key_open_time";
    public static final String KEY_CLOSE_TIME = "key_close_time";
    public static final String KEY_WEEK_DAY = "key_week_day";
    public static final String KEY_FACILITY_ID = "key_facility_id";

    private static final String CREATE_TABLE =
            String.format("CREATE TABLE %s (" +
                            "%s INTEGER, " +
                            "%s TEXT, " +
                            "%s TEXT, " +
                            "%s INTEGER," +
                            "%s INTEGER," +
                            "FOREIGN KEY (%s) REFERENCES %s(%s) ON DELETE CASCADE, " +
                            "PRIMARY KEY (%s))",
                    TABLE_NAME,
                    KEY_ID,
                    KEY_OPEN_TIME,
                    KEY_CLOSE_TIME,
                    KEY_WEEK_DAY,
                    KEY_FACILITY_ID,
                    KEY_FACILITY_ID,
                    FacilitySyncDBInteractor.TABLE_NAME,
                    FacilitySyncDBInteractor.KEY_ID,
                    KEY_ID
            );

    private static WorkDay[] WORKDAYS = new WorkDay[]{
            new WorkDay(0, DateUtils.getTimeInCommonFormat("08:00"), DateUtils.getTimeInCommonFormat("22:00"), WorkDay.WeekDay.Monday),
            new WorkDay(1, DateUtils.getTimeInCommonFormat("08:00"), DateUtils.getTimeInCommonFormat("22:00"), WorkDay.WeekDay.Tuesday),
            new WorkDay(2, DateUtils.getTimeInCommonFormat("08:00"), DateUtils.getTimeInCommonFormat("22:00"), WorkDay.WeekDay.Wednesday),
            new WorkDay(3, DateUtils.getTimeInCommonFormat("08:00"), DateUtils.getTimeInCommonFormat("22:00"), WorkDay.WeekDay.Thursday),
            new WorkDay(4, DateUtils.getTimeInCommonFormat("08:00"), DateUtils.getTimeInCommonFormat("22:00"), WorkDay.WeekDay.Friday),
            new WorkDay(5, DateUtils.getTimeInCommonFormat("09:00"), DateUtils.getTimeInCommonFormat("21:00"), WorkDay.WeekDay.Saturday),
            new WorkDay(6, DateUtils.getTimeInCommonFormat("10:00"), DateUtils.getTimeInCommonFormat("20:00"), WorkDay.WeekDay.Sunday),
            new WorkDay(7, DateUtils.getTimeInCommonFormat("08:00"), DateUtils.getTimeInCommonFormat("22:00"), WorkDay.WeekDay.Monday),
            new WorkDay(8, DateUtils.getTimeInCommonFormat("08:00"), DateUtils.getTimeInCommonFormat("22:00"), WorkDay.WeekDay.Tuesday),
            new WorkDay(9, DateUtils.getTimeInCommonFormat("08:00"), DateUtils.getTimeInCommonFormat("22:00"), WorkDay.WeekDay.Wednesday),
            new WorkDay(10, DateUtils.getTimeInCommonFormat("08:00"), DateUtils.getTimeInCommonFormat("22:00"), WorkDay.WeekDay.Thursday),
            new WorkDay(11, DateUtils.getTimeInCommonFormat("08:00"), DateUtils.getTimeInCommonFormat("22:00"), WorkDay.WeekDay.Friday),
            new WorkDay(12, DateUtils.getTimeInCommonFormat("09:00"), DateUtils.getTimeInCommonFormat("21:00"), WorkDay.WeekDay.Saturday),
            new WorkDay(13, DateUtils.getTimeInCommonFormat("10:00"), DateUtils.getTimeInCommonFormat("20:00"), WorkDay.WeekDay.Sunday),
            new WorkDay(14, DateUtils.getTimeInCommonFormat("08:00"), DateUtils.getTimeInCommonFormat("22:00"), WorkDay.WeekDay.Monday),
            new WorkDay(15, DateUtils.getTimeInCommonFormat("08:00"), DateUtils.getTimeInCommonFormat("22:00"), WorkDay.WeekDay.Tuesday),
            new WorkDay(16, DateUtils.getTimeInCommonFormat("08:00"), DateUtils.getTimeInCommonFormat("22:00"), WorkDay.WeekDay.Wednesday),
            new WorkDay(17, DateUtils.getTimeInCommonFormat("08:00"), DateUtils.getTimeInCommonFormat("22:00"), WorkDay.WeekDay.Thursday),
            new WorkDay(18, DateUtils.getTimeInCommonFormat("08:00"), DateUtils.getTimeInCommonFormat("22:00"), WorkDay.WeekDay.Friday),
            new WorkDay(19, DateUtils.getTimeInCommonFormat("09:00"), DateUtils.getTimeInCommonFormat("21:00"), WorkDay.WeekDay.Saturday),
            new WorkDay(20, DateUtils.getTimeInCommonFormat("10:00"), DateUtils.getTimeInCommonFormat("20:00"), WorkDay.WeekDay.Sunday),
            new WorkDay(21, DateUtils.getTimeInCommonFormat("08:00"), DateUtils.getTimeInCommonFormat("22:00"), WorkDay.WeekDay.Monday),
            new WorkDay(22, DateUtils.getTimeInCommonFormat("08:00"), DateUtils.getTimeInCommonFormat("22:00"), WorkDay.WeekDay.Tuesday),
            new WorkDay(23, DateUtils.getTimeInCommonFormat("08:00"), DateUtils.getTimeInCommonFormat("22:00"), WorkDay.WeekDay.Wednesday),
            new WorkDay(24, DateUtils.getTimeInCommonFormat("08:00"), DateUtils.getTimeInCommonFormat("22:00"), WorkDay.WeekDay.Thursday),
            new WorkDay(25, DateUtils.getTimeInCommonFormat("08:00"), DateUtils.getTimeInCommonFormat("22:00"), WorkDay.WeekDay.Friday),
            new WorkDay(26, DateUtils.getTimeInCommonFormat("09:00"), DateUtils.getTimeInCommonFormat("21:00"), WorkDay.WeekDay.Saturday),
            new WorkDay(27, DateUtils.getTimeInCommonFormat("10:00"), DateUtils.getTimeInCommonFormat("20:00"), WorkDay.WeekDay.Sunday),
            new WorkDay(28, DateUtils.getTimeInCommonFormat("08:00"), DateUtils.getTimeInCommonFormat("22:00"), WorkDay.WeekDay.Monday),
            new WorkDay(29, DateUtils.getTimeInCommonFormat("08:00"), DateUtils.getTimeInCommonFormat("22:00"), WorkDay.WeekDay.Tuesday),
            new WorkDay(30, DateUtils.getTimeInCommonFormat("08:00"), DateUtils.getTimeInCommonFormat("22:00"), WorkDay.WeekDay.Wednesday),
            new WorkDay(31, DateUtils.getTimeInCommonFormat("08:00"), DateUtils.getTimeInCommonFormat("22:00"), WorkDay.WeekDay.Thursday),
            new WorkDay(32, DateUtils.getTimeInCommonFormat("08:00"), DateUtils.getTimeInCommonFormat("22:00"), WorkDay.WeekDay.Friday),
            new WorkDay(33, DateUtils.getTimeInCommonFormat("09:00"), DateUtils.getTimeInCommonFormat("21:00"), WorkDay.WeekDay.Saturday),
            new WorkDay(34, DateUtils.getTimeInCommonFormat("10:00"), DateUtils.getTimeInCommonFormat("20:00"), WorkDay.WeekDay.Sunday)
    };

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpdate(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }

    @Override
    public void prePopulateData(SQLiteDatabase db) {
        db.beginTransaction();

        ContentValues values = new ContentValues();


        for (WorkDay workDay : WORKDAYS) {
            values.put(KEY_ID, workDay.getWorkDayId());
            values.put(KEY_OPEN_TIME, workDay.getOpenTime().toString(DateUtils.COMMON_TIME_PATTERN));
            values.put(KEY_CLOSE_TIME, workDay.getCloseTime().toString(DateUtils.COMMON_TIME_PATTERN));
            values.put(KEY_WEEK_DAY, workDay.getWeekDay().getValue());
            values.put(KEY_FACILITY_ID, workDay.getWorkDayId() / 7 + 1);
            db.insert(TABLE_NAME, null, values);
            values.clear();
        }

        db.setTransactionSuccessful();
        db.endTransaction();
    }
}

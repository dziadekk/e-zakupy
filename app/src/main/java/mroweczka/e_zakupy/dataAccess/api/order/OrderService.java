package mroweczka.e_zakupy.dataAccess.api.order;

import java.util.List;

import mroweczka.e_zakupy.entity.Order;
import mroweczka.e_zakupy.entity.OrderItem;

public interface OrderService {
    List<Order> getOrders(int clientId);

    List<OrderItem> getOrderItems(int orderId);

    boolean submitOrder(Order order);

    Order getOrder(int orderId);

    int getMaksId();

    boolean createEmtyOrder(int id);
}

package mroweczka.e_zakupy.dataAccess.api.order;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import mroweczka.e_zakupy.dataAccess.api.ServiceFactory;
import mroweczka.e_zakupy.dataAccess.api.customer.CustomerSyncDbInteractor;
import mroweczka.e_zakupy.dataAccess.api.discount.DiscountSyncDbInteractor;
import mroweczka.e_zakupy.dataAccess.api.facility.FacilitySyncDBInteractor;
import mroweczka.e_zakupy.dataAccess.api.payment.PaymentFactory;
import mroweczka.e_zakupy.entity.Order;
import mroweczka.e_zakupy.utils.DateUtils;


public class OrderFactory implements ServiceFactory {
    public static final String TABLE_NAME = "Orders";
    public static final String KEY_ID = "key_order_id";
    public static final String KEY_PRICE = "key_price";
    public static final String KEY_DATE_OF_ORDER = "key_date_of_order";
    public static final String KEY_REQUEST_COLLECT_DATE = "key_request_collect_date";
    public static final String KEY_COLLECT_DATE = "key_collect_date";
    public static final String KEY_COLLECT_CODE = "key_collect_code";
    public static final String KEY_STATE_OF_ORDER = "key_state_of_order";
    public static final String KEY_TYPE_ORDER = "key_type_order";
    public static final String KEY_FACILITY_ID = "key_facility_id";
    public static final String KEY_CUSTOMER_ID = "key_customer_id";
    public static final String KEY_PAYMENT_ID = "key_payment_id";
    public static final String KEY_DISCOUNT_ID = "key_discount_id";


    private static final String CREATE_TABLE =
            String.format("CREATE TABLE %s (" +
                            "%s INTEGER, " +
                            "%s DOUBLE, " +
                            "%s TEXT, " +
                            "%s TEXT, " +
                            "%s TEXT, " +
                            "%s INTEGER, " +
                            "%s INTEGER, " +
                            "%s INTEGER, " +
                            "%s INTEGER, " +
                            "%s INTEGER, " +
                            "%s INTEGER, " +
                            "%s INTEGER, " +
                            "FOREIGN KEY (%s) REFERENCES %s(%s) ON DELETE CASCADE, " +
                            "FOREIGN KEY (%s) REFERENCES %s(%s) ON DELETE CASCADE, " +
                            "FOREIGN KEY (%s) REFERENCES %s(%s) ON DELETE CASCADE, " +
                            "FOREIGN KEY (%s) REFERENCES %s(%s) ON DELETE CASCADE, " +
                            "PRIMARY KEY (%s))",
                    TABLE_NAME,
                    KEY_ID,
                    KEY_PRICE,
                    KEY_DATE_OF_ORDER,
                    KEY_REQUEST_COLLECT_DATE,
                    KEY_COLLECT_DATE,
                    KEY_COLLECT_CODE,
                    KEY_STATE_OF_ORDER,
                    KEY_TYPE_ORDER,
                    KEY_FACILITY_ID,
                    KEY_CUSTOMER_ID,
                    KEY_PAYMENT_ID,
                    KEY_DISCOUNT_ID,
                    KEY_FACILITY_ID, FacilitySyncDBInteractor.TABLE_NAME, FacilitySyncDBInteractor.KEY_ID,
                    KEY_CUSTOMER_ID, CustomerSyncDbInteractor.TABLE_NAME, CustomerSyncDbInteractor.KEY_ID,
                    KEY_PAYMENT_ID, PaymentFactory.TABLE_NAME, PaymentFactory.KEY_ID,
                    KEY_DISCOUNT_ID, DiscountSyncDbInteractor.TABLE_NAME, DiscountSyncDbInteractor.KEY_ID,
                    KEY_ID
            );


    public static final Order[] ORDERS = new Order[]{
            new Order(1, 130.60d, DateUtils.getDateInCommonFormat(DateUtils.DATE_TODAY), DateUtils.getDateTimeInCommonFormat(DateUtils.DATE_TIME_TODAY).plusDays(2), DateUtils.getDateInCommonFormat(DateUtils.DATE_TODAY).plusDays(3), 654345, Order.OrderType.Express, Order.StateOfOrder.Canceled, null, null, null, null),
            new Order(2, 230.10d, DateUtils.getDateInCommonFormat(DateUtils.DATE_TODAY), DateUtils.getDateTimeInCommonFormat(DateUtils.DATE_TIME_TODAY).plusDays(2), DateUtils.getDateInCommonFormat(DateUtils.DATE_TODAY).plusDays(3), 654345, Order.OrderType.Express, Order.StateOfOrder.Canceled, null, null, null, null),
            new Order(3, 270.22d, DateUtils.getDateInCommonFormat(DateUtils.DATE_TODAY), DateUtils.getDateTimeInCommonFormat(DateUtils.DATE_TIME_TODAY).plusDays(2), DateUtils.getDateInCommonFormat(DateUtils.DATE_TODAY).plusDays(3), 654345, Order.OrderType.Express, Order.StateOfOrder.Canceled, null, null, null, null),
            new Order(4, 55.25d, DateUtils.getDateInCommonFormat(DateUtils.DATE_TODAY), DateUtils.getDateTimeInCommonFormat(DateUtils.DATE_TIME_TODAY).plusDays(2), DateUtils.getDateInCommonFormat(DateUtils.DATE_TODAY).plusDays(3), 654345, Order.OrderType.Express, Order.StateOfOrder.Canceled, null, null, null, null),
    };

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpdate(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }

    @Override
    public void prePopulateData(SQLiteDatabase db) {
        db.beginTransaction();

        ContentValues values = new ContentValues();
        int rowId = 0;
        for (Order order : ORDERS) {
            values.put(KEY_ID, order.getOrderId());
            values.put(KEY_PRICE, order.getPrice());
            values.put(KEY_DATE_OF_ORDER, order.getDateOfOrder().toString(DateUtils.COMMON_DATE_PATTERN));
            values.put(KEY_REQUEST_COLLECT_DATE, order.getRequestedCollectDate().toString(DateUtils.COMMON_DATE_TIME_PATTERN));
            values.put(KEY_COLLECT_DATE, order.getCollectDate().toString(DateUtils.COMMON_DATE_PATTERN));
            values.put(KEY_COLLECT_CODE, order.getCollectCode());
            values.put(KEY_STATE_OF_ORDER, order.getStateOfOrder().getValue());
            values.put(KEY_TYPE_ORDER, order.getOrderType().getValue());
            values.put(KEY_FACILITY_ID, rowId % 4 + 1);
            values.put(KEY_CUSTOMER_ID, rowId % 4 + 1);
            values.put(KEY_PAYMENT_ID, rowId % 4 + 1);
            db.insert(TABLE_NAME, null, values);
            values.clear();
            rowId++;

        }

        db.setTransactionSuccessful();
        db.endTransaction();
    }
}

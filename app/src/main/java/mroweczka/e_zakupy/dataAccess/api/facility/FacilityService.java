package mroweczka.e_zakupy.dataAccess.api.facility;

import java.util.List;

import mroweczka.e_zakupy.entity.Facility;
import mroweczka.e_zakupy.entity.Town;
import mroweczka.e_zakupy.entity.WorkDay;

public interface FacilityService {
    List<Facility> getFacilities();
    List<WorkDay> getWorkDays(int facilityId);
    Town getTown(int townId);

    Facility getFacility(int facilityId);
}
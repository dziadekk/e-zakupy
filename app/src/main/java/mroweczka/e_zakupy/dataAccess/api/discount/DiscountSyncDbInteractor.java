package mroweczka.e_zakupy.dataAccess.api.discount;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import mroweczka.e_zakupy.app.di.qualifier.SyncDB;
import mroweczka.e_zakupy.dataAccess.DBAccess;
import mroweczka.e_zakupy.dataAccess.api.ServiceFactory;
import mroweczka.e_zakupy.dataAccess.api.catalog.CategoryFactory;
import mroweczka.e_zakupy.dataAccess.api.customer.CustomerSyncDbInteractor;
import mroweczka.e_zakupy.dataAccess.api.order.OrderFactory;
import mroweczka.e_zakupy.dataAccess.repositories.customer.CustomerRepository;
import mroweczka.e_zakupy.entity.Discount;


public class DiscountSyncDbInteractor implements DiscountService {
    public static final String TABLE_NAME = "Discounts";
    public static final String KEY_ID = "key_discount_id";
    public static final String KEY_PERCENT_OF_DISCOUNT = "key_percent_of_discount";
    public static final String KEY_DISCOUNT_CODE = "key_discount_code";
    public static final String KEY_ORDER_ID = "key_order_id";
    public static final String KEY_CUSTOMER_ID = "key_customer_id";
    public static final String KEY_CATEGORY_ID = "key_category_id";

    private DBAccess synchronizedDBAccess;

    @Inject
    public DiscountSyncDbInteractor(@SyncDB DBAccess synchronizedDBAccess) {
        this.synchronizedDBAccess = synchronizedDBAccess;
    }

    private static final String CREATE_TABLE =
            String.format("CREATE TABLE %s (" +
                            "%s INTEGER, " +
                            "%s INTEGER, " +
                            "%s TEXT, " +
                            "%s INTEGER, " +
                            "%s INTEGER," +
                            "%s INTEGER," +
                            "FOREIGN KEY (%s) REFERENCES %s(%s) ON DELETE CASCADE, " +
                            "FOREIGN KEY (%s) REFERENCES %s(%s) ON DELETE CASCADE, " +
                            "FOREIGN KEY (%s) REFERENCES %s(%s) ON DELETE CASCADE, " +
                            "PRIMARY KEY (%s))",
                    TABLE_NAME,
                    KEY_ID,
                    KEY_PERCENT_OF_DISCOUNT,
                    KEY_DISCOUNT_CODE,
                    KEY_CUSTOMER_ID,
                    KEY_CATEGORY_ID,
                    KEY_ORDER_ID,
                    KEY_CUSTOMER_ID, CustomerSyncDbInteractor.TABLE_NAME, CustomerSyncDbInteractor.KEY_ID,
                    KEY_CATEGORY_ID, CategoryFactory.TABLE_NAME, CategoryFactory.KEY_ID,
                    KEY_ORDER_ID, OrderFactory.TABLE_NAME, OrderFactory.KEY_ID,
                    KEY_ID
            );

    private static Discount[] DISCOUNTS = new Discount[]{
            new Discount(1, 10, "efkasd", null, null),
            new Discount(2, 20, "askasd", null, null),
            new Discount(3, 30, "afk2sd", null, null),
            new Discount(4, 40, "afkasd", null, null),
            new Discount(5, 50, "nbvasd", null, null),
            new Discount(6, 23, "etaads", null, null),
            new Discount(7, 23, "esadas", null, null)
    };

    public static ServiceFactory newSyncedDB() {
        return new ServiceFactory() {
            @Override
            public void onCreate(SQLiteDatabase db) {
                db.execSQL(CREATE_TABLE);
            }

            @Override
            public void onUpdate(SQLiteDatabase db) {
                db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            }

            @Override
            public void prePopulateData(SQLiteDatabase db) {
                db.beginTransaction();

                ContentValues values = new ContentValues();
                int rowId = 0;
                for (Discount discount : DISCOUNTS) {
                    values.put(KEY_ID, discount.getDiscountId());
                    values.put(KEY_DISCOUNT_CODE, discount.getDiscountCode());
                    values.put(KEY_ORDER_ID, rowId % 4 + 1);
                    values.put(KEY_CUSTOMER_ID, rowId % 4 + 1);
                    values.put(KEY_CATEGORY_ID, rowId % 4 + 9);
                    values.put(KEY_ORDER_ID, rowId % 4 + 1);
                    values.put(KEY_PERCENT_OF_DISCOUNT, discount.getPercentOfDiscount());

                    db.insert(TABLE_NAME, null, values);
                    values.clear();
                    rowId++;
                }

                db.setTransactionSuccessful();
                db.endTransaction();
            }
        };
    }

    @Override
    public List<Discount> getDiscounts(int idKlienta) {
        SQLiteDatabase db = synchronizedDBAccess.getReadableDatabase();

        String query = String.format("SELECT * FROM %s WHERE %s = %s", TABLE_NAME, KEY_CUSTOMER_ID, idKlienta);

        Cursor cursor = db.rawQuery(query, null);
        List<Discount> discounts = new ArrayList<Discount>();


        if (cursor.moveToFirst()) {
            do {
                discounts.add(new Discount(cursor.getInt(0), cursor.getInt(1), cursor.getString(2), null, null));
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return discounts;
    }

    @Override
    public Discount getDiscount(int discountId) {
        for (Discount discount : getDiscounts(CustomerRepository.CURRENT_CUSTOMER_ID)) {
            if (discount.getDiscountId() == discountId) {
                return discount;
            }
        }
        return null;
    }
}

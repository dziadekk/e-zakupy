package mroweczka.e_zakupy.dataAccess.api.storage;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import mroweczka.e_zakupy.app.di.qualifier.SyncDB;
import mroweczka.e_zakupy.dataAccess.DBAccess;
import mroweczka.e_zakupy.dataAccess.api.ServiceFactory;
import mroweczka.e_zakupy.dataAccess.api.facility.FacilitySyncDBInteractor;

public class StorageSyncDBInteractor implements StorageService {
    private static final String TAG = StorageSyncDBInteractor.class.getSimpleName();

    public static final String TABLE_NAME = "STORAGE";
    public static final String KEY_FACILITY_NUMBER = "key_facility_number";
    public static final String KEY_PRODUCT_ID = "key_product_id";
    public static final String KEY_QUANTITY = "key_quantity";

    private static final String CREATE_TABLE =
            String.format("CREATE TABLE %s (" +
                            "%s INTEGER, " +
                            "%s INTEGER, " +
                            "%s REAL, " +
                            "FOREIGN KEY (%s) REFERENCES %s(%s) ON DELETE CASCADE, " +
                            "PRIMARY KEY (%s,%s))",
                    TABLE_NAME,
                    KEY_FACILITY_NUMBER,
                    KEY_PRODUCT_ID,
                    KEY_QUANTITY,
                    KEY_FACILITY_NUMBER, FacilitySyncDBInteractor.TABLE_NAME, FacilitySyncDBInteractor.KEY_ID,
                    KEY_FACILITY_NUMBER, KEY_PRODUCT_ID);


    private DBAccess synchronizedDBAccess;

    @Inject
    public StorageSyncDBInteractor(@SyncDB DBAccess synchronizedDBAccess) {
        this.synchronizedDBAccess = synchronizedDBAccess;
    }

    @Override
    public Map<Integer, Double> getProductsAvailability(int facilityNumber, List<Integer> orderedProducts) {
        Map<Integer, Double> availability = new HashMap<>(orderedProducts.size());
        for (Integer productId : orderedProducts) {
            availability.put(productId, getProductAvailability(facilityNumber, productId));
        }
        return availability;
    }

    private double getProductAvailability(int facilityNumber, int productId) {
        SQLiteDatabase db = synchronizedDBAccess.getReadableDatabase();
        String query = String.format("SELECT %s FROM %s WHERE %s=%d AND %s=%d",
                KEY_QUANTITY,
                TABLE_NAME,
                KEY_PRODUCT_ID,
                productId,
                KEY_FACILITY_NUMBER,
                facilityNumber);

        Cursor cursor = db.rawQuery(query, null);

        double quantity = 0;
        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                quantity = cursor.getDouble(0);
            }
        }

        cursor.close();
        db.close();

        return quantity;
    }

    @Override
    public boolean lockItems(int facilityNumber, List<Pair<Integer, Double>> products) {
        boolean successful = true;
        List<Pair<Integer, Double>> lockedProducts = new ArrayList<>(products.size());

        for (int i = 0; i < products.size() && successful; i++) {
            successful = changeProductAvailability(facilityNumber, products.get(i), true);
        }

        if (!successful) {
            for (Pair<Integer, Double> lockedProduct : lockedProducts) {
                changeProductAvailability(facilityNumber, lockedProduct, false);
            }
        }

        return true;
    }

    @Override
    public boolean unlockItems(int facilityNumber, List<Pair<Integer, Double>> products) {
        boolean successful = true;
        for (int i = 0; i < products.size() && successful; i++) {
            successful = changeProductAvailability(facilityNumber, products.get(i), false);
        }
        return true;
    }

    /**
     * Changes the quantity of product in given facility
     * If lockItems is set to true, the items are being locked in the magazine (quantity is decreased)
     * If lockItems is set to false, the items are being unlocked from the magazine (quantity is increased)
     *
     * @param facilityNumber
     * @param product
     * @param lockItems      flag to determining whether to decrease(true) or increase(false) the product quantity
     * @return
     */
    private boolean changeProductAvailability(int facilityNumber, Pair<Integer, Double> product, boolean lockItems) {
        double productQuantity = getProductAvailability(facilityNumber, product.first);

        SQLiteDatabase db = synchronizedDBAccess.getWritableDatabase();

        ContentValues args = new ContentValues();
        if (lockItems) {
            args.put(KEY_QUANTITY, productQuantity - product.second);
        } else {
            args.put(KEY_QUANTITY, productQuantity + product.second);
        }

        db.update(TABLE_NAME, args, String.format("%s=%d AND %s=%d",
                KEY_FACILITY_NUMBER, facilityNumber,
                KEY_PRODUCT_ID, product.first), null);
        db.close();
        return true;
    }

    public static ServiceFactory newSyncedDB() {
        return new ServiceFactory() {
            @Override
            public void onCreate(SQLiteDatabase db) {
                db.execSQL(CREATE_TABLE);
            }

            @Override
            public void onUpdate(SQLiteDatabase db) {
                db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            }

            @Override
            public void prePopulateData(SQLiteDatabase db) {
                db.beginTransaction();

                ContentValues values = new ContentValues();

                for (int i = 1; i < 241; i++) {
                    values.put(KEY_FACILITY_NUMBER, 1);
                    values.put(KEY_PRODUCT_ID, i);
                    values.put(KEY_QUANTITY, 100d);
                    db.insert(TABLE_NAME, null, values);
                    values.clear();
                }

                db.setTransactionSuccessful();
                db.endTransaction();
            }
        };
    }
}
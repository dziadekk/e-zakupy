package mroweczka.e_zakupy.dataAccess.api;

import android.database.sqlite.SQLiteDatabase;

import mroweczka.e_zakupy.dataAccess.localDB.LocalDbFactory;

public interface ServiceFactory extends LocalDbFactory {
    void prePopulateData(SQLiteDatabase db);
}

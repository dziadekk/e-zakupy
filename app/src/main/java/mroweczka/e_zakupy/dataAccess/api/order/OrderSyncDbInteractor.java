package mroweczka.e_zakupy.dataAccess.api.order;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import mroweczka.e_zakupy.app.di.qualifier.SyncDB;
import mroweczka.e_zakupy.dataAccess.DBAccess;
import mroweczka.e_zakupy.dataAccess.api.catalog.CatalogService;
import mroweczka.e_zakupy.dataAccess.api.discount.DiscountService;
import mroweczka.e_zakupy.dataAccess.api.facility.FacilityService;
import mroweczka.e_zakupy.dataAccess.api.payment.PaymentService;
import mroweczka.e_zakupy.dataAccess.repositories.customer.CustomerRepository;
import mroweczka.e_zakupy.entity.Order;
import mroweczka.e_zakupy.entity.OrderItem;
import mroweczka.e_zakupy.utils.DateUtils;

public class OrderSyncDbInteractor implements OrderService {
    private DBAccess synchronizedDBAccess;

    private PaymentService paymentService;
    private DiscountService discountService;
    private FacilityService facilityService;
    private CatalogService catalogService;

    @Inject
    public OrderSyncDbInteractor(@SyncDB DBAccess synchronizedDBAccess,
                                 PaymentService paymentService,
                                 DiscountService discountService,
                                 FacilityService facilityService,
                                 CatalogService catalogService) {
        this.synchronizedDBAccess = synchronizedDBAccess;
        this.paymentService = paymentService;
        this.discountService = discountService;
        this.facilityService = facilityService;
        this.catalogService = catalogService;
    }

    @Override
    public List<Order> getOrders(int clientId) {
        SQLiteDatabase db = synchronizedDBAccess.getReadableDatabase();

        String query = String.format("SELECT * FROM %s WHERE %s = %s", OrderFactory.TABLE_NAME, OrderFactory.KEY_CUSTOMER_ID, clientId);

        Cursor cursor = db.rawQuery(query, null);
        List<Order> orders = new ArrayList<Order>();


        if (cursor.moveToFirst()) {
            do {
                orders.add(new Order(cursor.getInt(0),
                        cursor.getDouble(1),
                        DateUtils.getDateInCommonFormat(cursor.getString(2)),
                        DateUtils.getDateTimeInCommonFormat(cursor.getString(3)),
                        DateUtils.getDateInCommonFormat(cursor.getString(4)),
                        cursor.getInt(5),
                        Order.OrderType.enumOf(cursor.getInt(7)),
                        Order.StateOfOrder.enumOf(cursor.getInt(6)),
                        getOrderItems(cursor.getInt(0)),
                        null,
                        null,
                        null));
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return orders;
    }

    @Override
    public List<OrderItem> getOrderItems(int orderId) {
        SQLiteDatabase db = synchronizedDBAccess.getReadableDatabase();

        String query = String.format("SELECT * FROM %s WHERE %s = %s", OrderItemFactory.TABLE_NAME, OrderItemFactory.KEY_ORDER_ID, orderId);

        Cursor cursor = db.rawQuery(query, null);
        List<OrderItem> orderItems = new ArrayList<OrderItem>();


        if (cursor.moveToFirst()) {
            do {
                orderItems.add(new OrderItem(cursor.getInt(0), catalogService.getProduct(cursor.getInt(3)), cursor.getDouble(1), cursor.getDouble(2)));
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return orderItems;
    }

    @Override
    public Order getOrder(int orderId) {
        SQLiteDatabase db = synchronizedDBAccess.getReadableDatabase();

        String query = String.format("SELECT * FROM %s WHERE %s = %d",
                OrderFactory.TABLE_NAME,
                OrderFactory.KEY_ID, orderId);

        Cursor cursor = db.rawQuery(query, null);
        Order order = null;

        if (cursor.moveToFirst()) {
            order = new Order(cursor.getInt(0),
                    cursor.getDouble(1),
                    DateUtils.getDateInCommonFormat(cursor.getString(2)),
                    null,
                    null,//DateUtils.getDateInCommonFormat(cursor.getString(4)),
                    cursor.getInt(5),
                    Order.OrderType.enumOf(cursor.getInt(7)),
                    Order.StateOfOrder.enumOf(cursor.getInt(6)),
                    getOrderItems(cursor.getInt(0)),
                    null,
                    null,
                    facilityService.getFacility(cursor.getInt(8)));

            if (!cursor.isNull(3)) {
                order.setRequestedCollectDate(DateUtils.getDateTimeInCommonFormat(cursor.getString(3)));
            }
            if (!cursor.isNull(11)) {
                order.setDiscount(discountService.getDiscount(cursor.getInt(11)));
            }
            if (!cursor.isNull(10)) {
                order.setPayment(paymentService.getPayment(cursor.getInt(10)));
            }
        }

        cursor.close();
        db.close();

        return order;
    }

    @Override
    public int getMaksId() {
        SQLiteDatabase db = synchronizedDBAccess.getReadableDatabase();

        String query = String.format("SELECT MAX(%s)+1 FROM %s",
                OrderFactory.KEY_ID, OrderFactory.TABLE_NAME);

        Cursor cursor = db.rawQuery(query, null);
        int maksId = 1;
        if (cursor.moveToFirst()) {
            maksId = cursor.getInt(0);
        }

        cursor.close();
        db.close();

        return maksId;
    }

    @Override
    public boolean createEmtyOrder(int id) {
        SQLiteDatabase db = synchronizedDBAccess.getWritableDatabase();
        db.beginTransaction();
        ContentValues values = new ContentValues();
        values.put(OrderFactory.KEY_ID, id);
        db.insert(OrderFactory.TABLE_NAME, null, values);
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
        return true;
    }

    @Override
    public boolean submitOrder(Order order) {
        SQLiteDatabase db = synchronizedDBAccess.getWritableDatabase();
        db.beginTransaction();

        ContentValues values = new ContentValues();
        values.put(OrderFactory.KEY_PRICE, order.getPrice());
        values.put(OrderFactory.KEY_DATE_OF_ORDER, order.getDateOfOrder().toString(DateUtils.COMMON_DATE_PATTERN));
        if (order.getRequestedCollectDate() != null) {
            values.put(OrderFactory.KEY_REQUEST_COLLECT_DATE, order.getRequestedCollectDate().toString(DateUtils.COMMON_DATE_TIME_PATTERN));
        }
        values.put(OrderFactory.KEY_STATE_OF_ORDER, order.getStateOfOrder().getValue());
        values.put(OrderFactory.KEY_TYPE_ORDER, order.getOrderType().getValue());
        values.put(OrderFactory.KEY_FACILITY_ID, order.getFacility().getId());
        values.put(OrderFactory.KEY_CUSTOMER_ID, CustomerRepository.CURRENT_CUSTOMER_ID);
        if (order.getPayment().getId() != null) {
            values.put(OrderFactory.KEY_PAYMENT_ID, order.getPayment().getId());
        }
        if (order.getDiscount().getDiscountId() != null) {
            values.put(OrderFactory.KEY_DISCOUNT_ID, order.getDiscount().getDiscountId());
        }

        db.update(OrderFactory.TABLE_NAME, values, String.format("%s=%d",
                OrderFactory.KEY_ID, order.getOrderId()), null);

        submitOrderItems(order.getOrderItems(), db);

        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
        return true;
    }


    private int getNextOrderItemIndex(SQLiteDatabase db) {

        int index = 0;
        String query = String.format("SELECT MAX(%s) FROM %s",
                OrderItemFactory.KEY_ID, OrderItemFactory.TABLE_NAME);

        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            index = cursor.getInt(0);
        }
        index++;

        cursor.close();

        return index;
    }

    private boolean submitOrderItems(List<OrderItem> orderItems, SQLiteDatabase db) {
        int index = getNextOrderItemIndex(db);
        ContentValues values = new ContentValues();
        for (OrderItem orderItem : orderItems) {
            values.put(OrderItemFactory.KEY_ID, index);
            values.put(OrderItemFactory.KEY_PRICE, orderItem.getPrice());
            values.put(OrderItemFactory.KEY_QUANTITY, orderItem.getQuantity());
            values.put(OrderItemFactory.KEY_PRODUCT_ID, orderItem.getProduct().getId());
            values.put(OrderItemFactory.KEY_ORDER_ID, orderItem.getOrderItemId());
            db.insert(OrderItemFactory.TABLE_NAME, null, values);
            values.clear();
            index++;
        }


        return true;
    }

}

package mroweczka.e_zakupy.dataAccess.api.catalog;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import mroweczka.e_zakupy.dataAccess.api.ServiceFactory;
import mroweczka.e_zakupy.entity.Manufacturer;


public class ManufacturerFactory implements ServiceFactory {
    public static final String TABLE_NAME = "Manufacturers";
    public static final String KEY_ID = "key_manufacturer_id";
    public static final String KEY_MANUFACTURER_NAME = "key_manufacturer_name";
    public static final String KEY_COUNTRY_ID = "key_country_id";


    private static final String CREATE_TABLE =
            String.format("CREATE TABLE %s (" +
                            "%s INTEGER NOT NULL, " +
                            "%s TEXT NOT NULL, " +
                            "%s INTEGER NOT NULL, " +
                            "FOREIGN KEY (%s) REFERENCES %s(%s) ON DELETE CASCADE, " +
                            "PRIMARY KEY (%s))",
                    TABLE_NAME,
                    KEY_ID,
                    KEY_MANUFACTURER_NAME,
                    KEY_COUNTRY_ID,
                    KEY_COUNTRY_ID,
                    CountryFactory.TABLE_NAME,
                    CountryFactory.KEY_ID,
                    KEY_ID
            );

    private static final Manufacturer[] MANUFACTURERS = new Manufacturer[]{
            new Manufacturer(1, "Złoty Owoc"),
            new Manufacturer(2, "Gruszka Grozy"),
            new Manufacturer(3, "Atomowe Banany"),
            new Manufacturer(4, "Lord Somersby"),
            new Manufacturer(5, "Słodka przygoda"),
            new Manufacturer(6, "Pepsi"),
            new Manufacturer(7, "Coca-cola"),
            new Manufacturer(8, "Gouda"),
            new Manufacturer(9, "Danio"),
            new Manufacturer(10, "Milka"),
            new Manufacturer(11, "Dove"),
            new Manufacturer(12, "Szwarczkopf"),
            new Manufacturer(13, "Dew"),
            new Manufacturer(14, "Purex"),
            new Manufacturer(15, "Roleks"),
    };

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpdate(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }

    @Override
    public void prePopulateData(SQLiteDatabase db) {
        db.beginTransaction();

        ContentValues values = new ContentValues();

        int countryId = 0;
        for (Manufacturer manufacturer : MANUFACTURERS) {
            values.put(KEY_ID, manufacturer.getManufacturerId());
            values.put(KEY_MANUFACTURER_NAME, manufacturer.getManufacturerName());
            values.put(KEY_COUNTRY_ID, countryId%6+1);
            db.insert(TABLE_NAME, null, values);
            values.clear();

        }

        db.setTransactionSuccessful();
        db.endTransaction();
    }
}

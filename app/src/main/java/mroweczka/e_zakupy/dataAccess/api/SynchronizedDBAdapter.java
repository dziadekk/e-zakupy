package mroweczka.e_zakupy.dataAccess.api;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import mroweczka.e_zakupy.dataAccess.DBAccess;
import mroweczka.e_zakupy.dataAccess.api.catalog.CategoryFactory;
import mroweczka.e_zakupy.dataAccess.api.catalog.CountryFactory;
import mroweczka.e_zakupy.dataAccess.api.catalog.ManufacturerFactory;
import mroweczka.e_zakupy.dataAccess.api.catalog.ProductFactory;
import mroweczka.e_zakupy.dataAccess.api.catalog.PromotionalPriceFactory;
import mroweczka.e_zakupy.dataAccess.api.catalog.VatFactory;
import mroweczka.e_zakupy.dataAccess.api.customer.CustomerSyncDbInteractor;
import mroweczka.e_zakupy.dataAccess.api.discount.DiscountSyncDbInteractor;
import mroweczka.e_zakupy.dataAccess.api.facility.FacilitySyncDBInteractor;
import mroweczka.e_zakupy.dataAccess.api.facility.TownFactory;
import mroweczka.e_zakupy.dataAccess.api.facility.WorkDayFactory;
import mroweczka.e_zakupy.dataAccess.api.order.OrderFactory;
import mroweczka.e_zakupy.dataAccess.api.order.OrderItemFactory;
import mroweczka.e_zakupy.dataAccess.api.payment.PaymentFactory;
import mroweczka.e_zakupy.dataAccess.api.storage.StorageSyncDBInteractor;

public class SynchronizedDBAdapter extends SQLiteOpenHelper implements DBAccess {
    private static final String TAG = SynchronizedDBAdapter.class.getSimpleName();

    private static final int DATABASE_VERSION = 51;
    private static final String DATABASE_NAME = "e-zakupy-syncs-db";

    public SynchronizedDBAdapter(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @NonNull
    private static List<ServiceFactory> getServicesToCreate() {
        List<ServiceFactory> servicesToCreate = new ArrayList<>();
        servicesToCreate.add(new VatFactory());
        servicesToCreate.add(CustomerSyncDbInteractor.newSyncedDB());
        servicesToCreate.add(new CountryFactory());
        servicesToCreate.add(new ManufacturerFactory());
        servicesToCreate.add(new CategoryFactory());
        servicesToCreate.add(new PromotionalPriceFactory());
        servicesToCreate.add(new ProductFactory());
        servicesToCreate.add(FacilitySyncDBInteractor.newSyncedDB());
        servicesToCreate.add(StorageSyncDBInteractor.newSyncedDB());
        servicesToCreate.add(new WorkDayFactory());
        servicesToCreate.add(new TownFactory());
        servicesToCreate.add(new PaymentFactory());
        servicesToCreate.add(new OrderFactory());
        servicesToCreate.add(new OrderItemFactory());
        servicesToCreate.add(DiscountSyncDbInteractor.newSyncedDB());
        return servicesToCreate;
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {// TODO: 22/01/2017 zapomniales o tym
            //db.execSQL("PRAGMA foreign_keys = ON;");
        }
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        onCreateServices(sqLiteDatabase, getServicesToCreate());
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        List<ServiceFactory> servicesToCreate = getServicesToCreate();
        for (ServiceFactory serviceFactory : servicesToCreate) {
            serviceFactory.onUpdate(sqLiteDatabase);
        }

        onCreateServices(sqLiteDatabase, servicesToCreate);
    }

    private void onCreateServices(SQLiteDatabase sqLiteDatabase, List<ServiceFactory> servicesToCreate) {
        for (ServiceFactory serviceFactory : servicesToCreate) {
            serviceFactory.onCreate(sqLiteDatabase);
        }

        for (ServiceFactory serviceFactory : servicesToCreate) {
            serviceFactory.prePopulateData(sqLiteDatabase);
        }
    }
}
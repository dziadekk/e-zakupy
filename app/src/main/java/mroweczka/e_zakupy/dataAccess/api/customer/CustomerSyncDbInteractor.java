package mroweczka.e_zakupy.dataAccess.api.customer;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import mroweczka.e_zakupy.app.di.qualifier.SyncDB;
import mroweczka.e_zakupy.dataAccess.DBAccess;
import mroweczka.e_zakupy.dataAccess.api.ServiceFactory;


public class CustomerSyncDbInteractor implements CustomerService {
    private DBAccess synchronizedDBAccess;

    @Inject
    public CustomerSyncDbInteractor(@SyncDB DBAccess synchronizedDBAccess) {
        this.synchronizedDBAccess = synchronizedDBAccess;
    }

    public static final String TABLE_NAME = "Customers";
    public static final String KEY_ID = "key_customer_id";


    private static final String CREATE_TABLE =
            String.format("CREATE TABLE %s (" +
                            "%s INTEGER, " +
                            "PRIMARY KEY (%s))",
                    TABLE_NAME,
                    KEY_ID,
                    KEY_ID
            );

    @Override
    public List<Integer> getCustomers() {
        SQLiteDatabase db = synchronizedDBAccess.getReadableDatabase();
        String query = String.format("SELECT * FROM %s",
                                     TABLE_NAME
                                     );

        Cursor cursor = db.rawQuery(query, null);

        ArrayList<Integer> customers = new ArrayList<>();
         if (cursor.moveToFirst()) {
             do {
               customers.add(cursor.getInt(0));
          } while (cursor.moveToNext());
         }

        cursor.close();
        db.close();

        return customers;
    }


    public static ServiceFactory newSyncedDB() {
        return new ServiceFactory() {
            @Override
            public void onCreate(SQLiteDatabase db) {
                db.execSQL(CREATE_TABLE);
            }

            @Override
            public void onUpdate(SQLiteDatabase db) {
                db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            }

            @Override
            public void prePopulateData(SQLiteDatabase db) {
                db.beginTransaction();

                ContentValues values = new ContentValues();


                for (int i = 1; i < 5; i++) {
                    values.put(KEY_ID, i);
                    db.insert(TABLE_NAME, null, values);
                    values.clear();
                }

                db.setTransactionSuccessful();
                db.endTransaction();
            }
        };
    }


}


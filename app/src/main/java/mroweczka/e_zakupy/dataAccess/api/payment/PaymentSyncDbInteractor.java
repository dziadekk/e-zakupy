package mroweczka.e_zakupy.dataAccess.api.payment;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import mroweczka.e_zakupy.app.di.qualifier.SyncDB;
import mroweczka.e_zakupy.dataAccess.DBAccess;
import mroweczka.e_zakupy.entity.Payment;
import mroweczka.e_zakupy.utils.DateUtils;

public class PaymentSyncDbInteractor implements PaymentService {

    private DBAccess synchronizedDBAccess;

    @Inject
    public PaymentSyncDbInteractor(@SyncDB DBAccess synchronizedDBAccess) {
        this.synchronizedDBAccess = synchronizedDBAccess;
    }

    @Override
    public List<Payment> getPayments() {

        SQLiteDatabase db = synchronizedDBAccess.getReadableDatabase();

        String query = String.format("SELECT * FROM %s", PaymentFactory.TABLE_NAME);

        Cursor cursor = db.rawQuery(query, null);
        List<Payment> payments = new ArrayList<Payment>();

        if (cursor.moveToFirst()) {
            do {
                payments.add(new Payment(cursor.getInt(0), Payment.PaymentType.enumOf(cursor.getInt(1)), cursor.getDouble(2), DateUtils.getDateInCommonFormat(cursor.getString(3)), DateUtils.getDateInCommonFormat(cursor.getString(4)), cursor.getInt(5), Payment.PaymentStatus.enumOf(cursor.getInt(6))));
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return payments;
    }

    @Override
    public Payment getPayment(int paymentId) {
        for (Payment payment : getPayments()) {
            if (payment.getId() == paymentId) {
                return payment;
            }
        }
        return null;
    }

    private int getNextPaymentId(SQLiteDatabase db) {
        int index = 0;
        String query = String.format("SELECT MAX(%s) FROM %s",
                PaymentFactory.KEY_ID, PaymentFactory.TABLE_NAME);

        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            index = cursor.getInt(0);
        }
        index++;

        cursor.close();

        return index;
    }

    @Override
    public int submitPayment(Payment payment) {
        SQLiteDatabase db = synchronizedDBAccess.getWritableDatabase();

        int index = getNextPaymentId(db);
        ContentValues values = new ContentValues();

        values.put(PaymentFactory.KEY_ID, index);
        values.put(PaymentFactory.KEY_PAYMENT_TYPE, payment.getPaymentType().getValue());
        values.put(PaymentFactory.KEY_AMOUNT, payment.getAmount());
        values.put(PaymentFactory.KEY_POSTING_DATE, payment.getPostingDate().toString(DateUtils.COMMON_DATE_PATTERN));
        values.put(PaymentFactory.KEY_PAYMENT_DATE, payment.getPaymentDate().toString(DateUtils.COMMON_DATE_PATTERN));
        values.put(PaymentFactory.KEY_NUMBER_OF_TRIES, payment.getNumberOfTries());
        values.put(PaymentFactory.KEY_PAYMENT_STATUS, payment.getPaymentStatus().getValue());

        db.insert(PaymentFactory.TABLE_NAME, null, values);

        values.clear();
        db.close();
        return index;
    }
}


package mroweczka.e_zakupy.dataAccess.api.catalog;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import mroweczka.e_zakupy.R;
import mroweczka.e_zakupy.dataAccess.api.ServiceFactory;
import mroweczka.e_zakupy.entity.Product;



public class ProductFactory implements ServiceFactory {
    public static final String TABLE_NAME = "Products";
    public static final String KEY_ID = "key_product_id";
    public static final String KEY_BAR_CODE = "key_bar_code";
    public static final String KEY_SHORT_NAME = "key_short_name";
    public static final String KEY_NAME = "key_name";
    public static final String KEY_DESCRIBE = "key_describe";
    public static final String KEY_UNIT_PRICE = "key_unit_prcie";
    public static final String KEY_IMAGE = "key_image";
    public static final String KEY_SELL_UNIT = "key_sell_unit";

    public static final String KEY_MANUFACTURER_ID = "key_manufacturer_id";
    public static final String KEY_VAT_ID = "key_vat_id";
    public static final String KEY_COUNTRY_ID = "key_country_id";
    public static final String KEY_CATEGORY_ID = "key_category_id";


    private static final String CREATE_TABLE =
            String.format("CREATE TABLE %s (" +
                            "%s Integer PRIMARY KEY autoincrement, " +
                            "%s TEXT NOT NULL, " +
                            "%s TEXT, " +
                            "%s TEXT NOT NULL, " +
                            "%s TEXT, " +
                            "%s DOUBLE NOT NULL, " +
                            "%s INTEGER, " +
                            "%s INTEGER NOT NULL, " +
                            "%s INTEGER NOT NULL, " +
                            "%s INTEGER NOT NULL, " +
                            "%s INTEGER NOT NULL, " +
                            "%s INTEGER NOT NULL, " +
                            "FOREIGN KEY (%s) REFERENCES %s(%s) ON DELETE CASCADE, " +
                            "FOREIGN KEY (%s) REFERENCES %s(%s) ON DELETE CASCADE, " +
                            "FOREIGN KEY (%s) REFERENCES %s(%s) ON DELETE CASCADE, " +
                            "FOREIGN KEY (%s) REFERENCES %s(%s) ON DELETE CASCADE)",
                    TABLE_NAME,
                    KEY_ID,
                    KEY_BAR_CODE,
                    KEY_SHORT_NAME,
                    KEY_NAME,
                    KEY_DESCRIBE,
                    KEY_UNIT_PRICE,
                    KEY_IMAGE,
                    KEY_SELL_UNIT,
                    KEY_MANUFACTURER_ID,
                    KEY_VAT_ID,
                    KEY_COUNTRY_ID,
                    KEY_CATEGORY_ID,
                    KEY_MANUFACTURER_ID, ManufacturerFactory.TABLE_NAME, ManufacturerFactory.KEY_ID,
                    KEY_VAT_ID, VatFactory.TABLE_NAME, VatFactory.KEY_ID,
                    KEY_COUNTRY_ID, CountryFactory.TABLE_NAME, CountryFactory.KEY_ID,
                    KEY_CATEGORY_ID, CategoryFactory.TABLE_NAME, CategoryFactory.KEY_ID
            );

    private static final Product[] PRODUCTS = new Product[]{
            new Product(1, "2345673543", "wczesne", "Jabłka Wczesne",
                    "Świeżo zebrane jabłka prosto z posiadłości lorda Somersby", 1.20d, R.drawable.apple, Product.Unit.Kilogram,
                    null, null, null, null, null),
            new Product(2, "2231216544", "wczesne", "Jabłka Zielone",
                    "Świeżo zebrane jabłka prosto z posiadłości lorda Somersby", 12.59d, R.drawable.apple, Product.Unit.Kilogram,
                    null, null, null, null, null),
            new Product(3, "2321323135", "wczesne", "Gruszka żółta",
                    "Świeżo zebrane jabłka prosto z posiadłości lorda Somersby", 6.07d, R.drawable.pear, Product.Unit.Kilogram,
                    null, null, null, null, null),
            new Product(4, "2321323136", "wczesne", "Gruszka zielona",
                    "Świeżo zebrane jabłka prosto z posiadłości lorda Somersby", 3.99d, R.drawable.pear, Product.Unit.Pieces,
                    null, null, null, null, null),
            new Product(5, "2321323137", "wczesne", "Jabłka pózne",
                    "Świeżo zebrane jabłka prosto z posiadłości lorda Somersby", 3.99d, R.drawable.apple, Product.Unit.Kilogram,
                    null, null, null, null, null),
    };

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpdate(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }

    @Override
    public void prePopulateData(SQLiteDatabase db) {
        db.beginTransaction();

        ContentValues values = new ContentValues();

        int rowId = 3;
        for (int i = 9; i < 58; i++) {
            for (Product product : PRODUCTS) {
                values.put(KEY_BAR_CODE, product.getBarcode());
                values.put(KEY_SHORT_NAME, product.getShortName());
                values.put(KEY_NAME, product.getName());
                values.put(KEY_DESCRIBE, product.getDescribe());
                values.put(KEY_UNIT_PRICE, product.getUnitPrice());
                values.put(KEY_IMAGE, product.getImage());
                values.put(KEY_SELL_UNIT, product.getSellUnit().getValue());
                values.put(KEY_MANUFACTURER_ID, rowId % 16 + 1);
                values.put(KEY_VAT_ID, rowId % 4 + 1);
                values.put(KEY_COUNTRY_ID, rowId % 6 + 1);
                values.put(KEY_CATEGORY_ID, i);
                db.insert(TABLE_NAME, null, values);
                values.clear();
                rowId++;
            }
        }

        db.setTransactionSuccessful();
        db.endTransaction();
    }
}

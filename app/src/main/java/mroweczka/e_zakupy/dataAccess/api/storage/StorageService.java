package mroweczka.e_zakupy.dataAccess.api.storage;

import android.util.Pair;

import java.util.List;
import java.util.Map;

public interface StorageService {
    Map<Integer, Double> getProductsAvailability(int facilityNumber, List<Integer> orderedProductsId);

    boolean lockItems(int facilityNumber, List<Pair<Integer, Double>> products);

    boolean unlockItems(int facilityNumber, List<Pair<Integer, Double>> products);
}

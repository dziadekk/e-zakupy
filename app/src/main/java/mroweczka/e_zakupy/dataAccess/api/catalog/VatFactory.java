package mroweczka.e_zakupy.dataAccess.api.catalog;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import mroweczka.e_zakupy.dataAccess.api.ServiceFactory;
import mroweczka.e_zakupy.entity.Vat;


public class VatFactory implements ServiceFactory{
    public static final String TABLE_NAME = "Vats";
    public static final String KEY_ID = "key_vat_id";
    public static final String KEY_CATEGORY = "key_category";
    public static final String KEY_PERCENT = "key_percent";


    private static final String CREATE_TABLE =
            String.format("CREATE TABLE %s (" +
                            "%s INTEGER NOT NULL, " +
                            "%s INTEGER NOT NULL, " +
                            "%s INTEGER NOT NULL, " +
                            "PRIMARY KEY (%s))",
                    TABLE_NAME,
                    KEY_ID,
                    KEY_CATEGORY,
                    KEY_PERCENT,
                    KEY_ID
            );

    private static final Vat[] VATS = new Vat[]{
            new Vat(1,Vat.VatCategory.A,0),
            new Vat(2,Vat.VatCategory.B,8),
            new Vat(3,Vat.VatCategory.C,19),
            new Vat(4,Vat.VatCategory.D,23)
    };

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpdate(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }

    @Override
    public void prePopulateData(SQLiteDatabase db) {
        db.beginTransaction();

        ContentValues values = new ContentValues();


        for (Vat vat : VATS) {
            values.put(KEY_ID,vat.getVatId());
            values.put(KEY_CATEGORY, vat.getCategory().getValue());
            values.put(KEY_PERCENT, vat.getPercent());
            db.insert(TABLE_NAME, null, values);
            values.clear();
        }

        db.setTransactionSuccessful();
        db.endTransaction();
    }
}

package mroweczka.e_zakupy.dataAccess.api.payment;

import java.util.List;

import mroweczka.e_zakupy.entity.Payment;


public interface PaymentService {
    List<Payment> getPayments();

    Payment getPayment(int paymentId);

    int submitPayment(Payment payment);
}

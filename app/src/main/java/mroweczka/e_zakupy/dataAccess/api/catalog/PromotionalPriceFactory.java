package mroweczka.e_zakupy.dataAccess.api.catalog;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import mroweczka.e_zakupy.dataAccess.api.ServiceFactory;
import mroweczka.e_zakupy.entity.PromotionalPrice;
import mroweczka.e_zakupy.utils.DateUtils;


public class PromotionalPriceFactory implements ServiceFactory {
    public static final String TABLE_NAME = "PromotionalPrices";
    public static final String KEY_ID = "key_promotional_price_id";
    public static final String KEY_VALID_FROM = "key_valid_from";
    public static final String KEY_VALID_UNTIL = "key_valid_until";
    public static final String KEY_PROMOTIONAL_PERCENT = "key_promotional_percent";
    public static final String KEY_PRICE_UNIT = "key_price_unit";
    public static final String KEY_PRODUCT_ID = "key_product_id";


    private static final String CREATE_TABLE =
            String.format("CREATE TABLE %s (" +
                            "%s INTEGER NOT NULL, " +
                            "%s TEXT NOT NULL, " +
                            "%s TEXT NOT NULL, " +
                            "%s INTEGER, " +
                            "%s DOUBLE, " +
                            "%s INTEGER NOT NULL, " +
                            "FOREIGN KEY (%s) REFERENCES %s(%s) ON DELETE CASCADE, " +
                            "PRIMARY KEY (%s))",
                    TABLE_NAME,
                    KEY_ID,
                    KEY_VALID_FROM,
                    KEY_VALID_UNTIL,
                    KEY_PROMOTIONAL_PERCENT,
                    KEY_PRICE_UNIT,
                    KEY_PRODUCT_ID,
                    KEY_PRODUCT_ID, ProductFactory.TABLE_NAME, ProductFactory.KEY_ID,
                    KEY_ID
            );

    private static final PromotionalPrice[] PROMOTIONAL_PRICES = new PromotionalPrice[]{
            new PromotionalPrice(1, DateUtils.getDateInCommonFormat("2015-10-22"), DateUtils.getDateInCommonFormat("2018-10-22"), null, 3.00d),
            new PromotionalPrice(2, DateUtils.getDateInCommonFormat("2015-10-22"), DateUtils.getDateInCommonFormat("2018-10-22"), 20, null)
    };

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpdate(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }

    @Override
    public void prePopulateData(SQLiteDatabase db) {
        db.beginTransaction();

        ContentValues values = new ContentValues();

        int rowId = 3;
        for (PromotionalPrice promotionalPrice : PROMOTIONAL_PRICES) {
            values.put(KEY_ID, promotionalPrice.getPromotionalPriceId());
            values.put(KEY_VALID_FROM, promotionalPrice.getValidFrom().toString(DateUtils.COMMON_DATE_PATTERN));
            values.put(KEY_VALID_UNTIL, promotionalPrice.getValidUntil().toString(DateUtils.COMMON_DATE_PATTERN));
            if (promotionalPrice.getPromotionalPercent() != null) {
                values.put(KEY_PROMOTIONAL_PERCENT, promotionalPrice.getPromotionalPercent());
            } else {
                values.put(KEY_PRICE_UNIT, promotionalPrice.getPriceUnit());
            }
            values.put(KEY_PRODUCT_ID, rowId % 5 + 1);

            db.insert(TABLE_NAME, null, values);
            values.clear();
            rowId++;
        }

        db.setTransactionSuccessful();
        db.endTransaction();
    }
}

package mroweczka.e_zakupy.dataAccess.api.customer;


import java.util.List;

import mroweczka.e_zakupy.entity.Customer;

public interface CustomerService {
    List<Integer> getCustomers();
}

package mroweczka.e_zakupy.dataAccess.api.catalog;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import mroweczka.e_zakupy.dataAccess.api.ServiceFactory;
import mroweczka.e_zakupy.entity.Country;


public class CountryFactory implements ServiceFactory {
    public static final String TABLE_NAME = "Countries";
    public static final String KEY_ID = "key_country_id";
    public static final String KEY_COUNTRY_NAME = "key_country_name";

    private static final String CREATE_TABLE =
            String.format("CREATE TABLE %s (" +
                            "%s INTEGER NOT NULL, " +
                            "%s TEXT NOT NULL, " +
                            "PRIMARY KEY (%s))",
                    TABLE_NAME,
                    KEY_ID,
                    KEY_COUNTRY_NAME,
                    KEY_ID
            );
    private final static Country[] COUNTRIES = new Country[]{
      new Country(1,"Polska"),
      new Country(2,"Czechy"),
      new Country(3,"Włochy"),
      new Country(4,"Hiszpania"),
      new Country(5,"Portugalia"),
      new Country(6,"Niemcy")
    };
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpdate(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }

    @Override
    public void prePopulateData(SQLiteDatabase db) {
        db.beginTransaction();

        ContentValues values = new ContentValues();


        for (Country country : COUNTRIES) {
            values.put(KEY_ID,country.getCountryId());
            values.put(KEY_COUNTRY_NAME, country.getCountryName());
            db.insert(TABLE_NAME, null, values);
            values.clear();

        }

        db.setTransactionSuccessful();
        db.endTransaction();
    }
}

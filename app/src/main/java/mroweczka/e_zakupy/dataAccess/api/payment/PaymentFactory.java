package mroweczka.e_zakupy.dataAccess.api.payment;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import mroweczka.e_zakupy.dataAccess.api.ServiceFactory;
import mroweczka.e_zakupy.entity.Payment;
import mroweczka.e_zakupy.utils.DateUtils;


public class PaymentFactory implements ServiceFactory{
    public static final String TABLE_NAME = "Payments";
    public static final String KEY_ID = "key_payment_id";
    public static final String KEY_PAYMENT_TYPE = "key_payment_type";
    public static final String KEY_AMOUNT = "key_amount";
    public static final String KEY_POSTING_DATE = "key_posting_date";
    public static final String KEY_PAYMENT_DATE = "key_payment_date";
    public static final String KEY_NUMBER_OF_TRIES= "key_number_of_tries";
    public static final String KEY_PAYMENT_STATUS = "key_payment_status";


    private static final String CREATE_TABLE =
            String.format("CREATE TABLE %s (" +
                            "%s INTEGER, " +
                            "%s INTEGER, " +
                            "%s DOUBLE, " +
                            "%s TEXT, " +
                            "%s TEXT, " +
                            "%s INTEGER, " +
                            "%s INTEGER, " +
                            "PRIMARY KEY (%s))",
                    TABLE_NAME,
                    KEY_ID,
                    KEY_PAYMENT_TYPE,
                    KEY_AMOUNT,
                    KEY_POSTING_DATE,
                    KEY_PAYMENT_DATE,
                    KEY_NUMBER_OF_TRIES,
                    KEY_PAYMENT_STATUS,
                    KEY_ID
            );
    private static Payment[] PAYMENTS = new Payment[]{
            new Payment(1, Payment.PaymentType.BankTransfer, 43.23d, DateUtils.getDateInCommonFormat(DateUtils.DATE_TODAY), DateUtils.getDateInCommonFormat(DateUtils.DATE_TODAY), 1, Payment.PaymentStatus.Accepted),
            new Payment(2, Payment.PaymentType.BankTransfer, 53.42d, DateUtils.getDateInCommonFormat(DateUtils.DATE_TODAY), DateUtils.getDateInCommonFormat(DateUtils.DATE_TODAY), 2, Payment.PaymentStatus.Accepted),
            new Payment(3, Payment.PaymentType.BankTransfer, 22.33d, DateUtils.getDateInCommonFormat(DateUtils.DATE_TODAY), DateUtils.getDateInCommonFormat(DateUtils.DATE_TODAY), 3, Payment.PaymentStatus.Accepted),
            new Payment(4, Payment.PaymentType.BankTransfer, 81.63d, DateUtils.getDateInCommonFormat(DateUtils.DATE_TODAY), DateUtils.getDateInCommonFormat(DateUtils.DATE_TODAY), 4, Payment.PaymentStatus.Denied),
    };
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpdate(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }

    @Override
    public void prePopulateData(SQLiteDatabase db) {
        db.beginTransaction();

        ContentValues values = new ContentValues();

        for (Payment payment : PAYMENTS) {
            values.put(KEY_ID, payment.getId());
            values.put(KEY_PAYMENT_TYPE, payment.getPaymentType().getValue());
            values.put(KEY_AMOUNT,payment.getAmount());
            values.put(KEY_POSTING_DATE, payment.getPostingDate().toString(DateUtils.COMMON_DATE_PATTERN));
            values.put(KEY_PAYMENT_DATE,payment.getPaymentDate().toString(DateUtils.COMMON_DATE_PATTERN));
            values.put(KEY_NUMBER_OF_TRIES,payment.getNumberOfTries());
            values.put(KEY_PAYMENT_STATUS, payment.getPaymentStatus().getValue());
            db.insert(TABLE_NAME, null, values);
            values.clear();
        }

        db.setTransactionSuccessful();
        db.endTransaction();
    }
}

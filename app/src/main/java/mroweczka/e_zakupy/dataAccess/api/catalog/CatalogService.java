package mroweczka.e_zakupy.dataAccess.api.catalog;


import java.util.List;

import mroweczka.e_zakupy.entity.Category;
import mroweczka.e_zakupy.entity.Country;
import mroweczka.e_zakupy.entity.Manufacturer;
import mroweczka.e_zakupy.entity.Product;
import mroweczka.e_zakupy.entity.PromotionalPrice;
import mroweczka.e_zakupy.entity.Vat;

public interface CatalogService {
    List<Category> getCategories();
    List<Product> getProductsCategory(int categoryId);
    List<Product> getProducts();
    List<Vat> getVats();
    List<Country> getCountry();
    List<Manufacturer> getManufacturers();

    List<PromotionalPrice> getPromotionalPrice(int productId);
    Product getProduct(int productId);

}

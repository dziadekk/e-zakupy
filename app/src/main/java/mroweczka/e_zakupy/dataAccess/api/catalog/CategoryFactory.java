package mroweczka.e_zakupy.dataAccess.api.catalog;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import mroweczka.e_zakupy.dataAccess.api.ServiceFactory;
import mroweczka.e_zakupy.entity.Category;
import mroweczka.e_zakupy.entity.Product;




public class CategoryFactory implements ServiceFactory {

    public static final String TABLE_NAME = "Categories";
    public static final String KEY_ID = "key_category_id";
    public static final String KEY_CATEGORY_NAME = "key_category_name";
    public static final String KEY_DESCRIBE = "key_describe";
    public static final String KEY_CATEGORY_ID = "key_sub_category_id";


    private static final String CREATE_TABLE =
            String.format("CREATE TABLE %s (" +
                            "%s INTEGER PRIMARY KEY autoincrement, " +
                            "%s TEXT NOT NULL, " +
                            "%s TEXT, " +
                            "%s INTEGER," +
                            "FOREIGN KEY (%s) REFERENCES %s(%s) ON DELETE CASCADE)",
                    TABLE_NAME,
                    KEY_ID,
                    KEY_CATEGORY_NAME,
                    KEY_DESCRIBE,
                    KEY_CATEGORY_ID,
                    KEY_CATEGORY_ID,
                    TABLE_NAME,
                    KEY_ID
            );

    private final static Category[] SUBCATEGORIESFRUITS = new Category[]{
            new Category(9, "Banany i owoce egzotyczne", "najlepsze banany", new ArrayList<Category>(), new ArrayList<Product>()),
            new Category(10, "Cytrusy", "cytruski", new ArrayList<Category>(), new ArrayList<Product>()),
            new Category(11, "Jabłka i gruszki", "najlepsze gruchy po tej stronie Euroopy", new ArrayList<Category>(), new ArrayList<Product>()),
            new Category(12, "Winogorna, arbuzy, melony", "to co najlepsze", new ArrayList<Category>(), new ArrayList<Product>()),
            new Category(13, "Owoce miękkie", "owoce najlepsze", new ArrayList<Category>(), new ArrayList<Product>()),
            new Category(14, "Owoce pestkowe", "pestki to nic złego!", new ArrayList<Category>(), new ArrayList<Product>())
    };

    private final static Category[] MAINCATEGORIES = new Category[]{
            new Category(1, "Napoje", "napoje do picia", new ArrayList<Category>(), new ArrayList<Product>()),
            new Category(2, "Pieczywo", "swieże pieczywo", new ArrayList<Category>(), new ArrayList<Product>()),
            new Category(3, "Kosmetyki", "kosmetyki nie tylko dla pań", new ArrayList<Category>(), new ArrayList<Product>()),
            new Category(4, "Święta", "jajka na wielkanoc", new ArrayList<Category>(), new ArrayList<Product>()),
            new Category(5, "Owoce", "świeże owoce", new ArrayList<Category>(), new ArrayList<Product>()),
            new Category(6, "Warzywa", "najświeższe warzywa", new ArrayList<Category>(), new ArrayList<Product>()),
            new Category(7, "Mrożonki", "lodowate mrożonki", new ArrayList<Category>(), new ArrayList<Product>()),
            new Category(8, "Dla dzieci", "zabawki dla dzieci", new ArrayList<Category>(), new ArrayList<Product>())
        };


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpdate(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }

    @Override
    public void prePopulateData(SQLiteDatabase db) {
        db.beginTransaction();

        ContentValues values = new ContentValues();

        for (Category category : MAINCATEGORIES) {
            values.put(KEY_CATEGORY_NAME, category.getName());
            values.put(KEY_DESCRIBE, category.getDescribe());
            db.insert(TABLE_NAME, null, values);
            values.clear();

        }
        for (Category category : MAINCATEGORIES) {
            for (Category subCategory : SUBCATEGORIESFRUITS) {
                values.put(KEY_CATEGORY_NAME, subCategory.getName());
                values.put(KEY_DESCRIBE, subCategory.getDescribe());
                values.put(KEY_CATEGORY_ID, category.getCategoryId());

                db.insert(TABLE_NAME, null, values);
                values.clear();
            }
        }
        db.setTransactionSuccessful();
        db.endTransaction();
    }
}

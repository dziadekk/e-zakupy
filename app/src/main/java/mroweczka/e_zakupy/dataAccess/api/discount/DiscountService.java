package mroweczka.e_zakupy.dataAccess.api.discount;


import java.util.List;

import mroweczka.e_zakupy.entity.Discount;

public interface DiscountService {
    List<Discount> getDiscounts(int clientId);

    Discount getDiscount(int discountId);
}

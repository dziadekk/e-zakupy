package mroweczka.e_zakupy.dataAccess.api.facility;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import mroweczka.e_zakupy.dataAccess.api.ServiceFactory;
import mroweczka.e_zakupy.entity.Town;

public class TownFactory implements ServiceFactory {

    public static final String TABLE_NAME = "Towns";
    public static final String KEY_ID = "key_town_id";
    public static final String KEY_NAME = "key_name";


    private static final String CREATE_TABLE =
            String.format("CREATE TABLE %s (" +
                            "%s INTEGER, " +
                            "%s TEXT, " +
                            "PRIMARY KEY (%s))",
                    TABLE_NAME,
                    KEY_ID,
                    KEY_NAME,
                    KEY_ID
            );



    public static final Town[] TOWNS = new Town[]{
            new Town(1,"Wrocław"),
            new Town(2,"Kraków"),
            new Town(3,"Warszawa"),
            new Town(4,"Gdańsk"),
            new Town(5,"Radomsko"),
            new Town(6,"Częstochowa")
    };

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpdate(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }

    @Override
    public void prePopulateData(SQLiteDatabase db) {
        db.beginTransaction();

        ContentValues values = new ContentValues();

        for (Town town : TOWNS) {
            values.put(KEY_ID,town.getTownId());
            values.put(KEY_NAME, town.getName());
            db.insert(TABLE_NAME, null, values);
            values.clear();
        }

        db.setTransactionSuccessful();
        db.endTransaction();
    }
}

package mroweczka.e_zakupy.dataAccess.api.facility;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import mroweczka.e_zakupy.app.di.qualifier.SyncDB;
import mroweczka.e_zakupy.dataAccess.DBAccess;
import mroweczka.e_zakupy.dataAccess.api.ServiceFactory;
import mroweczka.e_zakupy.entity.Facility;
import mroweczka.e_zakupy.entity.Order;
import mroweczka.e_zakupy.entity.Town;
import mroweczka.e_zakupy.entity.WorkDay;
import mroweczka.e_zakupy.utils.DateUtils;

public class FacilitySyncDBInteractor implements FacilityService {
    private static final String TAG = FacilitySyncDBInteractor.class.getSimpleName();

    public static final String TABLE_NAME = "FACILITIES";
    public static final String KEY_ID = "key_facility_number";
    public static final String KEY_STREET = "key_street";
    public static final String KEY_POSTCODE = "key_postcode";
    public static final String KEY_PHONE = "key_phone";
    public static final String KEY_EMAIL = "key_email";
    public static final String KEY_LONGITIUDE = "key_longitiude";
    public static final String KEY_LATITIUDE = "key_latitiude";
    public static final String KEY_TOWN_ID = "key_town";


    private static final String CREATE_TABLE =
            String.format("CREATE TABLE %s (" +
                            "%s INTEGER, " +
                            "%s TEXT, " +
                            "%s TEXT, " +
                            "%s TEXT," +
                            "%s TEXT," +
                            "%s DOUBLE," +
                            "%s DOUBLE," +
                            "%s INTEGER, " +
                            "FOREIGN KEY (%s) REFERENCES %s(%s) ON DELETE CASCADE, " +
                            "PRIMARY KEY (%s))",
                    TABLE_NAME,
                    KEY_ID,
                    KEY_STREET,
                    KEY_POSTCODE,
                    KEY_PHONE,
                    KEY_EMAIL,
                    KEY_LONGITIUDE,
                    KEY_LATITIUDE,
                    KEY_TOWN_ID,
                    KEY_TOWN_ID,
                    TownFactory.TABLE_NAME,
                    TownFactory.KEY_ID,
                    KEY_ID
            );


    private static final Facility[] FACILITIES = new Facility[]{
            new Facility(1, "Benedygtyńska", "97-505", "Hipermróweczka@mróweczka.pl", new LatLng(51.114699, 17.053492), "518766765", TownFactory.TOWNS[0], new ArrayList<Order>(), new ArrayList<WorkDay>()),
            new Facility(2, "Grunwaldzka", "97-515", "minimróweczka@mróweczka.pl", new LatLng(71.114699, 27.053492), "512678765", TownFactory.TOWNS[1], new ArrayList<Order>(), new ArrayList<WorkDay>()),
            new Facility(3, "Piastowksa", "97-525", "mikromróweczka@mróweczka.pl", new LatLng(61.114699, 37.053492), "616568765", TownFactory.TOWNS[2], new ArrayList<Order>(), new ArrayList<WorkDay>()),
            new Facility(4, "Kościelna", "97-515", "Maksmrówa@mróweczka.pl", new LatLng(11.114699, 67.053492), "616687765", TownFactory.TOWNS[3], new ArrayList<Order>(), new ArrayList<WorkDay>()),
            new Facility(5, "Przyodrzańska", "69-505", "Sredniamrówa@mróweczka.pl", new LatLng(12.114699, 37.053492), "716621765", TownFactory.TOWNS[4], new ArrayList<Order>(), new ArrayList<WorkDay>()),
    };

    private DBAccess synchronizedDBAccess;

    @Inject
    public FacilitySyncDBInteractor(@SyncDB DBAccess synchronizedDBAccess) {
        this.synchronizedDBAccess = synchronizedDBAccess;
    }

    public List<Facility> getFacilities() {
        SQLiteDatabase db = synchronizedDBAccess.getReadableDatabase();

        String query = String.format("SELECT * FROM %s", TABLE_NAME);

        Cursor cursor = db.rawQuery(query, null);
        List<Facility> facilities = new ArrayList<Facility>();


        if (cursor.moveToFirst()) {
            List<WorkDay> workDays = getWorkDays(cursor.getInt(0));
            do {
                Town town = getTown(cursor.getInt(7));
                facilities.add(new Facility(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(4), new LatLng(cursor.getDouble(5), cursor.getDouble(6)), cursor.getString(3), town, null, workDays));
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return facilities;
    }

    @Override
    public List<WorkDay> getWorkDays(int facilityId) {
        SQLiteDatabase db = synchronizedDBAccess.getReadableDatabase();

        String query = String.format("SELECT * FROM %s WHERE %d = %s", WorkDayFactory.TABLE_NAME, facilityId, WorkDayFactory.KEY_FACILITY_ID);

        Cursor cursor = db.rawQuery(query, null);
        List<WorkDay> workDays = new ArrayList<WorkDay>();

        if (cursor.moveToFirst()) {
            do {
                workDays.add(new WorkDay(cursor.getInt(0),
                        DateUtils.getTimeInCommonFormat(cursor.getString(1)),
                        DateUtils.getTimeInCommonFormat(cursor.getString(2)),
                        WorkDay.WeekDay.enumOf(cursor.getInt(3))));
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return workDays;
    }

    @Override
    public Town getTown(int townId) {
        SQLiteDatabase db = synchronizedDBAccess.getReadableDatabase();

        String query = String.format("SELECT * FROM %s WHERE %s = %s", TownFactory.TABLE_NAME, townId, TownFactory.KEY_ID);

        Cursor cursor = db.rawQuery(query, null);
        Town town = null;

        if (cursor.moveToFirst()) {
            do {
                town = new Town(cursor.getInt(0), cursor.getString(1));
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return town;
    }

    //// TODO: 29/01/2017 so bad, ale nie chce sie palowac
    @Override
    public Facility getFacility(int facilityId) {
        for (Facility facility : getFacilities()) {
            if (facility.getId() == facilityId) {
                return facility;
            }
        }
        return null;
    }

    public static ServiceFactory newSyncedDB() {
        return new ServiceFactory() {
            @Override
            public void onCreate(SQLiteDatabase db) {
                db.execSQL(CREATE_TABLE);
            }

            @Override
            public void onUpdate(SQLiteDatabase db) {
                db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            }

            @Override
            public void prePopulateData(SQLiteDatabase db) {
                db.beginTransaction();

                ContentValues values = new ContentValues();
                int rowId = 0;
                for (Facility facility : FACILITIES) {
                    values.put(KEY_ID, facility.getId());
                    values.put(KEY_STREET, facility.getStreet());
                    values.put(KEY_POSTCODE, facility.getPostCode());
                    values.put(KEY_PHONE, facility.getPhoneNumber());
                    values.put(KEY_EMAIL, facility.getEmail());
                    values.put(KEY_LONGITIUDE, facility.getLatLng().longitude);
                    values.put(KEY_LATITIUDE, facility.getLatLng().latitude);
                    values.put(KEY_TOWN_ID, rowId % 5 + 1);
                    db.insert(TABLE_NAME, null, values);
                    values.clear();
                    rowId++;
                }

                db.setTransactionSuccessful();
                db.endTransaction();
            }
        };
    }
}

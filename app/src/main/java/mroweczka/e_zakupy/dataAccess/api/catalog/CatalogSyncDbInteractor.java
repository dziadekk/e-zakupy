package mroweczka.e_zakupy.dataAccess.api.catalog;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import mroweczka.e_zakupy.app.di.qualifier.SyncDB;
import mroweczka.e_zakupy.dataAccess.DBAccess;
import mroweczka.e_zakupy.entity.Category;
import mroweczka.e_zakupy.entity.Country;
import mroweczka.e_zakupy.entity.Manufacturer;
import mroweczka.e_zakupy.entity.Product;
import mroweczka.e_zakupy.entity.PromotionalPrice;
import mroweczka.e_zakupy.entity.Vat;
import mroweczka.e_zakupy.utils.DateUtils;


public class CatalogSyncDbInteractor implements CatalogService{

    private DBAccess synchronizedDBAccess;

    @Inject
    public CatalogSyncDbInteractor(@SyncDB DBAccess synchronizedDBAccess) {
        this.synchronizedDBAccess = synchronizedDBAccess;
    }

    @Override
    public List<Category> getCategories() {

        SQLiteDatabase db = synchronizedDBAccess.getReadableDatabase();

        String query = String.format("SELECT * FROM %s WHERE %s IS  NULL", CategoryFactory.TABLE_NAME, CategoryFactory.KEY_CATEGORY_ID);

        Cursor cursor = db.rawQuery(query, null);
        List<Category> categories = new ArrayList<Category>();

        if (cursor.moveToFirst()) {
            do {
                categories.add(new Category(cursor.getInt(0), cursor.getString(1), cursor.getString(2), null, null));
            } while (cursor.moveToNext());
        }

        cursor.close();

        query = String.format("SELECT * FROM %s WHERE %s IS NOT NULL", CategoryFactory.TABLE_NAME, CategoryFactory.KEY_CATEGORY_ID);

        cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                if (categories.get(cursor.getInt(3) - 1).getSubCategories() == null) {
                    categories.get(cursor.getInt(3) - 1).setSubCategories(new ArrayList<Category>());
                }
                categories.get(cursor.getInt(3) - 1).getSubCategories().add(new Category(cursor.getInt(0), cursor.getString(1), cursor.getString(2), null, null));

            } while (cursor.moveToNext());
        }
        db.close();

        return categories;
    }


    @Override
    public List<Product> getProductsCategory(int categoryId) {
        SQLiteDatabase db = synchronizedDBAccess.getReadableDatabase();

        String query = String.format("SELECT * " +
                        "FROM %s JOIN %s ON %s.%s = %s.%s " +
                        "JOIN %s ON %s.%s = %s.%s " +
                        "JOIN %s ON %s.%s = %s.%s " +
                        "JOIN %s ON %s.%s = %s.%s " +
                        "WHERE %s.%s = %s",
                ProductFactory.TABLE_NAME, ManufacturerFactory.TABLE_NAME, ProductFactory.TABLE_NAME, ProductFactory.KEY_MANUFACTURER_ID, ManufacturerFactory.TABLE_NAME, ManufacturerFactory.KEY_ID,
                CountryFactory.TABLE_NAME, CountryFactory.TABLE_NAME, CountryFactory.KEY_ID, ProductFactory.TABLE_NAME, ProductFactory.KEY_COUNTRY_ID,
                CategoryFactory.TABLE_NAME, CategoryFactory.TABLE_NAME, CategoryFactory.KEY_ID, ProductFactory.TABLE_NAME, ProductFactory.KEY_CATEGORY_ID,
                VatFactory.TABLE_NAME, VatFactory.TABLE_NAME, VatFactory.KEY_ID, ProductFactory.TABLE_NAME, ProductFactory.KEY_VAT_ID,
                ProductFactory.TABLE_NAME, ProductFactory.KEY_CATEGORY_ID, categoryId);

        Cursor cursor = db.rawQuery(query, null);

        List<Product> products = new ArrayList<Product>();
        if (cursor.moveToFirst()) {
            Category category = new Category(cursor.getInt(17), cursor.getString(18), cursor.getString(19), null, new ArrayList<Product>());
            do {
                Country country = new Country(cursor.getInt(15),cursor.getString(16));
                Manufacturer manufacturer = new Manufacturer(cursor.getInt(12),cursor.getString(13),country);
                Vat vat = new Vat(cursor.getInt(21),Vat.VatCategory.enumOf(cursor.getInt(22)),cursor.getInt(23));
                List<PromotionalPrice> promotionalPrice = getPromotionalPrice(cursor.getInt(0));
                products.add(new Product(cursor.getInt(0),cursor.getString(1),cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getDouble(5),cursor.getInt(6), Product.Unit.enumOf(cursor.getInt(7)),manufacturer,country,category,vat,promotionalPrice));
            } while (cursor.moveToNext());

            cursor.close();
            db.close();



        }
        return products;
    }

    @Override
    public List<Vat> getVats() {
        SQLiteDatabase db = synchronizedDBAccess.getReadableDatabase();

        String query = String.format("SELECT * " +
                                    "FROM %s",
                                    VatFactory.TABLE_NAME);

        Cursor cursor = db.rawQuery(query, null);
        List<Vat> vats = new ArrayList<Vat>();

        cursor.close();
        db.close();

        return vats;
    }

    @Override
    public List<Country> getCountry() {
        SQLiteDatabase db = synchronizedDBAccess.getReadableDatabase();

        String query = String.format("SELECT * " +
                        "FROM %s",
                CountryFactory.TABLE_NAME);

        Cursor cursor = db.rawQuery(query, null);
        List<Country> countries = new ArrayList<Country>();

        cursor.close();
        db.close();

        return countries;
    }

    @Override
    public List<Manufacturer> getManufacturers() {
        SQLiteDatabase db = synchronizedDBAccess.getReadableDatabase();

        String query = String.format("SELECT * " +
                        "FROM %s",
                ManufacturerFactory.TABLE_NAME);

        Cursor cursor = db.rawQuery(query, null);
        List<Manufacturer> manufacturers = new ArrayList<Manufacturer>();

        cursor.close();
        db.close();

        return manufacturers;
    }

    @Override
    public List<PromotionalPrice> getPromotionalPrice(int productId) {
        SQLiteDatabase db = synchronizedDBAccess.getReadableDatabase();

        String query = String.format("SELECT * " +
                        "FROM %s WHERE %s = %s",
                PromotionalPriceFactory.TABLE_NAME,
                PromotionalPriceFactory.KEY_PRODUCT_ID, productId);

        Cursor cursor = db.rawQuery(query, null);
        List<PromotionalPrice> promotionalPrices = new ArrayList<PromotionalPrice>();
        if (cursor.moveToFirst()) {
            do {
                promotionalPrices.add(new PromotionalPrice(cursor.getInt(0), DateUtils.getDateInCommonFormat(cursor.getString(1)), DateUtils.getDateInCommonFormat(cursor.getString(2)), cursor.getInt(3), cursor.getDouble(4)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return promotionalPrices;
    }

    @Override
    public Product getProduct(int productId) {
        SQLiteDatabase db = synchronizedDBAccess.getReadableDatabase();

        String query = String.format("SELECT * " +
                        "FROM %s WHERE %s = %s",
                PromotionalPriceFactory.TABLE_NAME,
                PromotionalPriceFactory.KEY_PRODUCT_ID, productId);

        Cursor cursor = db.rawQuery(query, null);
        List<Product> products = getProducts();
        Product product = null;
        for (Product productIter: products) {
            if(productIter.getId() == productId){
                product = productIter;
            }
        }
        cursor.close();
        db.close();

        return product;
    }


    @Override
    public List<Product> getProducts() {
        SQLiteDatabase db = synchronizedDBAccess.getReadableDatabase();

        String query = String.format("SELECT * " +
                        "FROM %s JOIN %s ON %s.%s = %s.%s " +
                        "JOIN %s ON %s.%s = %s.%s " +
                        "JOIN %s ON %s.%s = %s.%s " +
                        "JOIN %s ON %s.%s = %s.%s ",
                ProductFactory.TABLE_NAME, ManufacturerFactory.TABLE_NAME, ProductFactory.TABLE_NAME, ProductFactory.KEY_MANUFACTURER_ID, ManufacturerFactory.TABLE_NAME, ManufacturerFactory.KEY_ID,
                CountryFactory.TABLE_NAME, CountryFactory.TABLE_NAME, CountryFactory.KEY_ID, ProductFactory.TABLE_NAME, ProductFactory.KEY_COUNTRY_ID,
                CategoryFactory.TABLE_NAME, CategoryFactory.TABLE_NAME, CategoryFactory.KEY_ID, ProductFactory.TABLE_NAME, ProductFactory.KEY_CATEGORY_ID,
                VatFactory.TABLE_NAME, VatFactory.TABLE_NAME, VatFactory.KEY_ID, ProductFactory.TABLE_NAME, ProductFactory.KEY_VAT_ID );

        Cursor cursor = db.rawQuery(query, null);

        List<Product> products = new ArrayList<Product>();
        if (cursor.moveToFirst()) {
            Category category = new Category(cursor.getInt(17), cursor.getString(18), cursor.getString(19), null, new ArrayList<Product>());
            do {
                Country country = new Country(cursor.getInt(15),cursor.getString(16));
                Manufacturer manufacturer = new Manufacturer(cursor.getInt(12),cursor.getString(13),country);
                Vat vat = new Vat(cursor.getInt(21),Vat.VatCategory.enumOf(cursor.getInt(22)),cursor.getInt(23));
                List<PromotionalPrice> promotionalPrice = getPromotionalPrice(cursor.getInt(0));
                products.add(new Product(cursor.getInt(0),cursor.getString(1),cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getDouble(5),cursor.getInt(6), Product.Unit.enumOf(cursor.getInt(7)),manufacturer,country,category,vat,promotionalPrice));
            } while (cursor.moveToNext());

            cursor.close();
            db.close();



        }
        return products;
    }
}

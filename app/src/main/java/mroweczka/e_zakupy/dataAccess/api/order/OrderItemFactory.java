package mroweczka.e_zakupy.dataAccess.api.order;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import mroweczka.e_zakupy.dataAccess.api.ServiceFactory;
import mroweczka.e_zakupy.dataAccess.api.catalog.ProductFactory;
import mroweczka.e_zakupy.entity.OrderItem;



public class OrderItemFactory implements ServiceFactory {
    public static final String TABLE_NAME = "OrderItems";
    public static final String KEY_ID = "key_order_item_id";
    public static final String KEY_PRICE = "key_price";
    public static final String KEY_QUANTITY = "key_quantity";
    public static final String KEY_PRODUCT_ID = "key_product_id";
    public static final String KEY_ORDER_ID = "key_order_id";


    private static final String CREATE_TABLE =
            String.format("CREATE TABLE %s (" +
                            "%s INTEGER, " +
                            "%s DOUBLE, " +
                            "%s DOUBLE, " +
                            "%s INTEGER, " +
                            "%s INTEGER, " +
                            "FOREIGN KEY (%s) REFERENCES %s(%s) ON DELETE CASCADE, " +
                            "FOREIGN KEY (%s) REFERENCES %s(%s) ON DELETE CASCADE, " +
                            "PRIMARY KEY (%s))",
                    TABLE_NAME,
                    KEY_ID,
                    KEY_PRICE,
                    KEY_QUANTITY,
                    KEY_PRODUCT_ID,
                    KEY_ORDER_ID,
                    KEY_PRODUCT_ID, ProductFactory.TABLE_NAME,ProductFactory.KEY_ID,
                    KEY_ORDER_ID,OrderFactory.TABLE_NAME,OrderFactory.KEY_ID,
                    KEY_ID
            );



    public static final OrderItem[] ORDERITEMS = new OrderItem[]{
        new OrderItem(1,null,2,3.20),
        new OrderItem(2,null,2,3.20),
        new OrderItem(3,null,2,3.20),
        new OrderItem(4,null,2,3.20),
        new OrderItem(5,null,2,3.20),
        new OrderItem(6,null,2,3.20),
        new OrderItem(7,null,2,3.20),
        new OrderItem(8,null,2,3.20),
        new OrderItem(9,null,2,3.20),
        new OrderItem(10,null,2,3.20),
        new OrderItem(11,null,2,3.20),
        new OrderItem(12,null,2,3.20),
        new OrderItem(13,null,2,3.20),
        new OrderItem(14,null,2,3.20),
        new OrderItem(15,null,2,3.20),
        new OrderItem(16,null,2,3.20),
        new OrderItem(17,null,2,3.20),
        new OrderItem(18,null,2,3.20),
        new OrderItem(19,null,2,3.20),
        new OrderItem(20,null,2,3.20)
    };

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpdate(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }

    @Override
    public void prePopulateData(SQLiteDatabase db) {
        db.beginTransaction();

        ContentValues values = new ContentValues();
        int rowId = 1;
        for (OrderItem orderItem : ORDERITEMS) {
            values.put(KEY_ID,orderItem.getOrderItemId());
            values.put(KEY_PRICE, orderItem.getPrice());
            values.put(KEY_QUANTITY, orderItem.getQuantity());
            values.put(KEY_PRODUCT_ID, rowId%4+1);
            values.put(KEY_ORDER_ID, rowId%4+1);
            db.insert(TABLE_NAME, null, values);
            values.clear();
            rowId++;
        }


        db.setTransactionSuccessful();
        db.endTransaction();
    }
}

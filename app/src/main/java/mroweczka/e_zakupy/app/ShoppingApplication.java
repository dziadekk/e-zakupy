package mroweczka.e_zakupy.app;

import android.app.Application;

import mroweczka.e_zakupy.app.di.component.ApplicationComponent;
import mroweczka.e_zakupy.app.di.component.DaggerApplicationComponent;
import mroweczka.e_zakupy.app.di.module.ApplicationModule;
import mroweczka.e_zakupy.app.di.module.DataModule;


public class ShoppingApplication extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initApplicationComponent();
    }

    private void initApplicationComponent() {
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .dataModule(new DataModule())
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        if (applicationComponent == null) {
            initApplicationComponent();
        }

        return applicationComponent;
    }
}

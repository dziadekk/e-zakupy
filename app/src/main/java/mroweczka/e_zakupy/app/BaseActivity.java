package mroweczka.e_zakupy.app;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import butterknife.ButterKnife;
import mroweczka.e_zakupy.R;
import mroweczka.e_zakupy.app.di.component.ActivityComponent;
import mroweczka.e_zakupy.app.di.component.ApplicationComponent;
import mroweczka.e_zakupy.app.di.component.DaggerActivityComponent;
import mroweczka.e_zakupy.app.dialog.CustomDialogFragment;

public abstract class BaseActivity extends AppCompatActivity {

    protected ActivityComponent activityComponent;

    protected Toolbar toolbar;
    private ProgressDialog progressDialog;

    @LayoutRes
    protected abstract int getLayoutResource();

    protected void initDi() {
        getActivityComponent()
                .inject(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());
        initDi();
        ButterKnife.bind(this);

        toolbar = (Toolbar) findViewById(R.id.view_toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            //noinspection ConstantConditions
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        restartProgressDialog();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopProgressDialog();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public ShoppingApplication getShoppingApplication() {
        return (ShoppingApplication) getApplication();
    }

    protected ApplicationComponent getApplicationComponent() {
        return getShoppingApplication().getApplicationComponent();
    }

    protected ActivityComponent getActivityComponent() {
        if (activityComponent == null) {
            activityComponent = DaggerActivityComponent.builder()
                    .applicationComponent(getApplicationComponent())
                    .build();
        }
        return activityComponent;
    }


    public void showError() {
        showError(getString(R.string.default_error_message));
    }

    public void showError(String message) {
        showDialog(getString(R.string.default_error_title), message);
    }

    public void showError(String title, String message) {
        showDialog(title, message);
    }

    public void showDialog(String title, String message) {
        new CustomDialogFragment.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .create()
                .show(getSupportFragmentManager(), CustomDialogFragment.FRAGMENT_TAG);
    }

    public synchronized void showProgressDialog(@StringRes int resourceId) {
        showProgressDialog(getString(resourceId));
    }

    public void showProgressDialog() {
        showProgressDialog(R.string.default_loading_message);
    }

    public synchronized void showProgressDialog(String message) {
        if ((progressDialog != null && progressDialog.isShowing()) || isFinishing()) {
            return;
        }
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    public synchronized void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    private synchronized void restartProgressDialog() {
        if (progressDialog != null) {
            progressDialog.show();
        }
    }

    private synchronized void stopProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

}

package mroweczka.e_zakupy.app.customView;


import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.annotation.StringRes;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import mroweczka.e_zakupy.R;
import mroweczka.e_zakupy.entity.OrderItem;

public class SummaryItemView extends FrameLayout {
    @BindView(R.id.tv_summary_title)
    TextView tvTitle;
    @BindView(R.id.tv_summary_price)
    TextView tvPrice;
    @BindView(R.id.tv_summary_description_main)
    TextView tvMainDescription;
    @BindView(R.id.tv_summary_description_sub)
    TextView tvSubDescription;

    public SummaryItemView(Context context) {
        super(context);
        initViews(context, null);
    }

    public SummaryItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews(context, attrs);
    }

    public SummaryItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public SummaryItemView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initViews(context, attrs);
    }

    private void initViews(Context context, AttributeSet attrs) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_summary_item, this, true);
        ButterKnife.bind(this, view);

        if (attrs != null) {
            final TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
                    R.styleable.SummaryItemView, 0, 0);

            setTitleText(a.getString(R.styleable.SummaryItemView_textTitle));
            a.recycle();
        }
    }

    public void setTitleText(@StringRes int textId) {
        tvTitle.setText(textId);
    }

    public void setTitleText(String text) {
        tvTitle.setText(text);
    }

    public void setPrice(double price) {
        tvPrice.setText(String.format(OrderItem.DISPLAY_PRICE, price));
    }

    public void setPrice(String price) {
        tvPrice.setText(price);
    }

    public void setMainDescription(String text) {
        tvMainDescription.setVisibility(View.VISIBLE);
        tvMainDescription.setText(text);
    }

    public void setSubDescription(String text) {
        tvSubDescription.setVisibility(View.VISIBLE);
        tvSubDescription.setText(text);
    }

}

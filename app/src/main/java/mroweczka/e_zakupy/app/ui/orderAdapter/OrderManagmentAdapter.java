package mroweczka.e_zakupy.app.ui.orderAdapter;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import mroweczka.e_zakupy.R;
import mroweczka.e_zakupy.entity.Product;
import mroweczka.e_zakupy.entity.orderFlowModels.OrderFlowItem;

public class OrderManagmentAdapter extends BaseAdapter {
    private static final String LITR = " l";
    private static final String PIECES = " szt";
    private static final String KILOGRAM = " kg";
    private final static String CURRENCY = " zł";

    private LayoutInflater inflater;
    private List<OrderFlowItem> flowItems;
    private Map<Integer, Product> products;

    public OrderManagmentAdapter(Activity context, List<OrderFlowItem> flowItems, Map<Integer, Product> products) {
        super();
        this.flowItems = flowItems;
        this.products = products;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     * Metoda zwracająca ilość obiektów w liście
     *
     * @return ilość obiektów w liście
     */
    @Override
    public int getCount() {
        return flowItems.size();
    }

    /**
     * Metoda zwracająca obiekt na konkretnej pozycji
     *
     * @return obiekt znajdujący sie na miejscu, które wyznacza parametr
     * @parameter position indeks obiektu na liście
     */
    @Override
    public Object getItem(int position) {
        return flowItems.get(position);
    }

    /**
     * @param position zwraca pozycje obiektu na liście
     * @return pozycja obiektu na liście
     */
    @Override
    public long getItemId(int position) {

        return position;
    }

    /**
     * Metoda ustawiająca nowe dane
     *
     * @param flowItems lista podkategorii
     */
    public void setData(List<OrderFlowItem> flowItems) {
        this.flowItems = flowItems;
        notifyDataSetChanged();
    }

    /**
     * Metoda ustawiająca widok zgodnie z aktualnymi danymi
     *
     * @param position    pozycja listy
     * @param convertView widok, który konwertujemy
     * @param parent      grupa widoków wszystkich elementów listy
     * @return widok listy z kategoriami
     */
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        OrderFlowItem item = flowItems.get(position);
        View vi = convertView;
        if (convertView == null) {
            vi = inflater.inflate(R.layout.order_item, null);
        }
        Product product = products.get(item.getProductId());
        ImageView ivOrderItemListView = (ImageView) vi.findViewById(R.id.imageViewOrderItemListView);
        ivOrderItemListView.setImageResource(product.getImage());
        TextView tvOrderItemListName = (TextView) vi.findViewById(R.id.textViewOrderItemListName);
        tvOrderItemListName.setText(product.getName());
        TextView tvQuantityOrderItem = (TextView) vi.findViewById(R.id.textViewQuantityOrderItem);
        tvQuantityOrderItem.setText((int) item.getQuantity() + " " + getType(product));
        TextView tvOrderItemPrice = (TextView) vi.findViewById(R.id.textViewOrderItemPrice);
        tvOrderItemPrice.setText(String.format("%1$.2f zł", item.getPrice() * item.getQuantity()));
        TextView tvOrderItemUnit = (TextView) vi.findViewById(R.id.textViewOrderItemUnit);
        tvOrderItemUnit.setText(String.format("(%.2f zł/%s)", item.getPrice(), getType(product)));
        return vi;
    }

    private String getType(Product product) {
        switch (product.getSellUnit()) {
            case Kilogram:
                return KILOGRAM;
            case Pieces:
                return PIECES;
            default:
                return LITR;
        }
    }


}

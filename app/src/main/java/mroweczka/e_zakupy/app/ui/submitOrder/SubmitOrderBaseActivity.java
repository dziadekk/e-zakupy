package mroweczka.e_zakupy.app.ui.submitOrder;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import mroweczka.e_zakupy.R;
import mroweczka.e_zakupy.app.BaseActivity;
import mroweczka.e_zakupy.app.dialog.CustomDialogFragment;
import mroweczka.e_zakupy.app.ui.OrderManagementActivity;
import mroweczka.e_zakupy.entity.orderFlowModels.OrderFlow;
import mroweczka.e_zakupy.presentation.presenter.submitOrder.BaseSubmitOrderPresenter;
import mroweczka.e_zakupy.presentation.view.submitOrder.BaseSubmitOrderView;


public abstract class SubmitOrderBaseActivity extends BaseActivity implements BaseSubmitOrderView {

    protected BaseSubmitOrderPresenter presenter;

    protected OrderFlow orderFlow = new OrderFlow();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item_cancel, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_cancel) {
            createCancelOrderDialog();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void createCancelOrderDialog() {
        new CustomDialogFragment.Builder(this)
                .setTitle(R.string.cancel_order_title)
                .setMessage(R.string.cancel_order_message)
                .setPositiveButton(R.string.confirm, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        presenter.cancelOrder();
                        goToOrderManagementActivity();
                    }
                }, false)
                .setNegativeButton(R.string.go_back, null, false)
                .create()
                .show(getSupportFragmentManager(), CustomDialogFragment.FRAGMENT_TAG);
    }

    protected void goToOrderManagementActivity() {
        Intent intent = new Intent(this, OrderManagementActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(OrderManagementActivity.EXTRA_ORDER_CANCELED, true);
        startActivity(intent);
    }

    protected abstract TextView getPriceTextView();

    @Override
    public void showPrice(double price) {
        getPriceTextView().setText(String.format("Do zapłaty: %.2f zł", price));
    }

    protected Intent prepareContinueIntent(Intent intent) {
        return intent;
    }
}

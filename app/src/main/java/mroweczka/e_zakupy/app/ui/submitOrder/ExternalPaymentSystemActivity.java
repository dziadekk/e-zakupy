package mroweczka.e_zakupy.app.ui.submitOrder;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import mroweczka.e_zakupy.R;
import mroweczka.e_zakupy.app.BaseActivity;
import mroweczka.e_zakupy.entity.Payment;
import mroweczka.e_zakupy.presentation.view.BaseView;

public class ExternalPaymentSystemActivity extends BaseActivity implements BaseView {

    public static final String EXTRA_ORDER_ID = "EXTRA_ORDER_ID";
    public static final String EXTRA_PRICE = "EXTRA_PRICE";
    public static final String EXTRA_PAYMENT_STATUS = "EXTRA_PAYMENT_STATUS";

    @BindView(R.id.tv_price_summary)
    TextView tvPay;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_external_payment_system;
    }

    @Override
    protected void initDi() {
        getActivityComponent()
                .inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tvPay.setText(String.format("Do zapłaty: %.2f", getIntent().getDoubleExtra(EXTRA_PRICE, 0d)));
    }

    @OnClick(R.id.but_payment_confirmed)
    void onPaymentConfirmed() {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_PAYMENT_STATUS, Payment.PaymentStatus.Accepted.getValue());
        setResult(RESULT_OK, intent);
        finish();
    }

    @OnClick(R.id.but_payment_declined)
    void onPaymentDeclined() {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_PAYMENT_STATUS, Payment.PaymentStatus.Denied.getValue());
        setResult(RESULT_OK, intent);
        finish();
    }

}

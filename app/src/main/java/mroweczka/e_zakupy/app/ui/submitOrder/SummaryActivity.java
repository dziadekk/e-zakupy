package mroweczka.e_zakupy.app.ui.submitOrder;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.joda.time.LocalDateTime;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import mroweczka.e_zakupy.R;
import mroweczka.e_zakupy.app.BaseActivity;
import mroweczka.e_zakupy.app.customView.SummaryItemView;
import mroweczka.e_zakupy.app.ui.OrderManagementActivity;
import mroweczka.e_zakupy.entity.Category;
import mroweczka.e_zakupy.entity.Discount;
import mroweczka.e_zakupy.entity.Facility;
import mroweczka.e_zakupy.entity.OrderItem;
import mroweczka.e_zakupy.entity.orderFlowModels.OrderTypeOption;
import mroweczka.e_zakupy.entity.orderFlowModels.PaymentOption;
import mroweczka.e_zakupy.presentation.presenter.submitOrder.SummaryPresenter;
import mroweczka.e_zakupy.presentation.view.submitOrder.SummaryView;
import mroweczka.e_zakupy.utils.DateUtils;

public class SummaryActivity extends BaseActivity implements SummaryView {

    @BindView(R.id.summary_item_facility)
    SummaryItemView summaryItemFacility;
    @BindView(R.id.summary_item_order_type)
    SummaryItemView summaryItemOrderType;
    @BindView(R.id.summary_item_discount)
    SummaryItemView summaryItemDiscount;
    @BindView(R.id.summary_item_payment)
    SummaryItemView summaryItemPayment;
    @BindView(R.id.summary_item_products)
    SummaryItemView summaryItemProducts;
    @BindView(R.id.container_summary_item_product_list)
    LinearLayout containerProductList;
    @BindView(R.id.tv_price_summary)
    TextView tvPriceSummary;
    @BindView(R.id.card_summary)
    CardView cardSummary;

    @Inject
    SummaryPresenter presenter;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_summary;
    }

    @Override
    protected void initDi() {
        getActivityComponent()
                .inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.attachView(this);
        presenter.setup(getIntent().getIntExtra(ChoosePaymentActivity.EXTRA_SUMMARY_ORDER_ID, 0));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, OrderManagementActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void displayFacility(Facility facility) {
        summaryItemFacility.setMainDescription(facility.getTown().getName());
        summaryItemFacility.setSubDescription(facility.getStreet());
    }

    @Override
    public void displayOrderType(OrderTypeOption orderTypeOption) {
        summaryItemOrderType.setMainDescription(getString(orderTypeOption.getTitleRes()));
        summaryItemOrderType.setPrice(orderTypeOption.getPrice());
    }

    @Override
    public void displayOrderRequestedCollectDate(LocalDateTime requestedCollectDate) {
        summaryItemOrderType.setSubDescription(requestedCollectDate.toString(DateUtils.DISPLAY_DATE_TIME_PATTERN));
    }

    @Override
    public void displayDiscount(Discount discount, double discountedValue) {
        summaryItemDiscount.setVisibility(View.VISIBLE);
        summaryItemDiscount.setPrice(discountedValue);
        summaryItemDiscount.setMainDescription(getDiscountTitleForDisplay(discount));
        summaryItemDiscount.setSubDescription(
                String.format("%s: %d %%",
                        getString(R.string.discount_summary),
                        discount.getPercentOfDiscount()));
    }

    private String getDiscountTitleForDisplay(Discount discount) {
        String mainDesc;
        Category discountedCategory = discount.getDiscountedCategory();
        if (discountedCategory == null) {
            mainDesc = getString(R.string.all_inclusive);
        } else {
            mainDesc = String.format("Kategoria: %s", discountedCategory.getName());
        }
        return mainDesc;
    }

    @Override
    public void displayPaymentType(PaymentOption paymentOption) {
        summaryItemPayment.setPrice(paymentOption.getPrice());
        summaryItemPayment.setMainDescription(getString(paymentOption.getTitleRes()));
    }

    @Override
    public void displayOrderItems(List<OrderItem> orderItems, double orderItemsValue) {
        summaryItemProducts.setPrice(orderItemsValue);
        LayoutInflater layoutInflater = getLayoutInflater();
        for (OrderItem orderItem : orderItems) {
            addProductItem(layoutInflater, orderItem);
        }
    }

    private void addProductItem(LayoutInflater layoutInflater, OrderItem orderItem) {
        View view = layoutInflater.inflate(R.layout.layout_summary_item_product, null);
        ((TextView) view.findViewById(R.id.tv_summary_product_name)).setText(orderItem.getProduct().getName());
        ((TextView) view.findViewById(R.id.tv_summary_product_quantity)).setText(orderItem.getQuantityForDisplay());
        ((TextView) view.findViewById(R.id.tv_summary_product_price)).setText(orderItem.getPriceForDisplay());
        containerProductList.addView(view);
    }

    @Override
    public void displayTotalPrice(double price) {
        tvPriceSummary.setText(String.format(getString(R.string.summary_sum), price));
    }

    @Override
    public void showSummary() {
        cardSummary.setVisibility(View.VISIBLE);
    }
}
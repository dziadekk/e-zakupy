package mroweczka.e_zakupy.app.ui.submitOrder;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import mroweczka.e_zakupy.R;
import mroweczka.e_zakupy.app.adapter.OrderOptionsAdapter;
import mroweczka.e_zakupy.app.customView.DividerItemDecoration;
import mroweczka.e_zakupy.app.dialog.OrderDateTimePicker;
import mroweczka.e_zakupy.app.ui.submitOrder.products.orderType.OrderTypeOptionCardItem;
import mroweczka.e_zakupy.app.ui.submitOrder.products.orderType.OrderTypeOptionFactory;
import mroweczka.e_zakupy.domain.submitOrder.IsExpressOrderAvailable;
import mroweczka.e_zakupy.entity.orderFlowModels.OrderTypeOption;
import mroweczka.e_zakupy.presentation.presenter.submitOrder.ChooseOrderTypePresenter;
import mroweczka.e_zakupy.presentation.view.submitOrder.ChooseOrderTypeView;

public class ChooseOrderTypeActivity extends SubmitOrderBaseActivity implements ChooseOrderTypeView, OrderDateTimePicker.OrderDateTimeListener {

    @BindView(R.id.tv_price_summary)
    TextView tvToPay;
    @BindView(R.id.rv_order_types)
    RecyclerView rvOrderTypeOptions;

    @Inject
    ChooseOrderTypePresenter presenter;

    private OrderOptionsAdapter<OrderTypeOptionCardItem> orderOptionsAdapter;
    private LinearLayoutManager rvOrderTypesLinearLayoutManager;
    private OrderDateTimePicker orderDateTimePicker;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_choose_order_type;
    }

    @Override
    protected void initDi() {
        getActivityComponent()
                .inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.presenter = presenter;
        presenter.attachView(this);
    }

    @OnClick(R.id.but_continue)
    void onContinueClicked() {
        OrderTypeOptionCardItem selectedItem = orderOptionsAdapter.getSelectedItem();
        presenter.chooseOrderType(selectedItem != null ? selectedItem.getItem() : null);
    }

    @Override
    public void setupOrderTypeOptionsList() {
        orderOptionsAdapter = new OrderOptionsAdapter<OrderTypeOptionCardItem>(null);
        rvOrderTypeOptions.setAdapter(orderOptionsAdapter);
        rvOrderTypesLinearLayoutManager = new LinearLayoutManager(this);
        rvOrderTypeOptions.setLayoutManager(rvOrderTypesLinearLayoutManager);
        rvOrderTypeOptions.setItemAnimator(null);
        rvOrderTypeOptions.addItemDecoration(new DividerItemDecoration(getResources().getDimensionPixelSize(R.dimen.margin_tiny)));
    }

    @Override
    public void showOrderTypeOptions(List<OrderTypeOption> orderTypeOptions) {
        orderOptionsAdapter.setData(OrderTypeOptionFactory.factoryOrderTypeCardItems(orderTypeOptions));
    }

    @Override
    public void showErrorNoOrderTypePicked() {
        showError(getString(R.string.error_no_order_picked));
    }

    @Override
    public void showErrorExpressOrderUnavailable() {
        showError(getString(R.string.unavailable), String.format(getString(R.string.error_express_facility_unavailable), IsExpressOrderAvailable.EXPRESS_ORDER_MINUTES_TO_FACILITY_CLOSE));
    }

    @Override
    public void continueOrder() {
        Intent intent = new Intent(this, ChooseDiscountActivity.class);
        startActivity(prepareContinueIntent(intent));
    }

    @Override
    protected TextView getPriceTextView() {
        return tvToPay;
    }

    //region Hourly order dialog picker
    @Override
    public void showOrderTimePicker() {
        orderDateTimePicker = OrderDateTimePicker.newInstance();
        orderDateTimePicker.setOrderDateTimeListener(this);
        orderDateTimePicker.show(getSupportFragmentManager(), OrderDateTimePicker.FRAGMENT_TAG);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onSubmit(LocalDate pickedDay, LocalTime pickedTime) {
        Log.d(ChooseOrderTypeActivity.class.getSimpleName(), String.format("%s %s ", pickedDay.toString(), pickedTime.toString()));
        presenter.chooseCollectDate(orderOptionsAdapter.getSelectedItem().getItem(), pickedDay, pickedTime);
    }

    @Override
    public void closeCollectDatePicker() {
        orderDateTimePicker.dismiss();
    }

    @Override
    public void showErrorHourlyOrderUnavailable(String errorMessage) {
        orderDateTimePicker.showError(errorMessage);
    }
    //endregion
}

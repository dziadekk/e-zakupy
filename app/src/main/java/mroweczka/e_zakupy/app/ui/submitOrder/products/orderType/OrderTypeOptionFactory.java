package mroweczka.e_zakupy.app.ui.submitOrder.products.orderType;

import java.util.ArrayList;
import java.util.List;

import mroweczka.e_zakupy.R;
import mroweczka.e_zakupy.entity.Order;
import mroweczka.e_zakupy.entity.orderFlowModels.OrderTypeOption;

public class OrderTypeOptionFactory {

    public static final double ORDER_TYPE_HOURLY_VALUE = 6.60;
    public static final double ORDER_TYPE_EXPRESS_VALUE = 9.99;

    public static OrderTypeOption factoryHourlyOrder() {
        return new OrderTypeOption(Order.OrderType.Hourly, R.string.order_hourly_title, R.string.order_hourly_description, ORDER_TYPE_HOURLY_VALUE);
    }

    public static OrderTypeOption factoryExpressOrder() {
        return new OrderTypeOption(Order.OrderType.Express, R.string.order_express_title, R.string.order_express_description, ORDER_TYPE_EXPRESS_VALUE);
    }

    public static OrderTypeOption factoryCorrectOrderType(Order.OrderType orderType) {
        switch (orderType) {
            case Hourly:
                return factoryHourlyOrder();
            case Express:
                return factoryExpressOrder();

        }
        new IllegalArgumentException("This payment type is not available!");
        return null;

    }

    public static List<OrderTypeOption> factoryAllOrderTypeOptions() {
        List<OrderTypeOption> paymentItems = new ArrayList<>(2);
        paymentItems.add(factoryHourlyOrder());
        paymentItems.add(factoryExpressOrder());
        return paymentItems;
    }

    public static List<OrderTypeOptionCardItem> factoryOrderTypeCardItems(List<OrderTypeOption> orderTypeOptions) {
        List<OrderTypeOptionCardItem> orderTypeOptionCardItems = new ArrayList<>(orderTypeOptions.size());
        for (OrderTypeOption orderTypeOption : orderTypeOptions) {
            orderTypeOptionCardItems.add(new OrderTypeOptionCardItem(orderTypeOption));
        }
        return orderTypeOptionCardItems;
    }
}

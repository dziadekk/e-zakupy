package mroweczka.e_zakupy.app.ui.submitOrder.chooseFacility;

import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import mroweczka.e_zakupy.R;
import mroweczka.e_zakupy.app.adapter.BaseViewHolder;
import mroweczka.e_zakupy.entity.Facility;

public class FacilityDetailsVPAdapter extends PagerAdapter {

    @Nullable
    private List<Facility> listOfFacilities;

    public FacilityDetailsVPAdapter() {
    }

    public void setData(List<Facility> listOfFacilities) {
        this.listOfFacilities = listOfFacilities;
        notifyDataSetChanged();
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Facility facility = listOfFacilities.get(position);
        ViewGroup layout = (ViewGroup) LayoutInflater.from(container.getContext()).inflate(R.layout.layout_vp_facility, container, false);
        BaseViewHolder<Facility> holder = new FacilityViewHolder(layout);
        layout.setTag(holder);
        holder.bind(facility);
        container.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object view) {
        container.removeView((View) view);
    }

    @Override
    public int getCount() {
        return listOfFacilities != null ? listOfFacilities.size() : 0;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @SuppressWarnings("ConstantConditions")
    public int getFacilityId(int position) {
        return listOfFacilities.get(position).getId();
    }

    @SuppressWarnings("ConstantConditions")
    public int findPositionOfFacility(int facilityId) {
        for (int i = 0; i < listOfFacilities.size(); i++) {
            if (listOfFacilities.get(i).getId() == facilityId) {
                return i;
            }
        }
        return -1;
    }

    class FacilityViewHolder extends BaseViewHolder<Facility> {
        @BindView(R.id.tv_facility_city)
        TextView tvCity;
        @BindView(R.id.tv_facility_address)
        TextView tvAddress;

        public FacilityViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void bind(Facility facility) {
            tvCity.setText(facility.getTown().getName());
            tvAddress.setText(facility.getStreet());
        }
    }
}

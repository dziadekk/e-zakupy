package mroweczka.e_zakupy.app.ui.submitOrder.chooseFacility;

import android.content.Context;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import mroweczka.e_zakupy.entity.Facility;

public class MapController implements OnMapReadyCallback, GoogleMap.OnMapLoadedCallback {

    private static final float DEFAULT_CAMERA_ZOOM = 6f;

    private Context context;
    private FacilitiesView facilitiesView;
    private GoogleMap googleMap;
    private Map<Integer, Marker> markersForFacilityId = new HashMap<>();

    @Inject
    public MapController(Context context) {
        this.context = context;
    }

    public void attachView(FacilitiesView facilitiesView, MapFragment mapFragment) {
        this.facilitiesView = facilitiesView;
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        setMapSettings();
        setMapEventsListeners();
    }

    private void setMapSettings() {
        UiSettings settings = googleMap.getUiSettings();
        settings.setMyLocationButtonEnabled(false);
        settings.setTiltGesturesEnabled(false);
        settings.setRotateGesturesEnabled(false);
        settings.setCompassEnabled(false);

        googleMap.setOnMapLoadedCallback(this);
    }

    private void setMapEventsListeners() {
        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                facilitiesView.chooseFacility((Integer) marker.getTag());
                showMarker(marker);
                return true;
            }
        });
    }

    @Override
    public void onMapLoaded() {
        facilitiesView.onMapLoaded();
    }

    private void showMarker(Marker marker) {
        googleMap.stopAnimation();
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), DEFAULT_CAMERA_ZOOM));
    }

    public void showFacilities(List<Facility> facilities) {
        for (Facility facility : facilities) {
            MarkerOptions options = new MarkerOptions()
                    .position(facility.getLatLng())
                    .title(null)
                    .snippet(null)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
            Marker marker = googleMap.addMarker(options);
            marker.setTag(facility.getId());
            markersForFacilityId.put(facility.getId(), marker);
        }
    }

    public void changeFacility(int position) {
        showMarker(markersForFacilityId.get(position));
    }
}

package mroweczka.e_zakupy.app.ui.submitOrder.products.payment;

import mroweczka.e_zakupy.app.ui.submitOrder.products.BaseCardItem;
import mroweczka.e_zakupy.entity.OrderItem;
import mroweczka.e_zakupy.entity.orderFlowModels.PaymentOption;

public class PaymentCardItem extends BaseCardItem<PaymentOption> {

    public PaymentCardItem(PaymentOption item) {
        super(item);
    }

    @Override
    public int getTitleRes() {
        return item.getTitleRes();
    }

    @Override
    public int getDescriptionRes() {
        return item.getDescriptionRes();
    }

    @Override
    public String getSummaryValue() {
        return String.format(OrderItem.DISPLAY_PRICE, item.getPrice());
    }
}

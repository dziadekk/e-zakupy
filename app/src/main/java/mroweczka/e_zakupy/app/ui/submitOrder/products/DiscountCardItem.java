package mroweczka.e_zakupy.app.ui.submitOrder.products;

import mroweczka.e_zakupy.R;
import mroweczka.e_zakupy.entity.Category;
import mroweczka.e_zakupy.entity.Discount;

public class DiscountCardItem extends BaseCardItem<Discount> {
    public DiscountCardItem(Discount item) {
        super(item);
    }

    @Override
    public int getTitleRes() {
        Category discountedCategory = item.getDiscountedCategory();
        return discountedCategory == null ? R.string.all_inclusive : NO_RES_ID;
    }

    @Override
    public String getTitle() {
        return item.getDiscountedCategory().getName();
    }

    @Override
    public int getDescriptionRes() {
        Category discountedCategory = item.getDiscountedCategory();
        return discountedCategory == null ? R.string.all_inclusive_desc : R.string.discount_category_message;
    }

    @Override
    public int getSummaryTitleRes() {
        return R.string.discount_summary;
    }

    @Override
    public String getSummaryValue() {
        return String.format("%d %%", item.getPercentOfDiscount());
    }

    @Override
    public int getActionRes() {
        return R.string.add;
    }

    @Override
    public int getActionSelectedRes() {
        return R.string.added;
    }
}

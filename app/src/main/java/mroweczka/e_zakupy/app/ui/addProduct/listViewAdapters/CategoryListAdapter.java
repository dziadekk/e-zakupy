package mroweczka.e_zakupy.app.ui.addProduct.listViewAdapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import mroweczka.e_zakupy.R;
import mroweczka.e_zakupy.entity.Category;

/**
 * Adapter dla listy kategorii.
 */
public class CategoryListAdapter extends BaseAdapter {


    private LayoutInflater inflater;
    private List<Category> categories;
    private final static String CURRENCY = " zł";

    /**
     * Konstruktor obiektu CategoryListAdapter
     *
     * @param context    kontekst aktynwości
     * @param categories lista kategorii do wyświetlenia
     */
    public CategoryListAdapter(Activity context, List<Category> categories) {
        super();
        this.categories = categories;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     * Metoda zwracająca ilość obiektów w liście
     *
     * @return ilość obiektów w liście
     */
    @Override
    public int getCount() {
        return categories.size();
    }

    /**
     * Metoda zwracająca obiekt na konkretnej pozycji
     * @parameter position indeks obiektu na liście
     * @return obiekt znajdujący sie na miejscu, które wyznacza parametr
     */
    @Override
    public Object getItem(int position) {
        return categories.get(position);
    }

    /**
     *
     * @param position zwraca pozycje obiektu na liście
     * @return pozycja obiektu na liście
     */
    @Override
    public long getItemId(int position) {

        return position;
    }

    /**
     * Metoda ustawiająca nowe dane po przejściu do podkategorii
     *
     * @param categoriesToChange lista podkategorii
     */
    public void setData(List<Category> categoriesToChange) {
        categories = categoriesToChange;
        notifyDataSetChanged();
    }

    /**
     * Metoda ustawiająca widok zgodnie z aktualnymi danymi
     * @param position pozycja listy
     * @param convertView widok, który konwertujemy
     * @param parent grupa widoków wszystkich elementów listy
     * @return widok listy z kategoriami
     */
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Category item = categories.get(position);
        View vi = convertView;
        if (convertView == null) {
            vi = inflater.inflate(R.layout.category_item, null);
        }

        setNameOfCategory(item, vi);

        return vi;
    }

    private void setNameOfCategory(Category item, View vi) {
        TextView categoryName = (TextView) vi.findViewById(R.id.textViewChooseCategoryName);
        categoryName.setText(item.getName());
    }
}

package mroweczka.e_zakupy.app.ui.submitOrder.products.payment;

import java.util.ArrayList;
import java.util.List;

import mroweczka.e_zakupy.R;
import mroweczka.e_zakupy.entity.Payment;
import mroweczka.e_zakupy.entity.orderFlowModels.PaymentOption;

//// TODO: 26/01/2017 think about better solution of creating an Option and mapping to all cards
public class PaymentOptionFactory {

    public static final int PERSONAL_COLLECT_PAYMENT_VALUE = 0;
    public static final double CREDIT_CARD_PAYMENT_VALUE = 1.99;
    public static final double TRANSFER_PAYMENT_VALUE = 1.99;

    public static PaymentOption factoryPaymentPersonalCollect() {
        return new PaymentOption(Payment.PaymentType.PersonalCollect, R.string.payment_personal_title, R.string.payment_personal_description, PERSONAL_COLLECT_PAYMENT_VALUE);
    }

    public static PaymentOption factoryPaymentCC() {
        return new PaymentOption(Payment.PaymentType.CreditCard, R.string.payment_cc_title, R.string.payment_cc_description, CREDIT_CARD_PAYMENT_VALUE);
    }

    public static PaymentOption factoryPaymentTransfer() {
        return new PaymentOption(Payment.PaymentType.BankTransfer, R.string.payment_transfer_title, R.string.payment_transfer_description, TRANSFER_PAYMENT_VALUE);
    }

    public static PaymentOption factoryCorrectPaymentType(Payment.PaymentType paymentType) {
        switch (paymentType) {
            case PersonalCollect:
                return factoryPaymentPersonalCollect();
            case CreditCard:
                return factoryPaymentCC();
            case BankTransfer:
                return factoryPaymentTransfer();
        }
        new IllegalArgumentException("This payment type is not available!");
        return null;
    }

    public static List<PaymentOption> factoryAllPaymentOptions() {
        List<PaymentOption> paymentOptions = new ArrayList<>(3);
        paymentOptions.add(factoryPaymentPersonalCollect());
        paymentOptions.add(factoryPaymentCC());
        paymentOptions.add(factoryPaymentTransfer());
        return paymentOptions;
    }

    public static List<PaymentCardItem> factoryPaymentCardItems(List<PaymentOption> paymentOptions) {
        List<PaymentCardItem> paymentCardItems = new ArrayList<>(paymentOptions.size());
        for (PaymentOption paymentOption : paymentOptions) {
            paymentCardItems.add(new PaymentCardItem(paymentOption));
        }
        return paymentCardItems;
    }
}

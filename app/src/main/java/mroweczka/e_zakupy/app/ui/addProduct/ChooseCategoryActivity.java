package mroweczka.e_zakupy.app.ui.addProduct;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import mroweczka.e_zakupy.R;
import mroweczka.e_zakupy.app.ui.addProduct.listViewAdapters.CategoryListAdapter;
import mroweczka.e_zakupy.entity.Category;
import mroweczka.e_zakupy.presentation.presenter.addProduct.ChooseCategoryPresenter;
import mroweczka.e_zakupy.presentation.view.addProduct.ChooseCategoryView;

/**
 * Aktywność zawierająca listę kategorii
 */
public class ChooseCategoryActivity extends AddProductBaseActivity implements ChooseCategoryView {
    @BindView(R.id.textViewCategory)
    TextView tvCategoryTitle;

    /**
     * Adapter listy produktów
     */
    CategoryListAdapter categoryAdapter;
    @Override
    protected void initDi() {
        getActivityComponent()
                .inject(this);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_choose_category;
    }

    /**
     * Lista produktów
     */
    @BindView(R.id.lwCategoryListView)
    ListView listViewCategory;
    /**
     * Presenter aktywności ChooseCategoryActivity
     */
    @Inject
    ChooseCategoryPresenter presenter;

    private void setAdapterToListView() {
        categoryAdapter = new CategoryListAdapter(this, presenter.getSavedCategories());
        listViewCategory.setAdapter(categoryAdapter);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.presenter = presenter;
        presenter.attachView(this);

        listViewCategory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> av, View v, int index, long arg) {
                presenter.continueAddProductExecute(index, getApplicationContext());
            }
        });

    }

    /**
     * Metoda nadpisująca obsługe przyciksu powrotu do poprzedniego widoku na pasku narzedzi
     *
     * @param item kliknięty obiekt menu
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            presenter.manageBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * Metoda nadpisująca przycisk powrotu przycisk powrotu
     */
    @Override
    public void onBackPressed() {
        presenter.manageBackPressed();
    }

    /**
     * Metoda przechodząca do kolejnej aktywności
     *
     * @param intent intecnja aktywności
     */
    @Override
    public void continueToAnotherActivity(Intent intent) {
        startActivity(intent);
    }

    /**
     * Metoda ustawiająca dane do adapteru listy
     * @param savedCategories zapisane kategorie
     */
    @Override
    public void setDataAdapter(List<Category> savedCategories) {
        categoryAdapter.setData(savedCategories);
    }

    /**
     * Metoda ustawiająca nazwe produktu w widoku
     * @param nameOfChoosedCategory nazwa produktu
     */
    @Override
    public void setCategoryTitleTextView(String nameOfChoosedCategory) {
        tvCategoryTitle.setText(nameOfChoosedCategory);
    }

    /**
     * Metoda ustawiająca widok
     */
    @Override
    public void setup() {
        setAdapterToListView();
    }

    /**
     * Metoda tytuł kategorii z widoku
     * @return
     */
    @Override
    public String getTextFromCategoryLabel() {
        return tvCategoryTitle.getText().toString();
    }

    /**
     * Metoda wywołująca obsługę przycisku powrotu z nadklasy
     */
    @Override
    public void executeOnBackPressed() {
        super.onBackPressed();
    }
}


package mroweczka.e_zakupy.app.ui.addProduct.listViewAdapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.joda.time.LocalDate;

import java.text.DecimalFormat;
import java.util.List;

import mroweczka.e_zakupy.R;
import mroweczka.e_zakupy.entity.Product;
import mroweczka.e_zakupy.entity.PromotionalPrice;


public class ProductListAdapter extends BaseAdapter {

    DecimalFormat decim;

    LayoutInflater inflater;
    List<Product> products;
    private final static String CURRENCY = " zł";
    private final static String FORMAT = "0.00";

    /**
     * Konstruktor adaptera listy produktów
     *
     * @param context  kontekst aktywności
     * @param products
     */
    public ProductListAdapter(Activity context, List<Product> products) {
        super();
        decim = new DecimalFormat(FORMAT);
        this.products = products;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     * Metoda zwracająca ilość obiektów w liście
     *
     * @return ilość obiektów w liście
     */
    @Override
    public int getCount() {
        return products.size();
    }

    /**
     * Metoda zwracająca obiekt na konkretnej pozycji
     * @parameter position indeks obiektu na liście
     * @return obiekt znajdujący sie na miejscu, które wyznacza parametr
     */
    @Override
    public Object getItem(int position) {
        return position;
    }

    /**
     *
     * @param position zwraca pozycje obiektu na liście
     * @return pozycja obiektu na liście
     */
    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Product item = products.get(position);
        View vi = convertView;
        if (convertView == null) {
            vi = inflater.inflate(R.layout.product_item, null);
        }

        setProductImage(item, vi);

        setProductName(item, vi);

        getProductUnitString(item, vi);

        setPrice(item, vi);

        return vi;
    }

    private void setProductImage(Product item, View vi) {
        ImageView imageOfProduct = (ImageView) vi.findViewById(R.id.imageViewProductListView);
        imageOfProduct.setImageResource(item.getImage());
    }

    private void setProductName(Product item, View vi) {
        TextView productName = (TextView) vi.findViewById(R.id.textViewProductListName);
        productName.setText(item.getName());
    }


    private void setPrice(Product item, View vi) {
        TextView promotionalPrice = (TextView) vi.findViewById(R.id.textViewPromotionalPrice);
        TextView unitPrice = (TextView) vi.findViewById(R.id.textViewRegularPrice);
        List<PromotionalPrice> promotionalPrices = item.getPromotionalPrice();
        LocalDate today = new LocalDate();
        boolean find = false;
        for (int i = 0; i < promotionalPrices.size() && !find; i++) {
            if (promotionalPrices.get(i).getValidFrom().compareTo(today) < 0 && promotionalPrices.get(i).getValidUntil().compareTo(today) > 0) {
                promotionalPrice.setText(String.valueOf(item.getUnitPrice()) + CURRENCY);
                promotionalPrice.setPaintFlags(promotionalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                if (promotionalPrices.get(i).getPromotionalPercent() == 0) {
                    unitPrice.setText(String.format("%1$.2f zł", promotionalPrices.get(i).getPriceUnit()));
                } else {
                    unitPrice.setText(String.format("%.2f zł", ((100 - promotionalPrices.get(i).getPromotionalPercent())) * item.getUnitPrice() / 100));
                }
                unitPrice.setTextColor(Color.RED);
                unitPrice.setTextSize(22);
                find = true;
            }
        }

        if (!find) {
            unitPrice.setText(String.format("%1$.2f zł", item.getUnitPrice()));
            promotionalPrice.setVisibility(View.INVISIBLE);
        }

    }

    private void getProductUnitString(Product item, View vi) {
        TextView productUnit = (TextView) vi.findViewById(R.id.textViewUnit);
        switch (item.getSellUnit()) {
            case Kilogram:
                productUnit.setText("x 1 kg");
                break;
            case Pieces:
                productUnit.setText("x 1 szt");
                break;
            default:
                productUnit.setText("x 1 l");
        }
    }

}
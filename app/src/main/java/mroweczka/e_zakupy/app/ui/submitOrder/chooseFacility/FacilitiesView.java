package mroweczka.e_zakupy.app.ui.submitOrder.chooseFacility;


public interface FacilitiesView {
    void chooseFacility(int facilityId);

    void onMapLoaded();
}

package mroweczka.e_zakupy.app.ui.submitOrder;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import mroweczka.e_zakupy.R;
import mroweczka.e_zakupy.app.adapter.OrderOptionsAdapter;
import mroweczka.e_zakupy.app.customView.DividerItemDecoration;
import mroweczka.e_zakupy.app.dialog.CustomDialogFragment;
import mroweczka.e_zakupy.app.ui.OrderManagementActivity;
import mroweczka.e_zakupy.app.ui.submitOrder.products.payment.PaymentCardItem;
import mroweczka.e_zakupy.app.ui.submitOrder.products.payment.PaymentOptionFactory;
import mroweczka.e_zakupy.entity.Payment;
import mroweczka.e_zakupy.entity.orderFlowModels.PaymentOption;
import mroweczka.e_zakupy.presentation.presenter.submitOrder.ChoosePaymentPresenter;
import mroweczka.e_zakupy.presentation.view.submitOrder.ChoosePaymentView;

public class ChoosePaymentActivity extends SubmitOrderBaseActivity implements ChoosePaymentView {

    public static final String EXTRA_SUMMARY_ORDER_ID = "EXTRA_SUMMARY_ORDER_ID";
    public static final int EXTERNAL_PAYMENT_REQUEST_CODE = 101;

    @BindView(R.id.tv_price_summary)
    TextView tvToPay;
    @BindView(R.id.rv_payment_options)
    RecyclerView rvPaymentOptions;

    @Inject
    ChoosePaymentPresenter presenter;

    private OrderOptionsAdapter<PaymentCardItem> orderOptionsAdapter;
    private LinearLayoutManager rvOrderTypesLinearLayoutManager;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_choose_payment;
    }

    @Override
    protected void initDi() {
        getActivityComponent()
                .inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.presenter = presenter;
        presenter.attachView(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == EXTERNAL_PAYMENT_REQUEST_CODE) {
            Payment.PaymentStatus paymentStatus = Payment.PaymentStatus.enumOf(data.getIntExtra(ExternalPaymentSystemActivity.EXTRA_PAYMENT_STATUS, 0));
            presenter.onPaymentResult(paymentStatus);
        }
    }

    @Override
    public void setupPaymentOptionList() {
        orderOptionsAdapter = new OrderOptionsAdapter<PaymentCardItem>(null);
        rvPaymentOptions.setAdapter(orderOptionsAdapter);
        rvOrderTypesLinearLayoutManager = new LinearLayoutManager(this);
        rvPaymentOptions.setLayoutManager(rvOrderTypesLinearLayoutManager);
        rvPaymentOptions.setItemAnimator(null);
        rvPaymentOptions.addItemDecoration(new DividerItemDecoration(getResources().getDimensionPixelSize(R.dimen.margin_tiny)));
    }

    @Override
    public void showPaymentTypes(List<PaymentOption> paymentOptions) {
        orderOptionsAdapter.setData(PaymentOptionFactory.factoryPaymentCardItems(paymentOptions));
    }

    @Override
    public void redirectToPaymentSystem(int orderId, double price) {
        Intent intent = new Intent(this, ExternalPaymentSystemActivity.class);
        intent.putExtra(ExternalPaymentSystemActivity.EXTRA_ORDER_ID, orderId);
        intent.putExtra(ExternalPaymentSystemActivity.EXTRA_PRICE, price);
        startActivityForResult(intent, EXTERNAL_PAYMENT_REQUEST_CODE);
    }

    @Override
    public void goToOrderSummary(int orderId) {
        Intent intent = new Intent(this, SummaryActivity.class);
        intent.putExtra(EXTRA_SUMMARY_ORDER_ID, orderId);
        startActivity(prepareContinueIntent(intent));
    }

    @Override
    public void showPaymentDeclined() {
        new CustomDialogFragment.Builder(this)
                .setTitle(R.string.payment_declined)
                .setMessage(R.string.payment_declined_message)
                .create()
                .show(getSupportFragmentManager(), CustomDialogFragment.FRAGMENT_TAG);
    }

    @Override
    public void showErrorNoPaymentChosen() {
        showError(getString(R.string.error_no_payment_choosen));
    }

    @Override
    public void goToOrderManagementWithDeclinedPaymentError() {
        Intent intent = new Intent(this, OrderManagementActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(OrderManagementActivity.EXTRA_ORDER_CANCELED_PAYMENT, true);
        startActivity(intent);
    }

    @OnClick(R.id.but_continue)
    void onContinueClicked() {
        PaymentCardItem selectedItem = orderOptionsAdapter.getSelectedItem();
        presenter.choosePaymentOption(selectedItem != null ? selectedItem.getItem() : null);
    }

    @Override
    public void continueOrder() {
    }

    @Override
    protected TextView getPriceTextView() {
        return tvToPay;
    }
}
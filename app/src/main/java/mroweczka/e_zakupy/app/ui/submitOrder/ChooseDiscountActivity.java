package mroweczka.e_zakupy.app.ui.submitOrder;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import mroweczka.e_zakupy.R;
import mroweczka.e_zakupy.app.adapter.OrderOptionsAdapter;
import mroweczka.e_zakupy.app.customView.DividerItemDecoration;
import mroweczka.e_zakupy.app.ui.submitOrder.products.DiscountCardItem;
import mroweczka.e_zakupy.entity.Discount;
import mroweczka.e_zakupy.presentation.presenter.submitOrder.ChooseDiscountPresenter;
import mroweczka.e_zakupy.presentation.view.submitOrder.ChooseDiscountView;

public class ChooseDiscountActivity extends SubmitOrderBaseActivity implements ChooseDiscountView {

    @BindView(R.id.tv_price_summary)
    TextView tvToPay;
    @BindView(R.id.rv_discounts)
    RecyclerView rvDiscounts;

    @Inject
    ChooseDiscountPresenter presenter;

    private OrderOptionsAdapter<DiscountCardItem> discountAdapter;
    private LinearLayoutManager rvOrderTypesLinearLayoutManager;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_choose_discount;
    }

    @Override
    protected void initDi() {
        getActivityComponent()
                .inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.presenter = presenter;
        presenter.attachView(this);
    }

    @Override
    public void setupDiscountsList() {
        discountAdapter = new OrderOptionsAdapter<DiscountCardItem>(null);
        rvDiscounts.setAdapter(discountAdapter);
        rvOrderTypesLinearLayoutManager = new LinearLayoutManager(this);
        rvDiscounts.setLayoutManager(rvOrderTypesLinearLayoutManager);
        rvDiscounts.setItemAnimator(null);
        rvDiscounts.addItemDecoration(new DividerItemDecoration(getResources().getDimensionPixelSize(R.dimen.margin_tiny)));

    }

    @Override
    public void showAvailableDiscounts(List<Discount> availableDiscounts) {
        discountAdapter.setData(mapAvailableDiscountsToDiscountItems(availableDiscounts));
    }

    private List<DiscountCardItem> mapAvailableDiscountsToDiscountItems(List<Discount> availableDiscounts) {
        List<DiscountCardItem> discountCardItems = new ArrayList<>(availableDiscounts.size());
        for (Discount availableDiscount : availableDiscounts) {
            discountCardItems.add(new DiscountCardItem(availableDiscount));
        }
        return discountCardItems;
    }

    @OnClick(R.id.but_continue)
    void onContinueClicked() {
        DiscountCardItem selectedItem = discountAdapter.getSelectedItem();
        presenter.chooseDiscount(selectedItem != null ? selectedItem.getItem() : null);
    }

    @Override
    public void continueOrder() {
        Intent intent = new Intent(this, ChoosePaymentActivity.class);
        startActivity(prepareContinueIntent(intent));
    }

    @Override
    protected TextView getPriceTextView() {
        return tvToPay;
    }
}

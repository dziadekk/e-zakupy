package mroweczka.e_zakupy.app.ui.submitOrder.products.orderType;

import mroweczka.e_zakupy.app.ui.submitOrder.products.BaseCardItem;
import mroweczka.e_zakupy.entity.OrderItem;
import mroweczka.e_zakupy.entity.orderFlowModels.OrderTypeOption;

public class OrderTypeOptionCardItem extends BaseCardItem<OrderTypeOption> {

    public OrderTypeOptionCardItem(OrderTypeOption item) {
        super(item);
    }

    @Override
    public int getTitleRes() {
        return item.getTitleRes();
    }

    @Override
    public int getDescriptionRes() {
        return item.getDescriptionRes();
    }

    @Override
    public String getSummaryValue() {
        return String.format(OrderItem.DISPLAY_PRICE, item.getPrice());
    }
}

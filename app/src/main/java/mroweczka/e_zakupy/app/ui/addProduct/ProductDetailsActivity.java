package mroweczka.e_zakupy.app.ui.addProduct;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import mroweczka.e_zakupy.R;
import mroweczka.e_zakupy.app.ui.OrderManagementActivity;
import mroweczka.e_zakupy.presentation.presenter.addProduct.ProductDetailsPresenter;
import mroweczka.e_zakupy.presentation.view.addProduct.ProductDetailsView;

/**
 * Aktywność zawierająca szczegóły produkty
 */
public class ProductDetailsActivity extends AddProductBaseActivity implements ProductDetailsView {

    /**
     * Obiekt zawierający nazwę produktu
     */
    @BindView(R.id.textViewProductName)
    TextView tvProductName;
    /**
     * Obiekt zawierający cene produktu
     */
    @BindView(R.id.textViewProductPrice)
    TextView tvProductPrice;
    /**
     * Obiekt zawierający ilość produktu
     */
    @BindView(R.id.textViewProductQuantity)
    TextView tvProductQuantity;
    /**
     * Obiekt zawierający opis produktu
     */
    @BindView(R.id.textViewProductDescribe)
    TextView tvProductDescribe;
    /**
     * Obiekt zawierający producenta produktu
     */
    @BindView(R.id.textViewManufacturer)
    TextView tvManufacturer;
    /**
     * Obiekt zawierający kraj pochodzenia produktu
     */
    @BindView(R.id.textViewCountry)
    TextView tvCountry;
    /**
     * Obiekt zawierający obrazek produktu
     */
    @BindView(R.id.imageViewProductDetailsImage)
    ImageView imProductDetailsImage;
    /**
     * Presenter aktywności ProductDetailsActivity
     */
    @BindView(R.id.container_product_info)
    LinearLayout containerProductInfo;

    @Inject
    ProductDetailsPresenter presenter;

    @Override
    protected void initDi() {
        getActivityComponent()
                .inject(this);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_product_details;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.presenter = presenter;
        presenter.attachView(this);

    }

    @OnClick(R.id.imageViewPlus)
    void addQuantity() {
        changeQuantity(getQuantityOfProduct() + 1);
    }

    @OnClick(R.id.imageViewMinus)
    void substractQuantity() {
        changeQuantity(getQuantityOfProduct() - 1);
    }

    @OnClick(R.id.but_continue_product_details)
    void continueOrder() {
        presenter.addProduct(getQuantity());
    }

    private Double getQuantity() {
        return Double.valueOf(tvProductQuantity.getText().toString().split("\\s+")[0]);
    }

    private double getQuantityOfProduct() {
        return getQuantity();
    }

    /**
     * Metoda zmieniająca liczność produktu
     *
     * @param newQuantity nowa liczność
     */
    @Override
    public void changeQuantity(double newQuantity) {
        presenter.proccesDiffrence(newQuantity);
    }

    /**
     * Metoda tworząca dialog błędu liczności
     */
    @Override
    public void createErrorQuantityDialog() {
        super.createQuantityErorrderDialog();
    }

    /**
     * Metoda ustawiająca cenę produktu w widoku
     *
     * @param price cena produktu
     */
    @Override
    public void setProductPriceTextView(String price) {
        tvProductPrice.setText(price);
    }

    /**
     * Metoda ustawiająca liczność produktu w widoku
     *
     * @param quantity liczność produktu
     */
    @Override
    public void setProductQuantityTextView(String quantity) {
        tvProductQuantity.setText(quantity);
    }

    /**
     * Metoda ustawiająca nazwę porduktu w widoku
     *
     * @param productName nazwa produktu
     */
    @Override
    public void setProductNameTextView(String productName) {
        tvProductName.setText(productName);
    }

    /**
     * Metoda ustawiająca opis produktu w widoku
     *
     * @param productDescribe opis produktu
     */
    @Override
    public void setProductDescribeTextView(String productDescribe) {
        tvProductDescribe.setText(productDescribe);
    }

    /**
     * Metoda ustawiająca nazwę państwa, z którego pochodzi produkt, w widoku
     *
     * @param productCountry nazwa państwa
     */
    @Override
    public void setCountryTextView(String productCountry) {
        tvCountry.setText(productCountry);
    }

    /**
     * Metoda ustawiająca zdjęcie produktu w widoku
     *
     * @param productImage identyfikator zjęcia
     */
    @Override
    public void setProductDetailsImageImageView(int productImage) {
        imProductDetailsImage.setImageResource(productImage);
    }

    /**
     * Metoda ustawiająca producenta produktu w widoku
     *
     * @param productManufacturer nazwa producenta produktu
     */
    @Override
    public void setProductManufacturerTextView(String productManufacturer) {
        tvManufacturer.setText(productManufacturer);
    }

    @Override
    public void showProductInfo() {
        containerProductInfo.setVisibility(View.VISIBLE);
    }

    @Override
    public void goToOrderManagement() {
        Intent intent = new Intent(this, OrderManagementActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(OrderManagementActivity.EXTRA_PRODUCT_ADDED, true);
        startActivity(intent);
    }
}

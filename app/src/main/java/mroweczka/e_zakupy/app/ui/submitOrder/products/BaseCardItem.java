package mroweczka.e_zakupy.app.ui.submitOrder.products;

import android.support.annotation.StringRes;

import mroweczka.e_zakupy.R;

public abstract class BaseCardItem<T> {
    public static final int NO_RES_ID = -1;

    protected T item;

    public BaseCardItem(T item) {
        this.item = item;
    }

    @StringRes
    public abstract int getTitleRes();

    public String getTitle() {
        return null;
    }

    @StringRes
    public abstract int getDescriptionRes();

    @StringRes
    public int getSummaryTitleRes() {
        return R.string.summary_price;
    }

    @StringRes
    public int getActionRes() {
        return R.string.choose;
    }

    @StringRes
    public int getActionSelectedRes() {
        return R.string.choose_done;
    }

    public abstract String getSummaryValue();

    public T getItem() {
        return item;
    }
}

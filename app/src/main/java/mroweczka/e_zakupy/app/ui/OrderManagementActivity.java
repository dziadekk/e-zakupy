package mroweczka.e_zakupy.app.ui;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import mroweczka.e_zakupy.R;
import mroweczka.e_zakupy.app.BaseActivity;
import mroweczka.e_zakupy.app.ui.addProduct.ChooseCategoryActivity;
import mroweczka.e_zakupy.app.ui.orderAdapter.OrderManagmentAdapter;
import mroweczka.e_zakupy.app.ui.submitOrder.ChooseFacilityActivity;
import mroweczka.e_zakupy.entity.Product;
import mroweczka.e_zakupy.entity.orderFlowModels.OrderFlowItem;
import mroweczka.e_zakupy.presentation.presenter.OrderManagmentPresenter;
import mroweczka.e_zakupy.presentation.view.OrderManagmentView;

public class OrderManagementActivity extends BaseActivity implements OrderManagmentView {

    public static final String EXTRA_ORDER_CANCELED = "EXTRA_ORDER_CANCELED";
    public static final String EXTRA_ORDER_CANCELED_PAYMENT = "EXTRA_ORDER_CANCELED_PAYMENT";
    public static final String EXTRA_ADDING_CANCELED = "EXTRA_ADDING_CANCELED";
    public static final String EXTRA_PRODUCT_ADDED = "EXTRA_PRODUCT_ADDED";


    private OrderManagmentAdapter adapter;

    @BindView(R.id.listViewOrderItems)
    ListView lvOrderItems;
    @BindView(R.id.tv_price_summary)
    TextView tvPriceSummary;
    @Override
    protected int getLayoutResource() {
        return R.layout.activity_order_management;
    }

    @Override
    protected void initDi() {
        getActivityComponent()
                .inject(this);
    }

    @Inject
    OrderManagmentPresenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.attachView(this);

        handleIntent(getIntent());
    }

    private void handleIntent(Intent intent) {
        if (intent.getBooleanExtra(EXTRA_ORDER_CANCELED, false)) {
            showDialog(getString(R.string.order_canceled_title), getString(R.string.order_canceled_message));
        } else if (intent.getBooleanExtra(EXTRA_ORDER_CANCELED_PAYMENT, false)) {
            showDialog(getString(R.string.order_canceled_title), getString(R.string.ordered_canceled_payment_message));
        } else if (intent.getBooleanExtra(EXTRA_ADDING_CANCELED, false)) {
            showDialog(getString(R.string.adding_canceled_title), getString(R.string.adding_canceled_message));
        } else if (intent.getBooleanExtra(EXTRA_PRODUCT_ADDED, false)) {
            showDialog(getString(R.string.product_added_title), getString(R.string.product_added_message));
        }
    }

    @OnClick(R.id.but_continue)
    void onContinueClickedListener() {
        presenter.continueOrder();
    }


    @OnClick(R.id.buttonAddProductToOrder)
    void goToAddProductAcvtivity() {
        Intent intent = new Intent(this, ChooseCategoryActivity.class);
        startActivity(intent);
    }

    @Override
    public void continueOrder() {
        Intent intent = new Intent(this, ChooseFacilityActivity.class);
        startActivity(intent);
    }

    @Override
    public void setPrice(double totalPrice) {
        tvPriceSummary.setText(String.format("Do zapłaty: %.2f zł", totalPrice));
    }


    @Override
    public void setupAdapter(List<OrderFlowItem> orderFlowItems, Map<Integer, Product> productsMap) {
        adapter = new OrderManagmentAdapter(this, orderFlowItems, productsMap);
        lvOrderItems.setAdapter(adapter);

    }

    @Override
    public void createErrorDialog(String titleDialogString, String messageDialogString) {
        super.showDialog(titleDialogString, messageDialogString);
    }


}

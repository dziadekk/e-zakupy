package mroweczka.e_zakupy.app.ui.submitOrder;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.maps.MapFragment;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import mroweczka.e_zakupy.R;
import mroweczka.e_zakupy.app.dialog.CustomDialogFragment;
import mroweczka.e_zakupy.app.ui.submitOrder.chooseFacility.FacilitiesView;
import mroweczka.e_zakupy.app.ui.submitOrder.chooseFacility.FacilityDetailsVPAdapter;
import mroweczka.e_zakupy.app.ui.submitOrder.chooseFacility.MapController;
import mroweczka.e_zakupy.entity.Facility;
import mroweczka.e_zakupy.presentation.presenter.submitOrder.ChooseFacilityPresenter;
import mroweczka.e_zakupy.presentation.view.submitOrder.ChooseFacilityView;

public class ChooseFacilityActivity extends SubmitOrderBaseActivity implements ChooseFacilityView, FacilitiesView {
    @BindView(R.id.vp_facilities)
    ViewPager vpFacilitiesDetails;
    @BindView(R.id.but_facility_previous)
    ImageButton butFacilityPrevious;
    @BindView(R.id.but_facility_next)
    ImageButton butFacilityNext;
    @BindView(R.id.container_facilities_vp)
    FrameLayout containerFacilitiesVp;
    @BindView(R.id.tv_price_summary)
    TextView tvToPay;

    @Inject
    ChooseFacilityPresenter presenter;
    @Inject
    MapController mapController;

    FacilityDetailsVPAdapter adapterFacilityDetails;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_choose_facility;
    }

    @Override
    protected void initDi() {
        getActivityComponent()
                .inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.presenter = presenter;
        presenter.attachView(this);
        mapController.attachView(this, (MapFragment) getFragmentManager().findFragmentById(R.id.map));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            presenter.cancelOrder();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        presenter.cancelOrder();
    }

    @Override
    public void setupFacilityDetails() {
        butFacilityPrevious.setEnabled(false);
        containerFacilitiesVp.setVisibility(View.GONE);
        adapterFacilityDetails = new FacilityDetailsVPAdapter();
        vpFacilitiesDetails.setAdapter(adapterFacilityDetails);
        vpFacilitiesDetails.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mapController.changeFacility(adapterFacilityDetails.getFacilityId(position));
                setAvailableNavigation(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setAvailableNavigation(int currentPosition) {
        butFacilityNext.setEnabled(true);
        butFacilityPrevious.setEnabled(true);
        if (currentPosition == 0) {
            butFacilityPrevious.setEnabled(false);
        } else if (currentPosition == adapterFacilityDetails.getCount() - 1) {
            butFacilityNext.setEnabled(false);
        }
    }

    @OnClick(R.id.but_facility_previous)
    void onPreviousClicked() {
        vpFacilitiesDetails.setCurrentItem(vpFacilitiesDetails.getCurrentItem() - 1);
    }

    @OnClick(R.id.but_facility_next)
    void onNextClicked() {
        vpFacilitiesDetails.setCurrentItem(vpFacilitiesDetails.getCurrentItem() + 1);
    }


    @OnClick(R.id.but_continue)
    void onContinueClicked() {
        presenter.chooseFacility(adapterFacilityDetails.getFacilityId(vpFacilitiesDetails.getCurrentItem()));
    }

    @Override
    public void showFacilities(List<Facility> facilities) {
        containerFacilitiesVp.setVisibility(View.VISIBLE);
        adapterFacilityDetails.setData(facilities);
        mapController.showFacilities(facilities);
    }


    @Override
    public void showAvailabilityError() {
        new CustomDialogFragment.Builder(this)
                .setTitle(R.string.availability)
                .setMessage(R.string.availability_error_message)
                .setPositiveButton(R.string.confirm, null, false)
                .create()
                .show(getSupportFragmentManager(), CustomDialogFragment.FRAGMENT_TAG);
    }

    @Override
    public void continueOrder() {
        Intent intent = new Intent(this, ChooseOrderTypeActivity.class);
        startActivity(prepareContinueIntent(intent));
    }

    @Override
    protected TextView getPriceTextView() {
        return tvToPay;
    }

    //region MapController
    @Override
    public void onMapLoaded() {
        presenter.onViewReady();
    }

    @Override
    public void chooseFacility(int facilityId) {
        vpFacilitiesDetails.setCurrentItem(adapterFacilityDetails.findPositionOfFacility(facilityId));
    }

    //endregion
}
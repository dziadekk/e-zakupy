package mroweczka.e_zakupy.app.ui.addProduct;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import mroweczka.e_zakupy.R;
import mroweczka.e_zakupy.app.ui.addProduct.listViewAdapters.ProductListAdapter;
import mroweczka.e_zakupy.entity.Product;
import mroweczka.e_zakupy.presentation.presenter.addProduct.ProductListPresenter;
import mroweczka.e_zakupy.presentation.view.addProduct.ProductListView;

/**
 * Aktywność zawierająca listę produktów
 */
public class ProductListActivity extends AddProductBaseActivity implements ProductListView {
    @BindView(R.id.listViewProductList)
    ListView lvProductList;
    @Inject
    ProductListPresenter presenter;
    private List<Product> products;

    private static final String CATEGORY_ID_STRING = "categoryId";
    private static final String PRODUCT_ID_STRING = "productId";

    @Override
    protected void initDi() {
        getActivityComponent()
                .inject(this);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_product_list;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.presenter = presenter;
        presenter.attachView(this);

        lvProductList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                continueAddProduct(position);
            }
        });
    }

    /**
     * Metoda ustawiająca widok
     */
    public void setup() {
        setAdapterToListView(getIntent().getExtras().getInt(CATEGORY_ID_STRING));
    }

    private void continueAddProduct(int position) {
        Product product = products.get(position);
        Intent intent = prepareIntent(product);
        startActivity(intent);
    }

    @NonNull
    private Intent prepareIntent(Product product) {
        Intent intent = new Intent(getApplicationContext(), ProductDetailsActivity.class);
        intent.putExtras(prepareBundle(product));
        return intent;
    }

    @NonNull
    private Bundle prepareBundle(Product product) {
        Bundle extras = new Bundle();
        extras.putInt(PRODUCT_ID_STRING, product.getId());
        return extras;
    }

    private void setAdapterToListView(int categoryId) {
        products = presenter.getProductsCategory(categoryId);
        ProductListAdapter productAdapter = new ProductListAdapter(this, products);
        lvProductList.setAdapter(productAdapter);
    }

}

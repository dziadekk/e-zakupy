package mroweczka.e_zakupy.app.ui.addProduct;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import mroweczka.e_zakupy.R;
import mroweczka.e_zakupy.app.BaseActivity;
import mroweczka.e_zakupy.app.dialog.CustomDialogFragment;
import mroweczka.e_zakupy.app.ui.OrderManagementActivity;
import mroweczka.e_zakupy.presentation.presenter.addProduct.BaseAddProductPresenter;
import mroweczka.e_zakupy.presentation.view.addProduct.BaseAddProductView;

/**
 * Bazowa klasa, na której budowane są aktyności zwiazane z dodawaniem produktu
 */
public abstract class AddProductBaseActivity extends BaseActivity implements BaseAddProductView {
    protected BaseAddProductPresenter presenter;

    protected void initDi() {
        getActivityComponent()
                .inject(this);
    }

    /**
     * Metoda tworząca bazową aktywność
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.toolbar.setTitle(R.string.add_product_title);

    }

    /**
     * Metoda tworząca opcje w pasku narzędzi
     *
     * @param menu obiekt reprezentujący menu paska narzędzi
     * @return czy utworzenie menu przebiegło poprawnie
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item_cancel, menu);
        return true;
    }

    /**
     * Metoda obsługująca zdarzenie kliknięcia na którykolwiek przycisk menu
     * @param item kliknięty obiekt menu
     * @return czy wszystko przebiegło pomyślnie
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_cancel) {
            createCancelOrderDialog();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void createCancelOrderDialog() {
        new CustomDialogFragment.Builder(this)
                .setTitle(R.string.cancel_adding_product_title)
                .setMessage(R.string.cancel_adding_product)
                .setPositiveButton(R.string.confirm, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        goToOrderManagementActivity();
                    }
                }, false)
                .setNegativeButton(R.string.go_back, null, false)
                .create()
                .show(getSupportFragmentManager(), CustomDialogFragment.FRAGMENT_TAG);
    }

    protected void createQuantityErorrderDialog() {
        new CustomDialogFragment.Builder(this)
                .setTitle(R.string.bad_quantity_title)
                .setMessage(R.string.bad_quantity)
                .create()
                .show(getSupportFragmentManager(), CustomDialogFragment.FRAGMENT_TAG);
    }

    private void goToOrderManagementActivity() {
        Intent intent = new Intent(this, OrderManagementActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(OrderManagementActivity.EXTRA_ADDING_CANCELED, true);
        startActivity(intent);
    }
}

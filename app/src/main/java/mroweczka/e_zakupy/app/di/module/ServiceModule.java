package mroweczka.e_zakupy.app.di.module;


import dagger.Module;
import dagger.Provides;
import mroweczka.e_zakupy.app.di.qualifier.SyncDB;
import mroweczka.e_zakupy.app.di.scope.PerApplication;
import mroweczka.e_zakupy.dataAccess.DBAccess;
import mroweczka.e_zakupy.dataAccess.api.catalog.CatalogService;
import mroweczka.e_zakupy.dataAccess.api.catalog.CatalogSyncDbInteractor;
import mroweczka.e_zakupy.dataAccess.api.customer.CustomerService;
import mroweczka.e_zakupy.dataAccess.api.customer.CustomerSyncDbInteractor;
import mroweczka.e_zakupy.dataAccess.api.discount.DiscountService;
import mroweczka.e_zakupy.dataAccess.api.discount.DiscountSyncDbInteractor;
import mroweczka.e_zakupy.dataAccess.api.facility.FacilityService;
import mroweczka.e_zakupy.dataAccess.api.facility.FacilitySyncDBInteractor;
import mroweczka.e_zakupy.dataAccess.api.order.OrderService;
import mroweczka.e_zakupy.dataAccess.api.order.OrderSyncDbInteractor;
import mroweczka.e_zakupy.dataAccess.api.payment.PaymentService;
import mroweczka.e_zakupy.dataAccess.api.payment.PaymentSyncDbInteractor;
import mroweczka.e_zakupy.dataAccess.api.storage.StorageService;
import mroweczka.e_zakupy.dataAccess.api.storage.StorageSyncDBInteractor;

@Module
public class ServiceModule {

    @Provides
    @PerApplication
    FacilityService providesFacilityService(@SyncDB DBAccess synchronizedDBAccess) {
        return new FacilitySyncDBInteractor(synchronizedDBAccess);
    }

    @Provides
    @PerApplication
    StorageService provideStorageService(@SyncDB DBAccess synchronizedDBAccess) {
        return new StorageSyncDBInteractor(synchronizedDBAccess);
    }

    @Provides
    @PerApplication
    CatalogService provideCatalogService(@SyncDB DBAccess synchronizedDBAccess) {
        return new CatalogSyncDbInteractor(synchronizedDBAccess);
    }

    @Provides
    @PerApplication
    CustomerService provideCustomerService(@SyncDB DBAccess synchronizedDBAccess) {
        return new CustomerSyncDbInteractor(synchronizedDBAccess);
    }

    @Provides
    @PerApplication
    DiscountService provideDiscountService(@SyncDB DBAccess synchronizedDBAccess) {
        return new DiscountSyncDbInteractor(synchronizedDBAccess);
    }

    @Provides
    @PerApplication
    OrderService provideOrderService(@SyncDB DBAccess synchronizedDBAccess,
                                     PaymentService paymentService,
                                     DiscountService discountService,
                                     FacilityService facilityService,
                                     CatalogService catalogService) {
        return new OrderSyncDbInteractor(synchronizedDBAccess,
                paymentService,
                discountService,
                facilityService,
                catalogService);
    }

    @Provides
    @PerApplication
    PaymentService providePaymentService(@SyncDB DBAccess synchronizedDBAccess) {
        return new PaymentSyncDbInteractor(synchronizedDBAccess);
    }
}
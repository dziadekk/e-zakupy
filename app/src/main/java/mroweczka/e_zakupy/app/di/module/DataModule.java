package mroweczka.e_zakupy.app.di.module;


import android.content.Context;

import dagger.Module;
import dagger.Provides;
import mroweczka.e_zakupy.app.di.qualifier.LocalDB;
import mroweczka.e_zakupy.app.di.qualifier.SyncDB;
import mroweczka.e_zakupy.app.di.scope.PerApplication;
import mroweczka.e_zakupy.dataAccess.DBAccess;
import mroweczka.e_zakupy.dataAccess.api.SynchronizedDBAdapter;
import mroweczka.e_zakupy.dataAccess.localDB.LocalDBAdapter;

@Module
public class DataModule {

    @Provides
    @PerApplication
    LocalDBAdapter providesLocalDBAdapter(Context context) {
        return new LocalDBAdapter(context);
    }

    @Provides
    @PerApplication
    @LocalDB
    DBAccess providesLocalDBAccess(LocalDBAdapter localDBAdapter) {
        return localDBAdapter;
    }

    @Provides
    @PerApplication
    SynchronizedDBAdapter providesSynchronizedDBAdapter(Context context) {
        return new SynchronizedDBAdapter(context);
    }

    @Provides
    @PerApplication
    @SyncDB
    DBAccess providesSynchronizedDBAccess(SynchronizedDBAdapter synchronizedDBAdapter) {
        return synchronizedDBAdapter;
    }

}
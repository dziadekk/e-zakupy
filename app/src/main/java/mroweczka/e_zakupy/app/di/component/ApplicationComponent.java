package mroweczka.e_zakupy.app.di.component;


import android.content.Context;

import dagger.Component;
import mroweczka.e_zakupy.app.di.module.ApplicationModule;
import mroweczka.e_zakupy.app.di.module.DataModule;
import mroweczka.e_zakupy.app.di.module.ServiceModule;
import mroweczka.e_zakupy.app.di.module.StorageModule;
import mroweczka.e_zakupy.app.di.scope.PerApplication;
import mroweczka.e_zakupy.dataAccess.api.catalog.CatalogService;
import mroweczka.e_zakupy.dataAccess.api.customer.CustomerService;
import mroweczka.e_zakupy.dataAccess.api.discount.DiscountService;
import mroweczka.e_zakupy.dataAccess.api.facility.FacilityService;
import mroweczka.e_zakupy.dataAccess.api.order.OrderService;
import mroweczka.e_zakupy.dataAccess.api.payment.PaymentService;
import mroweczka.e_zakupy.dataAccess.api.storage.StorageService;
import mroweczka.e_zakupy.dataAccess.localDB.orderFlow.OrderFlowStorage;

@PerApplication
@Component(
        modules = {
                ApplicationModule.class,
                DataModule.class,
                ServiceModule.class,
                StorageModule.class
        }
)
public interface ApplicationComponent {
    Context getContext();

    //region SyncDB
    FacilityService getFacilityService();

    StorageService getStorageService();

    CatalogService getCatalogService();

    CustomerService getCustomerService();

    DiscountService getDiscountService();

    OrderService getOrderService();

    PaymentService getPaymentService();
    //endregion

    //region LocalDB
    OrderFlowStorage getOrderFlowStorage();
    //endregion
}

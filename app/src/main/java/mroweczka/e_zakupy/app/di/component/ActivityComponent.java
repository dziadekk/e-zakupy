package mroweczka.e_zakupy.app.di.component;


import dagger.Component;
import mroweczka.e_zakupy.app.BaseActivity;
import mroweczka.e_zakupy.app.di.scope.PerActivity;
import mroweczka.e_zakupy.app.ui.OrderManagementActivity;
import mroweczka.e_zakupy.app.ui.addProduct.AddProductBaseActivity;
import mroweczka.e_zakupy.app.ui.addProduct.ChooseCategoryActivity;
import mroweczka.e_zakupy.app.ui.addProduct.ProductDetailsActivity;
import mroweczka.e_zakupy.app.ui.addProduct.ProductListActivity;
import mroweczka.e_zakupy.app.ui.submitOrder.ChooseDiscountActivity;
import mroweczka.e_zakupy.app.ui.submitOrder.ChooseFacilityActivity;
import mroweczka.e_zakupy.app.ui.submitOrder.ChooseOrderTypeActivity;
import mroweczka.e_zakupy.app.ui.submitOrder.ChoosePaymentActivity;
import mroweczka.e_zakupy.app.ui.submitOrder.ExternalPaymentSystemActivity;
import mroweczka.e_zakupy.app.ui.submitOrder.SubmitOrderBaseActivity;
import mroweczka.e_zakupy.app.ui.submitOrder.SummaryActivity;

@PerActivity
@Component(dependencies = {ApplicationComponent.class})
public interface ActivityComponent {
    void inject(BaseActivity activity);

    void inject(OrderManagementActivity activity);

    void inject(SubmitOrderBaseActivity activity);

    void inject(ChooseFacilityActivity activity);

    void inject(ChooseOrderTypeActivity activity);

    void inject(ChooseDiscountActivity activity);

    void inject(ChoosePaymentActivity activity);

    void inject(ExternalPaymentSystemActivity activity);

    void inject(SummaryActivity activity);

    void inject(AddProductBaseActivity activity);

    void inject(ChooseCategoryActivity activity);

    void inject(ProductDetailsActivity activity);

    void inject(ProductListActivity activity);
}
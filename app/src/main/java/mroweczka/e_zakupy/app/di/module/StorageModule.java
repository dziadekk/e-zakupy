package mroweczka.e_zakupy.app.di.module;


import dagger.Module;
import dagger.Provides;
import mroweczka.e_zakupy.app.di.qualifier.LocalDB;
import mroweczka.e_zakupy.app.di.scope.PerApplication;
import mroweczka.e_zakupy.dataAccess.DBAccess;
import mroweczka.e_zakupy.dataAccess.localDB.orderFlow.OrderFlowLocalDbInteractor;
import mroweczka.e_zakupy.dataAccess.localDB.orderFlow.OrderFlowStorage;

@Module
public class StorageModule {

    @Provides
    @PerApplication
    OrderFlowStorage provideStorageService(@LocalDB DBAccess localDBAccess) {
        return new OrderFlowLocalDbInteractor(localDBAccess);
    }
}

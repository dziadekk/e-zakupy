package mroweczka.e_zakupy.app.di.module;


import android.content.Context;

import dagger.Module;
import dagger.Provides;
import mroweczka.e_zakupy.app.ShoppingApplication;
import mroweczka.e_zakupy.app.di.scope.PerApplication;

@Module
public class ApplicationModule {

    private ShoppingApplication application;

    public ApplicationModule(ShoppingApplication application) {
        this.application = application;
    }

    @Provides
    @PerApplication
    ShoppingApplication providesShoppingApplication() {
        return application;
    }

    @Provides
    @PerApplication
    Context providesApplicationContext() {
        return application.getApplicationContext();
    }
}
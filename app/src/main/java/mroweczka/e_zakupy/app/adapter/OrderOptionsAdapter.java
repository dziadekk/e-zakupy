package mroweczka.e_zakupy.app.adapter;


import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import mroweczka.e_zakupy.R;
import mroweczka.e_zakupy.app.ui.submitOrder.products.BaseCardItem;
import mroweczka.e_zakupy.utils.TextUtils;

public class OrderOptionsAdapter<T extends BaseCardItem> extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int NOT_SELECTED_ITEM_INDEX = -1;

    @Nullable
    private OrderProductCardListener<T> orderProductCardListener;
    private int selectedItemIndex = NOT_SELECTED_ITEM_INDEX;
    @Nullable
    private List<T> productItems;

    public OrderOptionsAdapter(@Nullable OrderProductCardListener<T> orderProductCardListener) {
        this.orderProductCardListener = orderProductCardListener;
    }

    public void setData(@Nullable List<T> productItems) {
        this.productItems = productItems;
        notifyDataSetChanged();
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_submit_order_element, parent, false);
        return new OrderProductViewHolder(itemView);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.bind(productItems.get(position));
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return productItems != null ? productItems.size() : 0;
    }

    public boolean isSelected(int position) {
        return selectedItemIndex == position;
    }

    public void onSelectListener(int position) {
        int oldPosition = selectedItemIndex;

        if (position == selectedItemIndex) {
            //clear selection
            selectedItemIndex = NOT_SELECTED_ITEM_INDEX;
        } else {
            //change selection
            selectedItemIndex = position;
            notifyItemChanged(position);

            //notify listener
            if (orderProductCardListener != null) {
                orderProductCardListener.onSelectListener(getSelectedItem());
            }
        }

        //unselect old item
        if (oldPosition != NOT_SELECTED_ITEM_INDEX) {
            notifyItemChanged(oldPosition);
        }
    }

    @SuppressWarnings("ConstantConditions")
    @Nullable
    public T getSelectedItem() {
        return selectedItemIndex >= 0 && selectedItemIndex < getItemCount()
                ? productItems.get(selectedItemIndex)
                : null;
    }

    public class OrderProductViewHolder extends BaseViewHolder<BaseCardItem> {
        public static final String TEXT_SUMMARY_FORMAT = "<b>%s:</b> %s";
        @BindView(R.id.container_card)
        CardView cardView;
        @BindView(R.id.tv_card_title)
        TextView tvCardTitle;
        @BindView(R.id.tv_card_description)
        TextView tvCardDescription;
        @BindView(R.id.tv_card_summary)
        TextView tvCardSummary;
        @BindView(R.id.but_card_action)
        Button butCardAction;

        @OnClick(R.id.container_card)
        void onCardClicked(View v) {
            onSelectListener(getAdapterPosition());
        }

        public OrderProductViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void bind(BaseCardItem element) {
            if (element.getTitleRes() != BaseCardItem.NO_RES_ID) {
                tvCardTitle.setText(element.getTitleRes());
            } else {
                tvCardTitle.setText(element.getTitle());
            }
            tvCardDescription.setText(element.getDescriptionRes());
            tvCardSummary.setText(TextUtils.fromHtml(
                    String.format(TEXT_SUMMARY_FORMAT,
                            context.getString(element.getSummaryTitleRes()),
                            element.getSummaryValue())));

            markSelection(element);
        }

        private void markSelection(BaseCardItem element) {
            boolean selected = isSelected(getAdapterPosition());
            cardView.setSelected(selected);
            butCardAction.setText(selected ? element.getActionSelectedRes() : element.getActionRes());
        }
    }

    /**
     * Listener to notify when item is selected.
     * Won't trigger when item is unselected, only if new item is selected
     *
     * @param <T>
     */
    public interface OrderProductCardListener<T> {
        void onSelectListener(T item);
    }

}

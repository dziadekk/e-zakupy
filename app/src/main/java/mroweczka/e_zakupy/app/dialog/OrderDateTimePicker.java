package mroweczka.e_zakupy.app.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mroweczka.e_zakupy.R;

public class OrderDateTimePicker extends DialogFragment {

    public static final String FRAGMENT_TAG = "FRAGMENT_TAG_ORDER_DATE_TIME_PICKER";

    @BindView(R.id.order_time_picker)
    TimePicker timePickerOrder;
    @BindView(R.id.order_date_picker)
    DatePicker datePickerOrder;
    @BindView(R.id.tv_dialog_error)
    TextView tvDialogError;

    private OrderDateTimeListener orderDateTimeListener;

    public static OrderDateTimePicker newInstance() {
        return new OrderDateTimePicker();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            orderDateTimeListener = (OrderDateTimeListener) getContext();
        } catch (ClassCastException e) {
            throw new ClassCastException(getContext().toString() + " must implement OrderDateTimeListener");
        }
    }

    private View inflateView() {
        final View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_time_picker, null);
        ButterKnife.bind(this, view);
        timePickerOrder.setIs24HourView(true);
        DateTime today = DateTime.now();
        datePickerOrder.init(today.getYear(), today.getMonthOfYear() - 1, today.getDayOfYear(), null);
        return view;
    }

    @SuppressWarnings("deprecation")
    @OnClick(R.id.but_accept_delivery_time)
    void onSubmitListener() {
        LocalTime pickedTime;
        if (Build.VERSION.SDK_INT >= 23) {
            pickedTime = new LocalTime(timePickerOrder.getHour(), timePickerOrder.getMinute());
        } else {
            pickedTime = new LocalTime(timePickerOrder.getCurrentHour(), timePickerOrder.getCurrentMinute());
        }
        LocalDate pickedDay = new LocalDate(datePickerOrder.getYear(),
                datePickerOrder.getMonth() + 1,
                datePickerOrder.getDayOfMonth());
        orderDateTimeListener.onSubmit(pickedDay, pickedTime);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getContext())
                .setView(inflateView())
                .create();
    }

    public void setOrderDateTimeListener(OrderDateTimeListener orderDateTimeListener) {
        this.orderDateTimeListener = orderDateTimeListener;
    }

    public void showError(String message) {
        tvDialogError.setText(message);
        tvDialogError.setVisibility(View.VISIBLE);
    }

    public interface OrderDateTimeListener {
        void onSubmit(LocalDate pickedDay, LocalTime pickedTime);
    }
}

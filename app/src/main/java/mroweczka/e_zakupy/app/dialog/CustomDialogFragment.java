package mroweczka.e_zakupy.app.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mroweczka.e_zakupy.R;

// TODO: 19/01/2017 add possibility to inflate own view
public class CustomDialogFragment extends DialogFragment {

    public static final String FRAGMENT_TAG = "FRAGMENT_TAG_CUSTOM_DIALOG_FRAGMENT";

    private static final String KEY_PREFIX = CustomDialogFragment.class.getSimpleName();

    private static final String ARG_TITLE_KEY = KEY_PREFIX + "title-key";
    private static final String ARG_MESSAGE_KEY = KEY_PREFIX + "message-key";
    private static final String ARG_POSITIVE_BUTTON_TEXT_KEY = KEY_PREFIX + "positive-button-text-key";
    private static final String ARG_POSITIVE_BUTTON_HIGHLIGHT_STYLE_KEY = KEY_PREFIX + "positive-button-highlight-style-key";
    private static final String ARG_NEGATIVE_BUTTON_TEXT_KEY = KEY_PREFIX + "negative-button-text-key";
    private static final String ARG_NEGATIVE_BUTTON_HIGHLIGHT_STYLE_KEY = KEY_PREFIX + "negative-button-highlight-style-key";
    private static final String ARG_NEGATIVE_BUTTON_ENABLED_KEY = KEY_PREFIX + "negative-button-enabled-key";
    private static final String ARG_CANCELABLE_KEY = KEY_PREFIX + "cancelable-key";

    @BindView(R.id.tv_dialog_title)
    TextView tvTitle;
    @BindView(R.id.tv_message)
    TextView tvMessage;
    @BindView(R.id.container_dialog_buttons)
    LinearLayout containerDialogButtons;
    @BindView(R.id.but_positive)
    Button buttonPositive;
    @BindView(R.id.but_negative)
    Button buttonNegative;
    @BindView(R.id.view_button_separator)
    View viewButtonSeparator;

    private String mTitle;
    private String mMessage;
    private String mPositiveButtonText;
    private boolean mPositiveButtonHighlightStyle;
    private View.OnClickListener mPositiveButtonListener;
    private String mNegativeButtonText;
    private boolean mNegativeButtonHighlightStyle;
    private View.OnClickListener mNegativeButtonListener;
    private boolean mCancelable;
    private DialogInterface.OnCancelListener mOnCancelListener;
    private DialogInterface.OnDismissListener mOnDismissListener;
    private boolean isNegativeButtonEnabled;


    // TODO: 19/01/2017 With the setRetainInstance(true) we can remove this bundle save
    // TODO: 19/01/2017 but it would be nice to find a solution to not use it and save listeners somehow
    private static CustomDialogFragment newInstance(
            String title,
            String message,
            String positiveButtonText,
            boolean positiveButtonHighlightStyle,
            View.OnClickListener positiveButtonListener,
            String negativeButtonText,
            boolean negativeButtonHighlightStyle,
            View.OnClickListener negativeButtonListener,
            boolean cancelable,
            DialogInterface.OnCancelListener onCancelListener,
            DialogInterface.OnDismissListener onDismissListener) {

        Bundle bundle = new Bundle();
        //put all arguments
        bundle.putString(ARG_TITLE_KEY, title);
        bundle.putString(ARG_MESSAGE_KEY, message);
        bundle.putString(ARG_POSITIVE_BUTTON_TEXT_KEY, positiveButtonText);
        bundle.putBoolean(ARG_POSITIVE_BUTTON_HIGHLIGHT_STYLE_KEY, positiveButtonHighlightStyle);
        bundle.putBoolean(ARG_CANCELABLE_KEY, cancelable);

        //put arguments determining negative button existence
        boolean isNegativeButtonEnabled = negativeButtonText != null;
        bundle.putBoolean(ARG_NEGATIVE_BUTTON_ENABLED_KEY, isNegativeButtonEnabled);
        if (isNegativeButtonEnabled) {
            bundle.putString(ARG_NEGATIVE_BUTTON_TEXT_KEY, negativeButtonText);
            bundle.putBoolean(ARG_NEGATIVE_BUTTON_HIGHLIGHT_STYLE_KEY, negativeButtonHighlightStyle);
        }

        //create CustomDialogFragment and set listeners
        CustomDialogFragment fragment = new CustomDialogFragment();
        fragment.setArguments(bundle);
        fragment.setRetainInstance(true);
        fragment.setPositiveButtonListener(positiveButtonListener);
        fragment.setNegativeButtonListener(negativeButtonListener);
        fragment.setOnCancelListener(onCancelListener);
        fragment.setOnDismissListener(onDismissListener);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            //decode arguments
            mTitle = args.getString(ARG_TITLE_KEY);
            mMessage = args.getString(ARG_MESSAGE_KEY);
            mPositiveButtonText = args.getString(ARG_POSITIVE_BUTTON_TEXT_KEY);
            mPositiveButtonHighlightStyle = args.getBoolean(ARG_POSITIVE_BUTTON_HIGHLIGHT_STYLE_KEY);
            mCancelable = args.getBoolean(ARG_CANCELABLE_KEY);

            //decode negative button arguments
            isNegativeButtonEnabled = args.getBoolean(ARG_NEGATIVE_BUTTON_ENABLED_KEY);
            if (isNegativeButtonEnabled) {
                mNegativeButtonText = args.getString(ARG_NEGATIVE_BUTTON_TEXT_KEY);
                mNegativeButtonHighlightStyle = args.getBoolean(ARG_NEGATIVE_BUTTON_HIGHLIGHT_STYLE_KEY);
            }
        }
    }

    private View inflateView() {
        final View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_generic, null);
        ButterKnife.bind(this, view);
        tvTitle.setText(mTitle);
        tvMessage.setText(mMessage);

        setButtonAppearance(buttonPositive, mPositiveButtonText, mPositiveButtonHighlightStyle);
        if (isNegativeButtonEnabled) {
            setButtonAppearance(buttonNegative, mNegativeButtonText, mNegativeButtonHighlightStyle);
            buttonNegative.setVisibility(View.VISIBLE);
            viewButtonSeparator.setVisibility(View.VISIBLE);
        }

        return view;
    }

    private void setButtonAppearance(Button button, String buttonText, boolean isHighlightStyle) {
        button.setText(buttonText);
        button.setTextColor(isHighlightStyle
                ? ContextCompat.getColor(getContext(), R.color.colorPrimary)
                : ContextCompat.getColor(getContext(), R.color.white));
        button.setBackgroundResource(isHighlightStyle
                ? R.drawable.button_dialog_highlight_style
                : R.drawable.button_dialog_style);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setCancelable(mCancelable);
        return new AlertDialog.Builder(getContext())
                .setView(inflateView())
                .create();
    }

    @OnClick(R.id.but_positive)
    void onPositiveButtonListener(View view) {
        if (mPositiveButtonListener != null) {
            mPositiveButtonListener.onClick(view);
        }
        dismiss();
    }

    @OnClick(R.id.but_negative)
    void onNegativeButtonListener(View view) {
        if (mNegativeButtonListener != null) {
            mNegativeButtonListener.onClick(view);
        }
        dismiss();
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        if (mOnCancelListener != null) {
            mOnCancelListener.onCancel(dialog);
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mOnDismissListener != null) {
            mOnDismissListener.onDismiss(dialog);
        }
    }

    private void setOnDismissListener(DialogInterface.OnDismissListener mOnDismissListener) {
        this.mOnDismissListener = mOnDismissListener;
    }

    private void setOnCancelListener(DialogInterface.OnCancelListener mOnCancelListener) {
        this.mOnCancelListener = mOnCancelListener;
    }

    private void setNegativeButtonListener(View.OnClickListener mNegativeButtonListener) {
        this.mNegativeButtonListener = mNegativeButtonListener;
    }

    private void setPositiveButtonListener(View.OnClickListener mPositiveButtonListener) {
        this.mPositiveButtonListener = mPositiveButtonListener;
    }

    public static class Builder {
        private final Context context;

        private String title;
        private String message;

        private boolean cancelable = false;

        private String positiveButtonText;
        private boolean positiveButtonHighlightStyle = false;
        private View.OnClickListener positiveButtonListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        };

        private String negativeButtonText;
        private boolean negativeButtonHighlightStyle = false;
        private View.OnClickListener negativeButtonListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        };

        private DialogInterface.OnCancelListener onCancelListener = new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {

            }
        };
        private DialogInterface.OnDismissListener onDismissListener = new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {

            }
        };

        public Builder(Context context) {
            this.context = context;
            title = context.getString(R.string.default_error_title);
            message = context.getString(R.string.default_error_message);
            positiveButtonText = context.getString(R.string.ok);
        }

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setTitle(@StringRes int titleId) {
            this.title = context.getString(titleId);
            return this;
        }

        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder setMessage(@StringRes int messageId) {
            this.message = context.getString(messageId);
            return this;
        }

        public Builder setPositiveButton(String positiveButtonText, final View.OnClickListener positiveButtonListener, boolean highlightStyle) {
            this.positiveButtonText = positiveButtonText;
            this.positiveButtonListener = positiveButtonListener;
            this.positiveButtonHighlightStyle = highlightStyle;
            return this;
        }

        public Builder setPositiveButton(@StringRes int positiveButtonTextId, final View.OnClickListener positiveButtonListener, boolean highlightStyle) {
            return setPositiveButton(context.getString(positiveButtonTextId), positiveButtonListener, highlightStyle);
        }

        public Builder setNegativeButton(String negativeButtonText, final View.OnClickListener negativeButtonListener, boolean highlightStyle) {
            this.negativeButtonText = negativeButtonText;
            this.negativeButtonListener = negativeButtonListener;
            this.negativeButtonHighlightStyle = highlightStyle;
            return this;
        }

        public Builder setNegativeButton(@StringRes int negativeButtonTextId, final View.OnClickListener negativeButtonListener, boolean highlightStyle) {
            return setNegativeButton(context.getString(negativeButtonTextId), negativeButtonListener, highlightStyle);
        }

        public Builder setCancelable(boolean cancelable) {
            this.cancelable = cancelable;
            return this;
        }

        public Builder setOnCancelListener(DialogInterface.OnCancelListener onCancelListener) {
            this.onCancelListener = onCancelListener;
            return this;
        }

        public Builder setOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {
            this.onDismissListener = onDismissListener;
            return this;
        }

        public CustomDialogFragment create() {
            return CustomDialogFragment.newInstance(
                    title,
                    message,
                    positiveButtonText,
                    positiveButtonHighlightStyle,
                    positiveButtonListener,
                    negativeButtonText,
                    negativeButtonHighlightStyle,
                    negativeButtonListener,
                    cancelable,
                    onCancelListener,
                    onDismissListener);
        }
    }
}
package mroweczka.e_zakupy.presentation.view.submitOrder;

import java.util.List;

import mroweczka.e_zakupy.entity.orderFlowModels.OrderTypeOption;

public interface ChooseOrderTypeView extends BaseSubmitOrderView {

    void showProgressDialog();

    void hideProgressDialog();

    void setupOrderTypeOptionsList();

    void showOrderTypeOptions(List<OrderTypeOption> orderTypeOptions);

    void showOrderTimePicker();

    void showErrorNoOrderTypePicked();

    void showErrorExpressOrderUnavailable();

    void closeCollectDatePicker();

    void showErrorHourlyOrderUnavailable(String errorMessage);
}

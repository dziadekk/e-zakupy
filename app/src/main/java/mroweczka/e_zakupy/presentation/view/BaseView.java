package mroweczka.e_zakupy.presentation.view;


public interface BaseView {
    void showError();

    void showError(String title, String message);
}

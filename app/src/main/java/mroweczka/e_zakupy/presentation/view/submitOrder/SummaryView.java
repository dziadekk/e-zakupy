package mroweczka.e_zakupy.presentation.view.submitOrder;

import org.joda.time.LocalDateTime;

import java.util.List;

import mroweczka.e_zakupy.entity.Discount;
import mroweczka.e_zakupy.entity.Facility;
import mroweczka.e_zakupy.entity.OrderItem;
import mroweczka.e_zakupy.entity.orderFlowModels.OrderTypeOption;
import mroweczka.e_zakupy.entity.orderFlowModels.PaymentOption;
import mroweczka.e_zakupy.presentation.view.BaseView;

public interface SummaryView extends BaseView {
    void showProgressDialog();

    void hideProgressDialog();

    void displayFacility(Facility facility);

    void displayOrderType(OrderTypeOption orderTypeOption);

    void displayDiscount(Discount discount, double discountedValue);

    void displayPaymentType(PaymentOption paymentOption);

    void displayOrderItems(List<OrderItem> orderItems, double orderItemsValue);

    void displayTotalPrice(double price);

    void displayOrderRequestedCollectDate(LocalDateTime requestedCollectDate);

    void showSummary();
}

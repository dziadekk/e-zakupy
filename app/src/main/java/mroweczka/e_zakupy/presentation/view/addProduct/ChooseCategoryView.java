package mroweczka.e_zakupy.presentation.view.addProduct;

import android.content.Intent;

import java.util.List;

import mroweczka.e_zakupy.entity.Category;

public interface ChooseCategoryView extends BaseAddProductView{

    void continueToAnotherActivity(Intent intent);

    void setDataAdapter(List<Category> savedCategories);

    void setCategoryTitleTextView(String nameOfChoosedCategory);

    void setup();

    String getTextFromCategoryLabel();

    void executeOnBackPressed();
}

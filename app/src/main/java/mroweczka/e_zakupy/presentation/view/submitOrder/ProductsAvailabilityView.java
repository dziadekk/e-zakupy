package mroweczka.e_zakupy.presentation.view.submitOrder;

public interface ProductsAvailabilityView extends BaseSubmitOrderView {
    void showProgressDialog();

    void hideProgressDialog();

    void showAvailabilityError();
}

package mroweczka.e_zakupy.presentation.view.submitOrder;


import mroweczka.e_zakupy.presentation.view.BaseView;

public interface BaseSubmitOrderView extends BaseView {
    void showPrice(double price);

    void continueOrder();
}

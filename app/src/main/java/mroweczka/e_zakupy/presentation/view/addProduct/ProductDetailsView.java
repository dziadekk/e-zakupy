package mroweczka.e_zakupy.presentation.view.addProduct;


import android.content.Intent;

public interface ProductDetailsView extends BaseAddProductView {

    void changeQuantity(double newQuantity);

    void createErrorQuantityDialog();

    void setProductPriceTextView(String price);

    void setProductQuantityTextView(String quantity);

    void setProductNameTextView(String productName);

    void setProductDescribeTextView(String productDescribe);

    void setCountryTextView(String productCountry);

    void setProductDetailsImageImageView(int productImage);

    void setProductManufacturerTextView(String productManufacturer);

    Intent getIntent();

    void showProgressDialog();

    void hideProgressDialog();

    void showProductInfo();

    void goToOrderManagement();
}

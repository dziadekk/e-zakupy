package mroweczka.e_zakupy.presentation.view.submitOrder;


import java.util.List;

import mroweczka.e_zakupy.entity.Facility;

public interface ChooseFacilityView extends BaseSubmitOrderView {

    void showProgressDialog();

    void hideProgressDialog();

    void showFacilities(List<Facility> facilities);

    void setupFacilityDetails();

    void showAvailabilityError();
}

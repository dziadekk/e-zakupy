package mroweczka.e_zakupy.presentation.view;

import java.util.List;
import java.util.Map;

import mroweczka.e_zakupy.entity.Product;
import mroweczka.e_zakupy.entity.orderFlowModels.OrderFlowItem;


public interface OrderManagmentView extends BaseView {

    void continueOrder();

    void setPrice(double totalPrice);

    void createErrorDialog(String titleDialogString, String messageDialogString);

    void showProgressDialog();

    void hideProgressDialog();

    void setupAdapter(List<OrderFlowItem> orderFlowItems, Map<Integer, Product> productsMap);
}

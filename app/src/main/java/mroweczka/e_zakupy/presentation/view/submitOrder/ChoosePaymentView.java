package mroweczka.e_zakupy.presentation.view.submitOrder;

import java.util.List;

import mroweczka.e_zakupy.entity.orderFlowModels.PaymentOption;

public interface ChoosePaymentView extends BaseSubmitOrderView {

    void setupPaymentOptionList();

    void showPaymentTypes(List<PaymentOption> paymentOptions);

    void showProgressDialog();

    void hideProgressDialog();

    void redirectToPaymentSystem(int orderId, double price);

    void goToOrderSummary(int orderId);

    void showPaymentDeclined();

    void goToOrderManagementWithDeclinedPaymentError();

    void showErrorNoPaymentChosen();
}

package mroweczka.e_zakupy.presentation.view.submitOrder;

import java.util.List;

import mroweczka.e_zakupy.entity.Discount;

public interface ChooseDiscountView extends BaseSubmitOrderView {

    void showProgressDialog();

    void hideProgressDialog();

    void setupDiscountsList();

    void showAvailableDiscounts(List<Discount> availableDiscounts);
}

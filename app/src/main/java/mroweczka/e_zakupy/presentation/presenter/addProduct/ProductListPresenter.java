package mroweczka.e_zakupy.presentation.presenter.addProduct;

import java.util.List;

import javax.inject.Inject;

import mroweczka.e_zakupy.domain.catalog.GetProductsCategory;
import mroweczka.e_zakupy.entity.Product;
import mroweczka.e_zakupy.presentation.view.addProduct.ProductListView;


/**
 * Presenter aktywnosći ProductListPresenter
 */
public class ProductListPresenter extends BaseAddProductPresenter
        implements BaseAddProductPresenterInterface<ProductListView>{
    private ProductListView view;

    /**
     * Metoda przylączająca widok
     *
     * @param view widok do przyłączenia
     */
    public void attachView(ProductListView view) {
        super.attachView(view);
        this.view = view;
        view.setup();
    }

    private GetProductsCategory getProductsCategory;

    @Inject
    public ProductListPresenter(GetProductsCategory getProductsCategory) {
        this.getProductsCategory = getProductsCategory;
    }

    /**
     * Metoda pobierająca porukdty podanej kategorii
     * @param categoryId identyfikator kategorii
     * @return lista produktów kategorii
     */
    public List<Product> getProductsCategory(int categoryId) {
        return getProductsCategory.execute(categoryId);
    }

}

package mroweczka.e_zakupy.presentation.presenter.addProduct;

import mroweczka.e_zakupy.presentation.presenter.BasePresenterInterface;
import mroweczka.e_zakupy.presentation.view.addProduct.BaseAddProductView;

/**
 * Bazowy presenter dla presenterów modułu dodawania produktu
 */
public abstract class BaseAddProductPresenter implements BasePresenterInterface<BaseAddProductView> {
    BaseAddProductView view;
    protected static final String CATEGORY_ID_STRING = "categoryId";

    public BaseAddProductPresenter() {
    }

    @Override
    public void attachView(BaseAddProductView view) {
        this.view = view;
        init();
    }

    private void init() {

    }



    @Override
    public void onStart() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }


}

package mroweczka.e_zakupy.presentation.presenter.submitOrder;


import java.util.List;

import javax.inject.Inject;

import mroweczka.e_zakupy.domain.discount.GetDiscountedPrice;
import mroweczka.e_zakupy.domain.discount.GetDiscounts;
import mroweczka.e_zakupy.domain.storage.UnlockStorageItems;
import mroweczka.e_zakupy.domain.submitOrder.GetOrderFlow;
import mroweczka.e_zakupy.domain.submitOrder.SaveOrderFlow;
import mroweczka.e_zakupy.entity.Discount;
import mroweczka.e_zakupy.presentation.view.submitOrder.ChooseDiscountView;

public class ChooseDiscountPresenter extends BaseSubmitOrderPresenter
        implements BaseSubmitOrderPresenterInterface<ChooseDiscountView> {

    private static final String TAG = ChooseDiscountPresenter.class.getSimpleName();

    private ChooseDiscountView view;

    private GetDiscounts getDiscounts;
    private GetDiscountedPrice getDiscountedPrice;

    @Inject
    public ChooseDiscountPresenter(GetOrderFlow getOrderFlow,
                                   SaveOrderFlow saveOrderFlow,
                                   UnlockStorageItems unlockStorageItems,
                                   GetDiscounts getDiscounts,
                                   GetDiscountedPrice getDiscountedPrice) {
        super(getOrderFlow, saveOrderFlow, unlockStorageItems);
        this.getDiscounts = getDiscounts;
        this.getDiscountedPrice = getDiscountedPrice;
        itemsLockedInMagazine = true;
    }

    public void attachView(ChooseDiscountView view) {
        super.attachView(view);
        this.view = view;
        setupView();
    }

    private void setupView() {
        view.showProgressDialog();
        view.setupDiscountsList();
        List<Discount> availableDiscounts = getDiscounts.execute(orderFlow.getOrderFlowItems());
        if (availableDiscounts != null) {
            view.showAvailableDiscounts(availableDiscounts);
        } else {
            view.continueOrder();
        }
        view.hideProgressDialog();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.view = null;
    }

    public void chooseDiscount(Discount discount) {
        if (discount == null) {
            view.continueOrder();
        } else {
            view.showProgressDialog();
            orderFlow.setDiscountId(discount.getDiscountId());

            double oldPrice = orderFlow.getPrice();//// TODO: 28/01/2017 I have to calculate old items price and add it to new price
            orderFlow.setPrice(getDiscountedPrice.execute(orderFlow.getOrderFlowItems(), discount, oldPrice));
            saveOrderFlow.execute(orderFlow);
            //set old price in order to calculate properly when on back is pressed
            orderFlow.setPrice(oldPrice);

            view.continueOrder();
            view.hideProgressDialog();
        }
    }
}

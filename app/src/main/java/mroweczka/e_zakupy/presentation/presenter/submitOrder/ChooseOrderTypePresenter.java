package mroweczka.e_zakupy.presentation.presenter.submitOrder;


import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;

import javax.inject.Inject;

import mroweczka.e_zakupy.app.ui.submitOrder.products.orderType.OrderTypeOptionFactory;
import mroweczka.e_zakupy.domain.storage.UnlockStorageItems;
import mroweczka.e_zakupy.domain.submitOrder.GetOrderFlow;
import mroweczka.e_zakupy.domain.submitOrder.HasHourlyOrderCorrectTime;
import mroweczka.e_zakupy.domain.submitOrder.IsExpressOrderAvailable;
import mroweczka.e_zakupy.domain.submitOrder.SaveOrderFlow;
import mroweczka.e_zakupy.entity.orderFlowModels.OrderFlowOrderType;
import mroweczka.e_zakupy.entity.orderFlowModels.OrderTypeOption;
import mroweczka.e_zakupy.presentation.view.submitOrder.ChooseOrderTypeView;
import mroweczka.e_zakupy.utils.DateUtils;

public class ChooseOrderTypePresenter extends BaseSubmitOrderPresenter
        implements BaseSubmitOrderPresenterInterface<ChooseOrderTypeView> {

    private static final String TAG = ChooseOrderTypePresenter.class.getSimpleName();

    private ChooseOrderTypeView view;

    private IsExpressOrderAvailable isExpressOrderAvailable;
    private HasHourlyOrderCorrectTime hasHourlyOrderCorrectTime;

    @Inject
    public ChooseOrderTypePresenter(GetOrderFlow getOrderFlow,
                                    SaveOrderFlow saveOrderFlow,
                                    UnlockStorageItems unlockStorageItems,
                                    IsExpressOrderAvailable isExpressOrderAvailable,
                                    HasHourlyOrderCorrectTime hasHourlyOrderCorrectTime) {
        super(getOrderFlow, saveOrderFlow, unlockStorageItems);
        this.isExpressOrderAvailable = isExpressOrderAvailable;
        this.hasHourlyOrderCorrectTime = hasHourlyOrderCorrectTime;
        itemsLockedInMagazine = true;
    }

    public void attachView(ChooseOrderTypeView view) {
        super.attachView(view);
        this.view = view;
        setupView();
    }

    private void setupView() {
        view.setupOrderTypeOptionsList();
        view.showOrderTypeOptions(OrderTypeOptionFactory.factoryAllOrderTypeOptions());
    }

    public void chooseOrderType(OrderTypeOption orderTypeOption) {
        if (orderTypeOption == null) {
            view.showErrorNoOrderTypePicked();
            return;
        }
        switch (orderTypeOption.getOrderType()) {
            case Express:
                if (isExpressOrderAvailable.execute(orderFlow.getFacilityId())) {
                    pickOrderType(orderTypeOption, null);
                } else {
                    view.showErrorExpressOrderUnavailable();
                }
                break;
            case Hourly:
                view.showOrderTimePicker();
                break;
            default:
                new IllegalArgumentException("This kind of order type doesn't exist");
        }
    }


    public void chooseCollectDate(OrderTypeOption item, LocalDate pickedDay, LocalTime pickedTime) {
        String orderAvailability = hasHourlyOrderCorrectTime.execute(orderFlow.getFacilityId(), pickedDay, pickedTime);
        if (orderAvailability == null) {
            view.closeCollectDatePicker();
            pickOrderType(item, DateUtils.mergeDateAndTime(pickedDay, pickedTime));
        } else {
            view.showErrorHourlyOrderUnavailable(orderAvailability);
        }
    }

    private void pickOrderType(OrderTypeOption orderTypeOption, LocalDateTime requestedCollectDate) {
        view.showProgressDialog();
        orderFlow.setOrderFlowOrderType(new OrderFlowOrderType(requestedCollectDate));


        double oldPrice = orderFlow.getPrice();
        orderFlow.setPrice(oldPrice + orderTypeOption.getPrice());
        saveOrderFlow.execute(orderFlow);
        //set old price in order to calculate properly when on back is pressed
        orderFlow.setPrice(oldPrice);

        view.continueOrder();
        view.hideProgressDialog();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.view = null;
    }
}

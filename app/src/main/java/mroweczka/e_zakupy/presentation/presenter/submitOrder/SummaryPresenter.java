package mroweczka.e_zakupy.presentation.presenter.submitOrder;


import android.os.AsyncTask;

import java.util.List;

import javax.inject.Inject;

import mroweczka.e_zakupy.app.ui.submitOrder.products.orderType.OrderTypeOptionFactory;
import mroweczka.e_zakupy.app.ui.submitOrder.products.payment.PaymentOptionFactory;
import mroweczka.e_zakupy.domain.submitOrder.GetOrder;
import mroweczka.e_zakupy.entity.Order;
import mroweczka.e_zakupy.entity.OrderItem;
import mroweczka.e_zakupy.entity.Payment;
import mroweczka.e_zakupy.entity.orderFlowModels.OrderTypeOption;
import mroweczka.e_zakupy.entity.orderFlowModels.PaymentOption;
import mroweczka.e_zakupy.presentation.presenter.BasePresenterInterface;
import mroweczka.e_zakupy.presentation.view.submitOrder.SummaryView;

public class SummaryPresenter implements BasePresenterInterface<SummaryView> {

    private static final String TAG = SummaryPresenter.class.getSimpleName();

    private SummaryView view;

    private GetOrder getOrder;

    private Order order;

    @Inject
    public SummaryPresenter(GetOrder getOrder) {
        this.getOrder = getOrder;
    }

    @Override
    public void attachView(SummaryView view) {
        this.view = view;
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {
        this.view = null;
    }

    public void setup(int orderId) {
        new AsyncGetOrder().execute(orderId);
    }

    private double getTotalPriceOfOrderItems(List<OrderItem> orderItems) {
        double totalPrice = 0;
        for (OrderItem orderItem : orderItems) {
            totalPrice += orderItem.getPrice() * orderItem.getQuantity();
        }
        return totalPrice;
    }

    private class AsyncGetOrder extends AsyncTask<Integer, Void, Order> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            view.showProgressDialog();
        }

        @Override
        protected Order doInBackground(Integer... integers) {
            return getOrder.execute(integers[0]);
        }

        @Override
        protected void onPostExecute(Order order) {
            SummaryPresenter.this.order = order;
            OrderTypeOption orderTypeOption = OrderTypeOptionFactory.factoryCorrectOrderType(order.getOrderType());
            PaymentOption paymentOption = PaymentOptionFactory.factoryCorrectPaymentType(
                    order.getPayment() == null
                            ? Payment.PaymentType.PersonalCollect
                            : order.getPayment().getPaymentType());
            double orderItemsValue = getTotalPriceOfOrderItems(order.getOrderItems());
            double discountedValue = order.getPrice() - orderTypeOption.getPrice() - paymentOption.getPrice() - orderItemsValue;

            view.displayFacility(order.getFacility());
            view.displayOrderType(orderTypeOption);
            if (order.getRequestedCollectDate() != null) {
                view.displayOrderRequestedCollectDate(order.getRequestedCollectDate());
            }
            if (order.getDiscount() != null) {
                view.displayDiscount(order.getDiscount(), discountedValue);
            }
            view.displayPaymentType(paymentOption);
            view.displayOrderItems(order.getOrderItems(), orderItemsValue);
            view.displayTotalPrice(order.getPrice());
            view.showSummary();
            view.hideProgressDialog();
        }
    }
}

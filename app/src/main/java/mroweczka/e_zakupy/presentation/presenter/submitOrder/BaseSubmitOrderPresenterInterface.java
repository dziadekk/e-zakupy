package mroweczka.e_zakupy.presentation.presenter.submitOrder;

import mroweczka.e_zakupy.presentation.presenter.BasePresenterInterface;
import mroweczka.e_zakupy.presentation.view.submitOrder.BaseSubmitOrderView;

public interface BaseSubmitOrderPresenterInterface<T extends BaseSubmitOrderView>
        extends BasePresenterInterface<BaseSubmitOrderView> {

}

package mroweczka.e_zakupy.presentation.presenter.submitOrder;

import android.util.Log;

import mroweczka.e_zakupy.domain.storage.UnlockStorageItems;
import mroweczka.e_zakupy.domain.submitOrder.GetOrderFlow;
import mroweczka.e_zakupy.domain.submitOrder.SaveOrderFlow;
import mroweczka.e_zakupy.entity.orderFlowModels.OrderFlow;
import mroweczka.e_zakupy.presentation.presenter.BasePresenterInterface;
import mroweczka.e_zakupy.presentation.view.submitOrder.BaseSubmitOrderView;

public abstract class BaseSubmitOrderPresenter implements BasePresenterInterface<BaseSubmitOrderView> {

    private static final String TAG = BaseSubmitOrderPresenter.class.getSimpleName();

    private BaseSubmitOrderView view;

    protected GetOrderFlow getOrderFlow;
    protected SaveOrderFlow saveOrderFlow;
    protected UnlockStorageItems unlockStorageItems;
    protected OrderFlow orderFlow;

    protected boolean itemsLockedInMagazine;

    public BaseSubmitOrderPresenter(GetOrderFlow getOrderFlow, SaveOrderFlow saveOrderFlow, UnlockStorageItems unlockStorageItems) {
        this.getOrderFlow = getOrderFlow;
        this.saveOrderFlow = saveOrderFlow;
        this.unlockStorageItems = unlockStorageItems;
    }

    //// TODO: 21/01/2017 how to make a child force implement attach view ??
    @Override
    public void attachView(BaseSubmitOrderView view) {
        this.view = view;
        init();
    }

    private void init() {
        orderFlow = getOrderFlow.execute();
        view.showPrice(orderFlow.getPrice());
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {
        this.view = null;
    }

    public void cancelOrder() {
        Log.d(TAG, "Order canceled");
        if (itemsLockedInMagazine) {
            Log.d(TAG, "Items are being unlocked from the magazine");
            unlockStorageItems.execute(orderFlow.getFacilityId(), orderFlow.getOrderFlowItems());
        }
    }
}

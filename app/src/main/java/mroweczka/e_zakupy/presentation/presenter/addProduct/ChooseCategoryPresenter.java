package mroweczka.e_zakupy.presentation.presenter.addProduct;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import java.util.List;
import java.util.Stack;

import javax.inject.Inject;

import mroweczka.e_zakupy.app.ui.addProduct.ProductListActivity;
import mroweczka.e_zakupy.domain.catalog.GetCategories;
import mroweczka.e_zakupy.entity.Category;
import mroweczka.e_zakupy.presentation.view.addProduct.ChooseCategoryView;

/**
 *Presenter aktywnosći ChooseCategoryActivity
 */
public class ChooseCategoryPresenter extends BaseAddProductPresenter
        implements BaseAddProductPresenterInterface<ChooseCategoryView> {
    private static final String CATEGORY_ID_STRING = "categoryId";

    //TODO nie wiem jak ująć te domenowe
    @Inject
    public ChooseCategoryPresenter(GetCategories getCategories) {
        this.getCategories = getCategories;
        this.stackList = new Stack<>();
        this.stackCategoryNames = new Stack<>();
    }

    private Stack<List<Category>> stackList;
    private Stack<String> stackCategoryNames;

    /**
     * Metoda pobierająca liste zapisnaych kategorii
     *
     * @return lista zapisanych podkategorii
     */
    public List<Category> getSavedCategories() {
        return savedCategories;
    }

    List<Category> savedCategories;
    private GetCategories getCategories;

    /**
     * Metoda inicijalizująca nowy stan zapisanych kategorii
     */
    public void getCategories() {
        savedCategories = getCategories.execute();
    }

    ChooseCategoryView view;

    /**
     * Metoda przylączająca widok
     * @param view widok do przyłączenia
     */
    public void attachView(ChooseCategoryView view) {
        super.attachView(view);
        this.view = view;
        getCategories();
        view.setup();
    }

    /**
     * Metoda wybierająca podkategorie wskazanej kateogrii
     *
     * @param index indeks kategorii w liście
     * @return identyfikator kategorii
     */
    public Integer continueChooseCategory(int index) {
        Category tempCategory = savedCategories.get(index);
        stackList.push(savedCategories);
        stackCategoryNames.push(view.getTextFromCategoryLabel());
        int categoryId = tempCategory.getCategoryId();
        return categoryId;
    }

    /**
     * Metoda przygotowująca intencje aktywności
     *
     * @param categoryId identyfikator kategorii
     * @param appContext kontekst aktywności
     * @return intencja aktywności ProductListActivity
     */
    public Intent prepareIntent(int categoryId, Context appContext) {
        Intent intent = new Intent(appContext, ProductListActivity.class);
        intent.putExtras(prepareBundle(categoryId));
        return intent;
    }

    /**
     * Jetoda przygotowująca dane do przekazania do następnej aktywności
     *
     * @param categoryId identyfikator kategorii
     * @return dane do przekazania do następnej aktywności
     */
    public Bundle prepareBundle(int categoryId) {
        Bundle extras = new Bundle();
        extras.putInt(CATEGORY_ID_STRING, categoryId);
        return extras;
    }

    /**
     * Metoda obsługująca wybranie kategorii
     *
     * @param elementIndex indeks w liście wybranej kategorii
     * @param appContext   kontekst aplikacji
     */
    public void continueAddProductExecute(int elementIndex, Context appContext) {
        String nameOfChoosedCategory = getSavedCategories().get(elementIndex).getName();
        int categoryId = continueChooseCategory(elementIndex);
        if (savedCategories.get(elementIndex).getSubCategories() == null) {
            Intent intent = prepareIntent(categoryId, appContext);
            view.continueToAnotherActivity(intent);
            stackList.pop();
            stackCategoryNames.pop();
        } else {
            savedCategories = savedCategories.get(elementIndex).getSubCategories();
            view.setDataAdapter(getSavedCategories());
            view.setCategoryTitleTextView(nameOfChoosedCategory);
        }
    }

    /**
     * Metoda obsługująca przycisk powrotu
     */
    public void manageBackPressed() {
        if (!stackList.isEmpty()) {
            savedCategories = stackList.pop();
            view.setDataAdapter(savedCategories);
            view.setCategoryTitleTextView(stackCategoryNames.pop());
        } else {
            view.executeOnBackPressed();
        }


    }
}

package mroweczka.e_zakupy.presentation.presenter.submitOrder;

import android.util.Log;

import java.util.List;

import javax.inject.Inject;

import mroweczka.e_zakupy.domain.facility.GetFacilities;
import mroweczka.e_zakupy.domain.storage.CheckProductsAvailability;
import mroweczka.e_zakupy.domain.storage.LockStorageItems;
import mroweczka.e_zakupy.domain.storage.UnlockStorageItems;
import mroweczka.e_zakupy.domain.submitOrder.GetOrderFlow;
import mroweczka.e_zakupy.domain.submitOrder.SaveOrderFlow;
import mroweczka.e_zakupy.entity.Facility;
import mroweczka.e_zakupy.entity.orderFlowModels.OrderFlowItem;
import mroweczka.e_zakupy.presentation.view.submitOrder.ChooseFacilityView;

public class ChooseFacilityPresenter extends BaseSubmitOrderPresenter
        implements BaseSubmitOrderPresenterInterface<ChooseFacilityView> {

    private static final String TAG = ChooseFacilityPresenter.class.getSimpleName();

    private ChooseFacilityView view;

    private GetFacilities getFacilities;
    private CheckProductsAvailability checkProductsAvailability;
    private LockStorageItems lockStorageItems;

    @Inject
    public ChooseFacilityPresenter(GetOrderFlow getOrderFlow,
                                   SaveOrderFlow saveOrderFlow,
                                   UnlockStorageItems unlockStorageItems,
                                   GetFacilities getFacilities,
                                   CheckProductsAvailability checkProductsAvailability,
                                   LockStorageItems lockStorageItems) {
        super(getOrderFlow, saveOrderFlow, unlockStorageItems);
        this.getFacilities = getFacilities;
        this.checkProductsAvailability = checkProductsAvailability;
        this.lockStorageItems = lockStorageItems;
        itemsLockedInMagazine = false;
    }

    public void attachView(ChooseFacilityView view) {
        super.attachView(view);
        this.view = view;
        view.showProgressDialog();
        setupView();
    }

    private void setupView() {
        view.setupFacilityDetails();
    }

    public void onViewReady() {
        showAllFacilities();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.view = null;
    }

    private void showAllFacilities() {
        view.showProgressDialog();
        List<Facility> facilities = getFacilities.execute();
        if (facilities == null) {
            view.showError();
        } else {
            view.showFacilities(facilities);
        }
        view.hideProgressDialog();
    }

    /**
     * The facility is added to the order, and then products are locked in given facility
     * In case the facility was already chosen, the products are unlocked
     * If facility didn't change we simply continue order
     *
     * @param facilityId
     */
    public void chooseFacility(int facilityId) {
        view.showProgressDialog();
        Integer currentFacilityId = orderFlow.getFacilityId();
        if (currentFacilityId != null) {
            if (currentFacilityId != facilityId) {
                unlockStorageItems.execute(currentFacilityId, orderFlow.getOrderFlowItems());
                lockOrderFlowItemsInStorage(facilityId);
            } else {
                view.continueOrder();
                view.hideProgressDialog();
            }
        } else {
            lockOrderFlowItemsInStorage(facilityId);
        }
    }

    private void lockOrderFlowItemsInStorage(Integer facilityId) {
        orderFlow.setFacilityId(null);
        List<OrderFlowItem> orderFlowItems = orderFlow.getOrderFlowItems();
        if (checkProductsAvailability.execute(facilityId, orderFlowItems)) {
            Log.d(TAG, "Items are available in the storage, starting to lock items...");
            if (lockStorageItems.execute(facilityId, orderFlowItems)) {
                Log.d(TAG, "Items locked successfully in the magazine");
                itemsLockedInMagazine = true;
                orderFlow.setFacilityId(facilityId);
                view.continueOrder();
            } else {
                Log.d(TAG, "Something went wrong while locking items in the magazine");
                view.showAvailabilityError();
            }
        } else {
            Log.d(TAG, "Some products are not available in the storage ");
            view.showAvailabilityError();
        }
        saveOrderFlow.execute(orderFlow);
        view.hideProgressDialog();
    }
}

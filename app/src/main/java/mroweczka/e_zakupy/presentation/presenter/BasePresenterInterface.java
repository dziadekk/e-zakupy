package mroweczka.e_zakupy.presentation.presenter;

import mroweczka.e_zakupy.presentation.view.BaseView;

public interface BasePresenterInterface<T extends BaseView> {

    void attachView(T view);

    void onStart();

    void onResume();

    void onPause();

    void onStop();

    void onDestroy();
}

package mroweczka.e_zakupy.presentation.presenter.addProduct;

import mroweczka.e_zakupy.presentation.presenter.BasePresenterInterface;
import mroweczka.e_zakupy.presentation.view.addProduct.BaseAddProductView;
import mroweczka.e_zakupy.presentation.view.submitOrder.BaseSubmitOrderView;

public interface BaseAddProductPresenterInterface <T extends BaseAddProductView>
        extends BasePresenterInterface<BaseAddProductView>{

}

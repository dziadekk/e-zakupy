package mroweczka.e_zakupy.presentation.presenter.submitOrder;


import org.joda.time.LocalDate;

import javax.inject.Inject;

import mroweczka.e_zakupy.app.ui.submitOrder.products.payment.PaymentOptionFactory;
import mroweczka.e_zakupy.domain.storage.UnlockStorageItems;
import mroweczka.e_zakupy.domain.submitOrder.ClearOrderFlow;
import mroweczka.e_zakupy.domain.submitOrder.GetOrderFlow;
import mroweczka.e_zakupy.domain.submitOrder.SaveOrderFlow;
import mroweczka.e_zakupy.domain.submitOrder.SubmitOrder;
import mroweczka.e_zakupy.domain.submitOrder.SubmitPayment;
import mroweczka.e_zakupy.entity.Payment;
import mroweczka.e_zakupy.entity.orderFlowModels.PaymentOption;
import mroweczka.e_zakupy.presentation.view.submitOrder.ChoosePaymentView;

public class ChoosePaymentPresenter extends BaseSubmitOrderPresenter
        implements BaseSubmitOrderPresenterInterface<ChoosePaymentView> {

    private static final String TAG = ChoosePaymentPresenter.class.getSimpleName();

    private ChoosePaymentView view;

    private SubmitOrder submitOrder;
    private SubmitPayment submitPayment;
    private ClearOrderFlow clearOrderFlow;

    private Payment.PaymentStatus paymentResultStatus;
    private PaymentOption chosenPaymentOption;
    private int paymentErrors = 0;

    @Inject
    public ChoosePaymentPresenter(GetOrderFlow getOrderFlow,
                                  SaveOrderFlow saveOrderFlow,
                                  UnlockStorageItems unlockStorageItems,
                                  SubmitOrder submitOrder,
                                  SubmitPayment submitPayment,
                                  ClearOrderFlow clearOrderFlow) {
        super(getOrderFlow, saveOrderFlow, unlockStorageItems);
        this.submitOrder = submitOrder;
        this.submitPayment = submitPayment;
        this.clearOrderFlow = clearOrderFlow;
        itemsLockedInMagazine = true;
    }

    public void attachView(ChoosePaymentView view) {
        super.attachView(view);
        this.view = view;
        setupView();
    }

    private void setupView() {
        view.setupPaymentOptionList();
        view.showPaymentTypes(PaymentOptionFactory.factoryAllPaymentOptions());
    }

    @Override
    public void onResume() {
        super.onResume();
        if (chosenPaymentOption != null && paymentResultStatus != null) {
            parsePaymentResult();
        }
    }

    public void onPaymentResult(Payment.PaymentStatus paymentStatus) {
        this.paymentResultStatus = paymentStatus;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.view = null;
    }

    public void choosePaymentOption(PaymentOption paymentOption) {
        if (paymentOption != null) {
            ;

            orderFlow.setPrice(orderFlow.getPrice() + paymentOption.getPrice());

            if (paymentOption.getPaymentType() == Payment.PaymentType.PersonalCollect) {
                submitOrder();
            } else {
                this.chosenPaymentOption = paymentOption;
                view.redirectToPaymentSystem(orderFlow.getOrderId(), orderFlow.getPrice());
                view.hideProgressDialog();
            }
        } else {
            view.showErrorNoPaymentChosen();
        }

    }

    private void parsePaymentResult() {
        switch (paymentResultStatus) {
            case Accepted:
                onPaymentAccepted();
                break;
            case Denied:
                onPaymentDenied();
                break;
            default:
                new IllegalArgumentException("This type of payment result is not supported");
        }
    }

    private void onPaymentDenied() {
        if (paymentErrors < 3) {
            paymentErrors++;
            orderFlow.setPrice(orderFlow.getPrice() - chosenPaymentOption.getPrice());
            chosenPaymentOption = null;
            paymentResultStatus = null;

            view.showPaymentDeclined();
        } else {
            view.goToOrderManagementWithDeclinedPaymentError();
            cancelOrder();
        }
    }

    private void onPaymentAccepted() {
        view.showProgressDialog();
        orderFlow.setPaymentId(submitPayment());
        submitOrder();
        view.hideProgressDialog();
    }

    private int submitPayment() {
        return submitPayment.execute(
                new Payment(null,
                        chosenPaymentOption.getPaymentType(),
                        orderFlow.getPrice(),
                        LocalDate.now(),
                        LocalDate.now(),
                        paymentErrors,
                        Payment.PaymentStatus.Accepted));
    }

    private void submitOrder() {
        submitOrder.execute(orderFlow);
        clearOrderFlow.execute();
        view.goToOrderSummary(orderFlow.getOrderId());
    }
}

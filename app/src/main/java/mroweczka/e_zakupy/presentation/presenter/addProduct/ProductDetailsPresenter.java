package mroweczka.e_zakupy.presentation.presenter.addProduct;

import android.os.AsyncTask;

import org.joda.time.LocalDate;

import java.util.List;

import javax.inject.Inject;

import mroweczka.e_zakupy.domain.catalog.GetProduct;
import mroweczka.e_zakupy.domain.submitOrder.SaveOrderFlowItem;
import mroweczka.e_zakupy.entity.Product;
import mroweczka.e_zakupy.entity.PromotionalPrice;
import mroweczka.e_zakupy.entity.orderFlowModels.OrderFlowItem;
import mroweczka.e_zakupy.presentation.view.addProduct.ProductDetailsView;


/**
 * Presenter aktywnosći ProductDetailsPresenter
 */
public class ProductDetailsPresenter extends BaseAddProductPresenter
        implements BaseAddProductPresenterInterface<ProductDetailsView> {
    private static final double MAXIMUMLITR = 5;
    private static final double MINIMUMLITR = 1;
    private static final double MAXIMUMPIECES = 16;
    private static final double MINIMUMPIECES = 1;
    private static final double MAXIMUMKILOGRAM = 8;
    private static final double MINIMUMKILOGRAM = 1;
    private static final String LITR = " l";
    private static final String PIECES = " szt";
    private static final String KILOGRAM = " kg";
    private static final String PRODUCT_ID_STRING = "productId";
    private static final String PRODUCT_QUANTITY_STRING = "quantity";
    private static final String PRODUCT_PRICE_STRING = "orderItemPrice";
    private static final String CURRENCY = " zł";

    private ProductDetailsView view;

    private GetProduct getProduct;
    private SaveOrderFlowItem saveOrderFlowItem;

    private Product product;

    @Inject
    public ProductDetailsPresenter(GetProduct getProduct,
                                   SaveOrderFlowItem saveOrderFlowItem) {
        this.getProduct = getProduct;
        this.saveOrderFlowItem = saveOrderFlowItem;
    }

    /**
     * Metoda przylączająca widok
     *
     * @param view widok do przyłączenia
     */
    public void attachView(ProductDetailsView view) {
        super.attachView(view);
        this.view = view;

        downloadProduct();
    }

    private AsyncTask<Integer, Void, Product> downloadProduct() {
        return new AsyncGetProduct().execute(this.view.getIntent().getExtras().getInt(PRODUCT_ID_STRING));
    }

    /**
     * Metoda zwracająca nazwę produktu
     *
     * @return nazwa prodktu
     */
    public String getProductName() {
        return product.getName();
    }

    public String getProductPriceString() {
        return String.format("%1$.2f zł", getProductPrice());
    }

    /**
     * Metoda zwracająca cenę produktu
     * * <b>Reguła biznesowa:</b>
     * Aktualna cena produktu to cena promocyjna w przypadku gdy obecna data znajduje w przedziale obowiązywania promocji.
     *
     * @return aktualna cena produktu
     */
    public double getProductPrice() {
        List<PromotionalPrice> promotionalPrices = product.getPromotionalPrice();
        LocalDate today = new LocalDate();
        for (int i = 0; i < promotionalPrices.size(); i++) {
            if (promotionalPrices.get(i).getValidFrom().compareTo(today) < 0 && promotionalPrices.get(i).getValidUntil().compareTo(today) > 0) {
                if (promotionalPrices.get(i).getPromotionalPercent() == 0) {
                    return promotionalPrices.get(i).getPriceUnit();
                } else {
                    return ((100 - promotionalPrices.get(i).getPromotionalPercent())) * product.getUnitPrice() / 100;
                }
            }
        }

        return product.getUnitPrice();
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    /**
     * Metoda zwracająca domyślną ilość razem z typem produktu
     *
     * @return literał zawierający domyślną ilość i typ produktu
     */
    public String getProductQuanityString() {
        switch (product.getSellUnit()) {
            case Kilogram:
                return "1 " + getType();
            case Pieces:
                return "1 " + getType();
            default:
                return "1 " + getType();
        }
    }

    /**
     * Metoda zwaracjąca literał typu liczności produktu
     *
     * @return typ roduktu
     */
    public String getType() {
        switch (product.getSellUnit()) {
            case Kilogram:
                return KILOGRAM;
            case Pieces:
                return PIECES;
            default:
                return LITR;
        }
    }

    /**
     * Metoda sprawdzajaca czy można zwiększyć/zmienjszyć ilość zamówionego produktu
     * <p>
     * <b>Reguła biznesowa:</b>
     * Nie można kupić więcej niż 16 sztuk, 5 kilogramów lub 8 litrów danego produktu.
     *
     * @return czy produkt jest dostępny
     */
    public boolean checkAvability(double newQuantity) {
        switch (product.getSellUnit()) {
            case Kilogram:
                return checkConstraints(newQuantity, MAXIMUMKILOGRAM, MINIMUMKILOGRAM);
            case Pieces:
                return checkConstraints(newQuantity, MAXIMUMPIECES, MINIMUMPIECES);
            default:
                return checkConstraints(newQuantity, MAXIMUMLITR, MINIMUMLITR);
        }
    }

    private boolean checkConstraints(double newQuantity, double maximum, double minimum) {
        return checkMaximum(maximum, newQuantity) && checkMinimum(minimum, newQuantity);
    }

    private boolean checkMinimum(double minimum, double newQuantity) {
        return minimum <= newQuantity;
    }

    private boolean checkMaximum(double maximumQuantityOfType, double newQuantity) {
        return maximumQuantityOfType >= newQuantity;
    }

    /**
     * Metoda zwracająca opis produktu
     *
     * @return opis produktu
     */
    public String getProductDescribe() {
        return product.getDescribe();
    }

    /**
     * Metoda zwracająca nazwę producenta produktu
     *
     * @return nazwa producenta produktu
     */
    public String getProductManufacturer() {
        return product.getManufacturer().getManufacturerName();
    }

    /**
     * Metoda zwracająca państwo, z którego pochodzi produkt
     *
     * @return nazwa państwa
     */
    public String getProductCountry() {
        return product.getCountry().getCountryName();
    }

    /**
     * Metoda zwracająca zdjęcie produktu
     *
     * @return identyfikator zdjęcia
     */
    public int getProductImage() {
        return product.getImage();
    }


    public String updatePrice(int quantity) {
        return String.format("%1$.2f zł", getProductPrice() * quantity);
    }

    /**
     * Metoda ustawiająca zawartość widoku
     */
    public void setup() {
        view.setProductNameTextView(getProductName());
        view.setProductPriceTextView(getProductPriceString());
        view.setProductQuantityTextView(getProductQuanityString());
        view.setProductManufacturerTextView(getProductManufacturer());
        view.setProductDescribeTextView(getProductDescribe());
        view.setCountryTextView(getProductCountry());
        view.setProductDetailsImageImageView(getProductImage());
        view.showProductInfo();
    }

    /**
     * Metoda obsługująca zmianę liczności produktu
     *
     * @param newQuantity nowa liczność produktu
     */
    public void proccesDiffrence(double newQuantity) {
        if (checkAvability(newQuantity)) {
            view.setProductPriceTextView(updatePrice((int) newQuantity));
            view.setProductQuantityTextView(String.valueOf((int) newQuantity + getType()));
        } else {
            view.createErrorQuantityDialog();
        }
    }

    public void addProduct(Double quantity) {
        OrderFlowItem orderFlowItem = new OrderFlowItem(product.getId(),
                product.getCategory().getCategoryId(),
                quantity, getProductPrice());
        saveOrderFlowItem.execute(orderFlowItem);
        view.goToOrderManagement();
    }

    private class AsyncGetProduct extends AsyncTask<Integer, Void, Product> {
        @Override
        protected void onPreExecute() {
            view.showProgressDialog();
        }

        @Override
        protected Product doInBackground(Integer... productId) {
            return getProduct.execute(productId[0]);
        }

        @Override
        protected void onPostExecute(Product product) {
            ProductDetailsPresenter.this.product = product;
            setup();
            view.hideProgressDialog();
        }
    }
}

package mroweczka.e_zakupy.presentation.presenter;

import android.os.AsyncTask;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import mroweczka.e_zakupy.domain.catalog.GetProduct;
import mroweczka.e_zakupy.domain.submitOrder.CreateEmptyOrder;
import mroweczka.e_zakupy.domain.submitOrder.GetNewOrderId;
import mroweczka.e_zakupy.domain.submitOrder.GetOrderFlow;
import mroweczka.e_zakupy.domain.submitOrder.SaveOrderFlow;
import mroweczka.e_zakupy.entity.Product;
import mroweczka.e_zakupy.entity.orderFlowModels.OrderFlow;
import mroweczka.e_zakupy.entity.orderFlowModels.OrderFlowItem;
import mroweczka.e_zakupy.presentation.view.OrderManagmentView;

public class OrderManagmentPresenter implements BasePresenterInterface<OrderManagmentView> {
    private static final String TITLE_DIALOG_STRING = "Puste zamówienie";
    private static final String MESSAGE_DIALOG_STRING = "Nie można wysłać pustego zamówienia";

    private OrderManagmentView view;

    protected GetOrderFlow getOrderFlow;
    protected GetProduct getProduct;
    protected SaveOrderFlow saveOrderFlow;
    protected GetNewOrderId getNewOrderId;
    protected CreateEmptyOrder createEmptyOrder;

    private OrderFlow orderFlow;

    @Inject
    public OrderManagmentPresenter(GetOrderFlow getOrder, GetProduct getProduct, SaveOrderFlow saveOrderFlow, GetNewOrderId getNewOrderId, CreateEmptyOrder createEmptyOrder) {
        this.getOrderFlow = getOrder;
        this.getProduct = getProduct;
        this.saveOrderFlow = saveOrderFlow;
        this.getNewOrderId = getNewOrderId;
        this.createEmptyOrder = createEmptyOrder;
    }

    @Override
    public void attachView(OrderManagmentView view) {
        this.view = view;
        setup();
    }

    private void setup() {
        if (orderFlow == null) {
            orderFlow = getOrderFlow.execute();
        }
        new AsyncGetProducts().execute(orderFlow.getOrderFlowItems(), null);
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }

    public void continueOrder() {
        if (orderFlow.getOrderFlowItems().size() > 0) {
            int maksId = getNewOrderId.execute();
            orderFlow.setOrderId(maksId);
            createEmptyOrder.execute(maksId);
            saveOrderFlow.execute(orderFlow);
            view.continueOrder();
        } else {
            view.createErrorDialog(TITLE_DIALOG_STRING, MESSAGE_DIALOG_STRING);
        }
    }

    private void setOrderPrice() {
        double totalPrice = 0;
        for (OrderFlowItem orderFlowItem : orderFlow.getOrderFlowItems()) {
            totalPrice += orderFlowItem.getPrice() * orderFlowItem.getQuantity();
        }
        orderFlow.setPrice(totalPrice);
        view.setPrice(totalPrice);
    }

    private class AsyncGetProducts extends AsyncTask<List<OrderFlowItem>, Void, Map<Integer, Product>> {

        @Override
        protected void onPreExecute() {
            view.showProgressDialog();
        }

        @Override
        protected Map<Integer, Product> doInBackground(List<OrderFlowItem>... orderFlowItems) {
            Map<Integer, Product> productMap = new HashMap<>(orderFlowItems[0].size());
            for (OrderFlowItem orderFlowItem : orderFlowItems[0]) {
                productMap.put(orderFlowItem.getProductId(), getProduct.execute(orderFlowItem.getProductId()));
            }
            return productMap;
        }
        @Override
        protected void onPostExecute(Map<Integer, Product> productsMap) {
            setOrderPrice();

            view.setupAdapter(orderFlow.getOrderFlowItems(), productsMap);
            view.hideProgressDialog();
        }

    }

}

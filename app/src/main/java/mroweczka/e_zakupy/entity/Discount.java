package mroweczka.e_zakupy.entity;

public class Discount {
    private Integer discountId;
    private int percentOfDiscount;
    private String discountCode;
    private Order order;
    private Category discountedCategory;

    public Discount(Integer discountId) {
        this.discountId = discountId;
    }

    public Discount(Integer discountId, int percentOfDiscount, String discountCode, Order order, Category discountedCategory) {
        this.discountId = discountId;
        this.percentOfDiscount = percentOfDiscount;
        this.discountCode = discountCode;
        this.order = order;
        this.discountedCategory = discountedCategory;
    }

    public Integer getDiscountId() {
        return discountId;
    }

    public void setDiscountId(Integer discountId) {
        this.discountId = discountId;
    }

    public int getPercentOfDiscount() {
        return percentOfDiscount;
    }

    public void setPercentOfDiscount(int percentOfDiscount) {
        this.percentOfDiscount = percentOfDiscount;
    }

    public String getDiscountCode() {
        return discountCode;
    }

    public void setDiscountCode(String discountCode) {
        this.discountCode = discountCode;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Category getDiscountedCategory() {
        return discountedCategory;
    }

    public void setDiscountedCategory(Category discountedCategory) {
        this.discountedCategory = discountedCategory;
    }
}

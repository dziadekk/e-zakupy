package mroweczka.e_zakupy.entity;

import org.joda.time.LocalDate;

import java.util.HashMap;
import java.util.Map;


public class Payment {
    public enum PaymentType {
        BankTransfer(0),
        CreditCard(1),
        PersonalCollect(2);
        private static Map<Integer, PaymentType> map = new HashMap<Integer,PaymentType>();

        static {
            for (PaymentType paymentEnum : PaymentType.values()) {
                map.put(paymentEnum.value, paymentEnum);
            }
        }

        public static PaymentType enumOf(int value) {
            return map.get(value);
        }
        public int getValue() {
            return value;
        }

        private int value;

        PaymentType (int value)
        {
            this.value = value;
        }
    }

    public enum PaymentStatus {
        Accepted(0),
        Denied(1);
        private static Map<Integer, PaymentStatus> map = new HashMap<Integer,PaymentStatus>();

        static {
            for (PaymentStatus statusEnum : PaymentStatus.values()) {
                map.put(statusEnum.value, statusEnum);
            }
        }

        public static PaymentStatus enumOf(int value) {
            return map.get(value);
        }
        public int getValue() {
            return value;
        }

        private int value;

        PaymentStatus (int value)
        {
            this.value = value;
        }
    }

    private Integer id;
    private PaymentType paymentType;
    private double amount;
    private LocalDate postingDate;
    private LocalDate paymentDate;
    private int numberOfTries;
    private PaymentStatus paymentStatus;

    public Payment(Integer id) {
        this.id = id;
    }

    public Payment(Integer id, PaymentType paymentType, double amount, LocalDate postingDate, LocalDate paymentDate, int numberOfTries, PaymentStatus paymentStatus) {
        this.id = id;
        this.paymentType = paymentType;
        this.amount = amount;
        this.postingDate = postingDate;
        this.paymentDate = paymentDate;
        this.numberOfTries = numberOfTries;
        this.paymentStatus = paymentStatus;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public LocalDate getPostingDate() {
        return postingDate;
    }

    public void setPostingDate(LocalDate postingDate) {
        this.postingDate = postingDate;
    }

    public LocalDate getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(LocalDate paymentDate) {
        this.paymentDate = paymentDate;
    }

    public int getNumberOfTries() {
        return numberOfTries;
    }

    public void setNumberOfTries(int numberOfTries) {
        this.numberOfTries = numberOfTries;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }
}

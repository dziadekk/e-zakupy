package mroweczka.e_zakupy.entity;

import org.joda.time.LocalDate;

public class Worker {
    private int workerId;
    private String name;
    private String surname;
    private LocalDate registrationDate;
    private LocalDate birthday;
    private String pesel;
    private String accessCode;
    private Facility facility;

    public Worker(int workerId, String name, String surname, LocalDate registrationDate, LocalDate birthday, String pesel, String accessCode, Facility facility) {
        this.workerId = workerId;
        this.name = name;
        this.surname = surname;
        this.registrationDate = registrationDate;
        this.birthday = birthday;
        this.pesel = pesel;
        this.accessCode = accessCode;
        this.facility = facility;
    }

    public int getWorkerId() {
        return workerId;
    }

    public void setWorkerId(int workerId) {
        this.workerId = workerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getAccessCode() {
        return accessCode;
    }

    public void setAccessCode(String accessCode) {
        this.accessCode = accessCode;
    }

    public Facility getFacility() {
        return facility;
    }

    public void setFacility(Facility facility) {
        this.facility = facility;
    }
}

package mroweczka.e_zakupy.entity.orderFlowModels;


import mroweczka.e_zakupy.entity.Payment;

public class OrderFlowPayment {//// TODO: 22/01/2017 probably not needed
    private Integer paymentId;
    private Payment.PaymentType paymentType;

    public OrderFlowPayment(int paymentId, Payment.PaymentType paymentType) {
        this.paymentId = paymentId;
        this.paymentType = paymentType;
    }

    public int getPaymentId() {
        return paymentId;
    }

    public Payment.PaymentType getPaymentType() {
        return paymentType;
    }
}

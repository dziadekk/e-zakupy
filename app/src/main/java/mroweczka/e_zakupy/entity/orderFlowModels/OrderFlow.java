package mroweczka.e_zakupy.entity.orderFlowModels;

import java.util.List;


public class OrderFlow {
    private int orderId;
    private List<OrderFlowItem> orderFlowItems;
    private Integer facilityId;
    private OrderFlowOrderType orderFlowOrderType;
    private Integer discountId;
    private Integer paymentId;
    private double price;

    public OrderFlow() {
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public List<OrderFlowItem> getOrderFlowItems() {
        return orderFlowItems;
    }

    public void setOrderFlowItems(List<OrderFlowItem> orderFlowItems) {
        this.orderFlowItems = orderFlowItems;
    }

    public Integer getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(Integer facilityId) {
        this.facilityId = facilityId;
    }

    public OrderFlowOrderType getOrderFlowOrderType() {
        return orderFlowOrderType;
    }

    public void setOrderFlowOrderType(OrderFlowOrderType orderFlowOrderType) {
        this.orderFlowOrderType = orderFlowOrderType;
    }

    public Integer getDiscountId() {
        return discountId;
    }

    public void setDiscountId(Integer discountId) {
        this.discountId = discountId;
    }

    public Integer getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Integer paymentId) {
        this.paymentId = paymentId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void addFlowItem(OrderFlowItem orderFlowItem) {
        orderFlowItems.add(orderFlowItem);
    }
}

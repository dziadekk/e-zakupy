package mroweczka.e_zakupy.entity.orderFlowModels;

public class OrderFlowItem {
    private final int productId;
    private final Integer productCategoryId;
    private final double quantity;
    private final double price;

    public OrderFlowItem(int productId, Integer productCategoryId, double quantity, double price) {
        this.productId = productId;
        this.productCategoryId = productCategoryId;
        this.quantity = quantity;
        this.price = price;
    }

    public int getProductId() {
        return productId;
    }

    public Integer getProductCategoryId() {
        return productCategoryId;
    }

    public double getQuantity() {
        return quantity;
    }

    public double getPrice() {
        return price;
    }
}
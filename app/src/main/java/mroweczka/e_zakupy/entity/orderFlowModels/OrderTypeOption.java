package mroweczka.e_zakupy.entity.orderFlowModels;

import mroweczka.e_zakupy.entity.Order;

public class OrderTypeOption {
    private final Order.OrderType orderType;
    private final int titleRes;
    private final int descriptionRes;
    private final double price;

    public OrderTypeOption(Order.OrderType orderType, int titleRes, int descriptionRes, double price) {
        this.orderType = orderType;
        this.titleRes = titleRes;
        this.descriptionRes = descriptionRes;
        this.price = price;
    }

    public Order.OrderType getOrderType() {
        return orderType;
    }

    public int getTitleRes() {
        return titleRes;
    }

    public int getDescriptionRes() {
        return descriptionRes;
    }

    public double getPrice() {
        return price;
    }
}
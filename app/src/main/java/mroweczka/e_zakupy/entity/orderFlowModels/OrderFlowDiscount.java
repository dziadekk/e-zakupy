package mroweczka.e_zakupy.entity.orderFlowModels;

public class OrderFlowDiscount {//// TODO: 22/01/2017 probably not needed
    private final int discountId;
    private final int percentOfDiscount;
    private final Integer discountedCategory;

    public OrderFlowDiscount(int discountId, int percentOfDiscount, Integer discountedCategory) {
        this.discountId = discountId;
        this.percentOfDiscount = percentOfDiscount;
        this.discountedCategory = discountedCategory;
    }

    public int getDiscountId() {
        return discountId;
    }

    public int getPercentOfDiscount() {
        return percentOfDiscount;
    }

    public Integer getDiscountedCategory() {
        return discountedCategory;
    }
}

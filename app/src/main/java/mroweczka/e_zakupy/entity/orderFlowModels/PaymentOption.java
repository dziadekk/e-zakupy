package mroweczka.e_zakupy.entity.orderFlowModels;

import mroweczka.e_zakupy.entity.Payment;

public class PaymentOption {
    private final Payment.PaymentType paymentType;
    private final int titleRes;
    private final int descriptionRes;
    private final double price;

    public PaymentOption(Payment.PaymentType paymentType, int titleRes, int descriptionRes, double price) {
        this.paymentType = paymentType;
        this.titleRes = titleRes;
        this.descriptionRes = descriptionRes;
        this.price = price;
    }

    public Payment.PaymentType getPaymentType() {
        return paymentType;
    }

    public int getTitleRes() {
        return titleRes;
    }

    public int getDescriptionRes() {
        return descriptionRes;
    }

    public double getPrice() {
        return price;
    }


}
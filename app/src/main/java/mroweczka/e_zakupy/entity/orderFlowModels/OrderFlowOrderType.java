package mroweczka.e_zakupy.entity.orderFlowModels;

import org.joda.time.LocalDateTime;

import mroweczka.e_zakupy.entity.Order;

public class OrderFlowOrderType {
    private final LocalDateTime requestedCollectDate;

    public OrderFlowOrderType(LocalDateTime requestedCollectDate) {
        this.requestedCollectDate = requestedCollectDate;
    }

    public LocalDateTime getRequestedCollectDate() {
        return requestedCollectDate;
    }

    /**
     * Metoda zwraca typ zamówienia w oparciu o podaną datę odbioru
     * <p>
     * <b>Reguła biznesowa:</b>
     * Zamówienie musi być uznane jako ekspresowe, jeśli klient nie poda daty odbioru.
     *
     * @return typ zamówienia
     */
    public Order.OrderType getOrderType() {
        return requestedCollectDate == null ? Order.OrderType.Express : Order.OrderType.Hourly;
    }
}

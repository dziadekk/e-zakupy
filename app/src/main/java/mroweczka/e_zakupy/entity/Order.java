package mroweczka.e_zakupy.entity;


import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Order {
    public enum StateOfOrder {
        Canceled(1),
        InCreate(2),
        WaitingForRealisation(3),
        WaitingForPayment(4),
        Packing(5),
        ReadyForCollect(6),
        Collected(7),
        NotCollected(8);

        private static Map<Integer, StateOfOrder> map = new HashMap<Integer, StateOfOrder>();

        static {
            for (StateOfOrder stateEnum : StateOfOrder.values()) {
                map.put(stateEnum.value, stateEnum);
            }
        }

        public static StateOfOrder enumOf(int value) {
            return map.get(value);
        }

        public int getValue() {
            return value;
        }

        private int value;

        StateOfOrder(int value) {
            this.value = value;
        }
    }

    public enum OrderType {
        Express(1),
        Hourly(2);

        private static Map<Integer, OrderType> map = new HashMap<Integer, OrderType>();

        static {
            for (OrderType orderEnum : OrderType.values()) {
                map.put(orderEnum.value, orderEnum);
            }
        }

        public static OrderType enumOf(int value) {
            return map.get(value);
        }

        public int getValue() {
            return value;
        }

        private int value;

        OrderType(int value) {
            this.value = value;
        }
    }

    private int orderId;
    private double price;
    private LocalDate dateOfOrder;
    private LocalDateTime requestedCollectDate;
    private LocalDate collectDate;
    private int collectCode;
    private OrderType orderType;//// TODO: 22/01/2017 mozna wyrzucic i oceniac to na podstawie requesteColledDate czy jest nullem, ale to jak uważasz
    private StateOfOrder stateOfOrder;
    private List<OrderItem> orderItems;
    private Discount discount;
    private Payment payment;
    private Facility facility;

    public Order(int orderId, double price, LocalDate dateOfOrder, LocalDateTime requestedCollectDate, LocalDate collectDate, int collectCode, OrderType orderType, StateOfOrder stateOfOrder, List<OrderItem> orderItems, Discount discount, Payment payment, Facility facility) {
        this.orderId = orderId;
        this.price = price;
        this.dateOfOrder = dateOfOrder;
        this.requestedCollectDate = requestedCollectDate;
        this.collectDate = collectDate;
        this.collectCode = collectCode;
        this.orderType = orderType;
        this.stateOfOrder = stateOfOrder;
        this.orderItems = orderItems;
        this.discount = discount;
        this.payment = payment;
        this.facility = facility;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public LocalDate getDateOfOrder() {
        return dateOfOrder;
    }

    public void setDateOfOrder(LocalDate dateOfOrder) {
        this.dateOfOrder = dateOfOrder;
    }

    public LocalDateTime getRequestedCollectDate() {
        return requestedCollectDate;
    }

    public void setRequestedCollectDate(LocalDateTime requestedCollectDate) {
        this.requestedCollectDate = requestedCollectDate;
    }

    public LocalDate getCollectDate() {
        return collectDate;
    }

    public void setCollectDate(LocalDate collectDate) {
        this.collectDate = collectDate;
    }

    public int getCollectCode() {
        return collectCode;
    }

    public void setCollectCode(int collectCode) {
        this.collectCode = collectCode;
    }

    public OrderType getOrderType() {
        return orderType;
    }

    public void setOrderType(OrderType orderType) {
        this.orderType = orderType;
    }

    public StateOfOrder getStateOfOrder() {
        return stateOfOrder;
    }

    public void setStateOfOrder(StateOfOrder stateOfOrder) {
        this.stateOfOrder = stateOfOrder;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public Discount getDiscount() {
        return discount;
    }

    public void setDiscount(Discount discount) {
        this.discount = discount;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public Facility getFacility() {
        return facility;
    }

    public void setFacility(Facility facility) {
        this.facility = facility;
    }
}

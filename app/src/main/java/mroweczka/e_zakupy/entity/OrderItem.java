package mroweczka.e_zakupy.entity;

import android.annotation.SuppressLint;

public class OrderItem {
    public static final String DISPLAY_PRICE = "%.2f zł";
    public static final String DISPLAY_QUANTITY_PIECES = "x %d";
    public static final String DISPLAY_QUANTITY_KG = "%.2f kg";
    public static final String DISPLAY_QUANITY_LITER = "%.2f l";

    private Integer orderItemId;
    private Product product;
    private double quantity;
    private double price;

    public OrderItem(Integer orderItemId, Product product, double quantity, double price) {
        this.orderItemId = orderItemId;
        this.product = product;
        this.quantity = quantity;
        this.price = price;
    }

    public int getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(int orderItemId) {
        this.orderItemId = orderItemId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @SuppressLint("DefaultLocale")
    public String getQuantityForDisplay() {
        switch (product.getSellUnit()) {
            case Pieces:
                return String.format(DISPLAY_QUANTITY_PIECES, (int) quantity);
            case Kilogram:
                return String.format(DISPLAY_QUANTITY_KG, quantity);
            case Liter:
                return String.format(DISPLAY_QUANITY_LITER, quantity);
            default:
                new IllegalArgumentException("Niepoprawny typ liczności");
        }
        return null;
    }

    public String getPriceForDisplay() {
        return String.format(DISPLAY_PRICE, price);
    }
}

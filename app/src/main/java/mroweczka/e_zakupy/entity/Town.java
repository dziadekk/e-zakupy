package mroweczka.e_zakupy.entity;

public class Town {
    private int townId;
    private String name;

    public Town(int townId, String name) {
        this.townId = townId;
        this.name = name;
    }

    public int getTownId() {
        return townId;
    }

    public void setTownId(int townId) {
        this.townId = townId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

package mroweczka.e_zakupy.entity;

public class Manufacturer {

    private int manufacturerId;
    private String manufacturerName;
    private Country country;

    public Manufacturer(int manufacturerId, String manufacturerName) {
        this.manufacturerName = manufacturerName;
        this.manufacturerId = manufacturerId;
    }

    public Manufacturer(int manufacturerId, String manufacturerName, Country country) {
        this.manufacturerId = manufacturerId;
        this.manufacturerName = manufacturerName;
        this.country = country;
    }

    public int getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(int manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
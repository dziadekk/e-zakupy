package mroweczka.e_zakupy.entity;

import org.joda.time.LocalDate;

public class PromotionalPrice {
    private int promotionalPriceId;
    private LocalDate validFrom;
    private LocalDate validUntil;
    private Integer promotionalPercent;
    private Double priceUnit;


    public PromotionalPrice(int promotionalPriceId, LocalDate validFrom, LocalDate validUntil, Integer promotionalPercent, Double priceUnit) {
        this.promotionalPriceId = promotionalPriceId;
        this.validFrom = validFrom;
        this.validUntil = validUntil;
        this.promotionalPercent = promotionalPercent;
        this.priceUnit = priceUnit;
    }

    public int getPromotionalPriceId() {
        return promotionalPriceId;
    }

    public void setPromotionalPriceId(int promotionalPriceId) {
        this.promotionalPriceId = promotionalPriceId;
    }

    public LocalDate getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(LocalDate validFrom) {
        this.validFrom = validFrom;
    }

    public LocalDate getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(LocalDate validUntil) {
        this.validUntil = validUntil;
    }

    public Integer getPromotionalPercent() {
        return promotionalPercent;
    }

    public void setPromotionalPercent(Integer promotionalPercent) {
        this.promotionalPercent = promotionalPercent;
    }

    public Double getPriceUnit() {
        return priceUnit;
    }

    public void setPriceUnit(Double priceUnit) {
        this.priceUnit = priceUnit;
    }
}

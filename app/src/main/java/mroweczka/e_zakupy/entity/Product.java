package mroweczka.e_zakupy.entity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Product {
    public enum Unit {
        Pieces(1),
        Kilogram(2),
        Liter(3);

        private static Map<Integer, Unit> map = new HashMap<Integer, Unit>();

        static {
            for (Unit unitEnum : Unit.values()) {
                map.put(unitEnum.value, unitEnum);
            }
        }
        public static Unit enumOf(int value) {
            return map.get(value);
        }

        public int getValue() {
            return value;
        }

        private int value;

        Unit (int value)
        {
            this.value = value;
        }
    }

    private Integer id;
    private String barcode;
    private String shortName;
    private String name;
    private String describe;
    private double unitPrice;
    private int image;
    private Unit sellUnit;
    private Manufacturer manufacturer;
    private Country country;
    private Category category;
    private Vat vat;
    private List<PromotionalPrice> promotionalPrice;

    public Product() {
    }

    public Product(Integer id) {
        this.id = id;
    }

    public Product(Integer id, String barcode, String shortName, String name, String describe, double unitPrice, int image, Unit sellUnit, Manufacturer manufacturer, Country country, Category category, Vat vat, List<PromotionalPrice> promotionalPrice) {
        this.id = id;
        this.barcode = barcode;
        this.shortName = shortName;
        this.name = name;
        this.describe = describe;
        this.unitPrice = unitPrice;
        this.image = image;
        this.sellUnit = sellUnit;
        this.manufacturer = manufacturer;
        this.country = country;
        this.category = category;
        this.vat = vat;
        this.promotionalPrice = promotionalPrice;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public Unit getSellUnit() {
        return sellUnit;
    }

    public void setSellUnit(Unit sellUnit) {
        this.sellUnit = sellUnit;
    }

    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(Manufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Vat getVat() {
        return vat;
    }

    public void setVat(Vat vat) {
        this.vat = vat;
    }

    public List<PromotionalPrice> getPromotionalPrice() {
        return promotionalPrice;
    }

    public void setPromotionalPrice(List<PromotionalPrice> promotionalPrice) {
        this.promotionalPrice = promotionalPrice;
    }
}


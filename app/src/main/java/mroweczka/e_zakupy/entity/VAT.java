package mroweczka.e_zakupy.entity;

import java.util.HashMap;
import java.util.Map;

public class Vat {
    public enum VatCategory {

        A(1), B(2), C(3), D(4);

        private static Map<Integer, VatCategory> map = new HashMap<Integer, VatCategory>();

        static {
            for (VatCategory vatEnum : VatCategory.values()) {
                map.put(vatEnum.value, vatEnum);
            }
        }

        public static VatCategory enumOf(int value) {
            return map.get(value);
        }

        public int getValue() {
            return value;
        }

        private int value;

        VatCategory(int value) {
            this.value = value;
        }
    }

    private int vatId;
    private VatCategory category;
    private int percent;

    public Vat(int vatId, VatCategory category, int percent) {
        this.vatId = vatId;
        this.category = category;
        this.percent = percent;
    }

    public int getVatId() {
        return vatId;
    }

    public void setVatId(int vatId) {
        this.vatId = vatId;
    }

    public VatCategory getCategory() {
        return category;
    }

    public void setCategory(VatCategory category) {
        this.category = category;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }
}
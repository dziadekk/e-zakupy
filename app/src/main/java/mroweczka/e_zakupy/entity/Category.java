package mroweczka.e_zakupy.entity;


import java.util.List;

public class Category {
    private int categoryId;
    private String name;
    private String describe;
    private List<Category> subCategories;
    private List<Product> products;

    public Category(int categoryId, String name, String describe, List<Category> subCategories, List<Product> products) {
        this.categoryId = categoryId;
        this.name = name;
        this.describe = describe;
        this.subCategories = subCategories;
        this.products = products;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public List<Category> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(List<Category> subCategories) {
        this.subCategories = subCategories;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}

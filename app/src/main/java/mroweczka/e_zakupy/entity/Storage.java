package mroweczka.e_zakupy.entity;

public class Storage {
    private int id;

    public Storage(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

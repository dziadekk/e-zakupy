package mroweczka.e_zakupy.entity;

import org.joda.time.LocalTime;

import java.util.HashMap;
import java.util.Map;

public class WorkDay {
    public enum WeekDay {
        Monday(1), Tuesday(2), Wednesday(3), Thursday(4), Friday(5), Saturday(6), Sunday(7);

        private static Map<Integer, WeekDay> map = new HashMap<Integer, WeekDay>();

        static {
            for (WeekDay weekEnum : WeekDay.values()) {
                map.put(weekEnum.value, weekEnum);
            }
        }

        public static WeekDay enumOf(int value) {
            return map.get(value);
        }

        public int getValue() {
            return value;
        }

        private int value;

        WeekDay(int value) {
            this.value = value;
        }
    }

    private int workDayId;
    private LocalTime openTime;
    private LocalTime closeTime;
    private WeekDay weekDay;

    public WorkDay(int workDayId, LocalTime openTime, LocalTime closeTime, WeekDay weekDay) {
        this.workDayId = workDayId;
        this.openTime = openTime;
        this.closeTime = closeTime;
        this.weekDay = weekDay;
    }

    public int getWorkDayId() {
        return workDayId;
    }

    public void setWorkDayId(int workDayId) {
        this.workDayId = workDayId;
    }

    public LocalTime getOpenTime() {
        return openTime;
    }

    public void setOpenTime(LocalTime openTime) {
        this.openTime = openTime;
    }

    public LocalTime getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(LocalTime closeTime) {
        this.closeTime = closeTime;
    }

    public WeekDay getWeekDay() {
        return weekDay;
    }

    public void setWeekDay(WeekDay weekDay) {
        this.weekDay = weekDay;
    }
}
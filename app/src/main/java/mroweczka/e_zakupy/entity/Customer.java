package mroweczka.e_zakupy.entity;

import org.joda.time.LocalDate;

import java.util.List;


public class Customer{
    public enum AccountState {
        New,
        Active,
        Banned
    }
    private String name;
    private String surname;
    private LocalDate registrationDate;
    private LocalDate birthday;
    private String email;
    private String phoneNumber;
    private String password;
    private AccountState accountState;
    private List<Order> orderList;
    private List<Discount> discounts;

    public Customer(String name, String surname, LocalDate registrationDate, LocalDate birthday, String email, String phoneNumber, String password, AccountState accountState, List<Order> orderList, List<Discount> discounts) {
        this.name = name;
        this.surname = surname;
        this.registrationDate = registrationDate;
        this.birthday = birthday;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.accountState = accountState;
        this.orderList = orderList;
        this.discounts = discounts;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public AccountState getAccountState() {
        return accountState;
    }

    public void setAccountState(AccountState accountState) {
        this.accountState = accountState;
    }

    public List<Order> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<Order> orderList) {
        this.orderList = orderList;
    }

    public List<Discount> getDiscounts() {
        return discounts;
    }

    public void setDiscounts(List<Discount> discounts) {
        this.discounts = discounts;
    }
}

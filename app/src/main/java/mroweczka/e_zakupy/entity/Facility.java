package mroweczka.e_zakupy.entity;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

public class Facility {
    private Integer id;
    private String street;
    private String postCode;
    private String email;
    private LatLng latLng;
    private String phoneNumber;
    private Town town;
    private List<Order> orders;
    private List<WorkDay> workDays;

    public Facility(Integer id) {
        this.id = id;
    }

    public Facility(Integer id, String street, String postCode, String email, LatLng latLng, String phoneNumber, Town town, List<Order> orders, List<WorkDay> workDays) {
        this.id = id;
        this.street = street;
        this.postCode = postCode;
        this.email = email;
        this.latLng = latLng;
        this.phoneNumber = phoneNumber;
        this.town = town;
        this.orders = orders;
        this.workDays = workDays;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Town getTown() {
        return town;
    }

    public void setTown(Town town) {
        this.town = town;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public List<WorkDay> getWorkDays() {
        return workDays;
    }

    public void setWorkDays(List<WorkDay> workDays) {
        this.workDays = workDays;
    }
}

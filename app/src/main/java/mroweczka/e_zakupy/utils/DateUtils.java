package mroweczka.e_zakupy.utils;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DateUtils {
    public static final String COMMON_DATE_PATTERN = "yyyy-MM-dd";
    public static final String COMMON_DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm";
    public static final String COMMON_TIME_PATTERN = "HH:mm";

    public static final String DISPLAY_DATE_TIME_PATTERN = "dd MMM yyyy | HH:mm";

    public static final String DATE_TODAY = "2017-01-21";
    public static final String DATE_TIME_TODAY = "2017-01-21 12:13";

    public static final DateTimeFormatter COMMON_DATE_FORMATTER = DateTimeFormat.forPattern(COMMON_DATE_PATTERN);
    public static final DateTimeFormatter COMMON_DATE_TIME_FORMATTER = DateTimeFormat.forPattern(COMMON_DATE_TIME_PATTERN);
    public static final DateTimeFormatter COMMON_TIME_FORMATTER = DateTimeFormat.forPattern(COMMON_TIME_PATTERN);

    public static final DateTimeFormatter DISPLAY_DATE_TIME_FORMATTER = DateTimeFormat.forPattern(DISPLAY_DATE_TIME_PATTERN);

    private DateUtils() {
    }

    public static LocalDate getDateInCommonFormat(String date) {
        return COMMON_DATE_FORMATTER.parseLocalDate(date);
    }

    public static LocalTime getTimeInCommonFormat(String time) {
        return COMMON_TIME_FORMATTER.parseLocalTime(time);
    }

    public static LocalDateTime getDateTimeInCommonFormat(String dateTime) {
        return COMMON_DATE_TIME_FORMATTER.parseLocalDateTime(dateTime);
    }

    public static LocalDate today() {
        return LocalDate.now();
    }

    public static LocalDateTime mergeDateAndTime(LocalDate date, LocalTime time) {
        return new LocalDateTime(date.getYear(),
                date.getMonthOfYear(),
                date.getDayOfMonth(),
                time.getHourOfDay(),
                time.getMinuteOfHour());
    }
}
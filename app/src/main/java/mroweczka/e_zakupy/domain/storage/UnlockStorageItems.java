package mroweczka.e_zakupy.domain.storage;

import java.util.List;

import javax.inject.Inject;

import mroweczka.e_zakupy.dataAccess.repositories.storage.StorageRepository;
import mroweczka.e_zakupy.entity.orderFlowModels.OrderFlowItem;

/**
 * Klasa odblokowująca elementy w magazynie
 */
public class UnlockStorageItems {

    private StorageRepository storageRepository;

    @Inject
    public UnlockStorageItems(StorageRepository storageRepository) {
        this.storageRepository = storageRepository;
    }

    public void execute(Integer facilityId, List<OrderFlowItem> orderFlowItems) {
        storageRepository.unlockOrderItemsFromStorage(facilityId, orderFlowItems);
    }
}

package mroweczka.e_zakupy.domain.storage;

import java.util.List;

import javax.inject.Inject;

import mroweczka.e_zakupy.dataAccess.repositories.storage.StorageRepository;
import mroweczka.e_zakupy.entity.orderFlowModels.OrderFlowItem;

/**
 * Klasa zablokowująca produkty w magazynie
 */
public class LockStorageItems {

    private StorageRepository storageRepository;

    @Inject
    public LockStorageItems(StorageRepository storageRepository) {
        this.storageRepository = storageRepository;
    }


    public boolean execute(int facilityNumber, List<OrderFlowItem> orderFlowItems) {
        return storageRepository.lockOrderItemsInStorage(facilityNumber, orderFlowItems);
    }
}

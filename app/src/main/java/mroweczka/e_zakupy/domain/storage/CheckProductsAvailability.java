package mroweczka.e_zakupy.domain.storage;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import mroweczka.e_zakupy.dataAccess.repositories.storage.StorageRepository;
import mroweczka.e_zakupy.entity.orderFlowModels.OrderFlowItem;

/**
 * Klasa służąca do sprawdzenia dostępności elementów zamówienia
 */
public class CheckProductsAvailability {
    private static final String TAG = CheckProductsAvailability.class.getSimpleName();

    private StorageRepository storageRepository;

    @Inject
    public CheckProductsAvailability(StorageRepository storageRepository) {
        this.storageRepository = storageRepository;
    }

    /**
     * Metoda służąca do stwierdzenia czy elementy zamówienia są dostępne
     *
     * @param facilityNumber placówka, dla której chcemy sprawdzić dostępność
     * @param orderItems     lista elementów zamówienia, które chcemy sprawdzić
     * @return czy elementy zamówienia są dostępne
     */
    public boolean execute(int facilityNumber, List<OrderFlowItem> orderFlowItems) {
        Map<Integer, Double> productsAvailability = storageRepository.getProductsAvailability(facilityNumber, orderFlowItems);

        return isFullProductsAvailability(orderFlowItems, productsAvailability);
    }

    private boolean isFullProductsAvailability(List<OrderFlowItem> orderFlowItems, Map<Integer, Double> productsAvailability) {
        for (OrderFlowItem orderItem : orderFlowItems) {
            Double productAvailability = productsAvailability.get(orderItem.getProductId());

            if (productAvailability == null || orderItem.getQuantity() > productAvailability) {
                return false;
            }
        }
        return true;
    }

}

package mroweczka.e_zakupy.domain.submitOrder;

import javax.inject.Inject;

import mroweczka.e_zakupy.dataAccess.repositories.order.OrderRepository;

/**
 * Klasa czyszcząca orderFlow
 */
public class ClearOrderFlow {

    private OrderRepository orderRepository;

    @Inject
    public ClearOrderFlow(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public void execute() {
        orderRepository.clearOrderFlow();
    }
}

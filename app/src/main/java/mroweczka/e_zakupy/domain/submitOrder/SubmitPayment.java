package mroweczka.e_zakupy.domain.submitOrder;


import javax.inject.Inject;

import mroweczka.e_zakupy.dataAccess.repositories.order.OrderRepository;
import mroweczka.e_zakupy.entity.Payment;

/**
 * Klasa zapisująca płatność na serwerze
 */
public class SubmitPayment {

    private OrderRepository orderRepository;

    @Inject
    public SubmitPayment(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public int execute(Payment payment) {
        return orderRepository.submitPayment(payment);
    }
}

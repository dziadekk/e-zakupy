package mroweczka.e_zakupy.domain.submitOrder;


import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import java.util.List;

import javax.inject.Inject;

import mroweczka.e_zakupy.domain.facility.GetFacility;
import mroweczka.e_zakupy.entity.Facility;
import mroweczka.e_zakupy.entity.WorkDay;

/**
 * Klasa sprawdzająca czy zamówienie ekspresowe jest dostępne
 */
public class IsExpressOrderAvailable {
    public static final int EXPRESS_ORDER_MINUTES_TO_FACILITY_CLOSE = 30;

    private GetFacility getFacility;

    @Inject
    public IsExpressOrderAvailable(GetFacility getFacility) {
        this.getFacility = getFacility;
    }

    public boolean execute(int facilityId) {
        Facility orderFacility = getFacility.execute(facilityId);

        WorkDay workDay = findWorkDay(orderFacility.getWorkDays(), LocalDate.now().getDayOfWeek());

        return workDay != null
                && LocalTime.now().plusMinutes(EXPRESS_ORDER_MINUTES_TO_FACILITY_CLOSE).isBefore(workDay.getCloseTime());
    }

    private WorkDay findWorkDay(List<WorkDay> workDays, int dayOfWeek) {
        for (WorkDay workDay : workDays) {
            if (workDay.getWeekDay().getValue() == dayOfWeek) {
                return workDay;
            }
        }
        return null;
    }
}

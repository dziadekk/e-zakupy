package mroweczka.e_zakupy.domain.submitOrder;

import javax.inject.Inject;

import mroweczka.e_zakupy.dataAccess.repositories.order.OrderRepository;
import mroweczka.e_zakupy.entity.orderFlowModels.OrderFlow;

/**
 * Klasa zapisująca tymczasowe zamówienie
 */
public class SaveOrderFlow {

    private OrderRepository orderRepository;

    @Inject
    public SaveOrderFlow(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public boolean execute(OrderFlow orderFlow) {
        return orderRepository.saveOrderFlow(orderFlow);
    }
}

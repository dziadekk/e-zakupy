package mroweczka.e_zakupy.domain.submitOrder;

import javax.inject.Inject;

import mroweczka.e_zakupy.dataAccess.repositories.order.OrderRepository;
import mroweczka.e_zakupy.entity.orderFlowModels.OrderFlow;

/**
 * Klasa pobierająca tymczasowe zamówienie
 */
public class GetOrderFlow {

    private OrderRepository orderRepository;

    @Inject
    public GetOrderFlow(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public OrderFlow execute() {
        return orderRepository.getOrderFlow();
    }
}

package mroweczka.e_zakupy.domain.submitOrder;


import javax.inject.Inject;

import mroweczka.e_zakupy.dataAccess.repositories.order.OrderRepository;

/**
 * Klasa pobierająca nastepne wolne id zamówienia
 */
public class GetNewOrderId {
    private OrderRepository orderRepository;

    @Inject
    public GetNewOrderId(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public int execute() {
        return orderRepository.getMaksId();
    }
}

package mroweczka.e_zakupy.domain.submitOrder;

import javax.inject.Inject;

import mroweczka.e_zakupy.dataAccess.repositories.order.OrderRepository;
import mroweczka.e_zakupy.entity.orderFlowModels.OrderFlowItem;

/**
 * Klasa zapisująca pozycje zamówienia
 */
public class SaveOrderFlowItem {

    private OrderRepository orderRepository;

    @Inject
    public SaveOrderFlowItem(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public boolean execute(OrderFlowItem orderFlowItem) {
        return orderRepository.saveOrderFlowItem(orderFlowItem);
    }
}

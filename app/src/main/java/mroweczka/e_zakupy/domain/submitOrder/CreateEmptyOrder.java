package mroweczka.e_zakupy.domain.submitOrder;

import javax.inject.Inject;

import mroweczka.e_zakupy.dataAccess.repositories.order.OrderRepository;


public class CreateEmptyOrder {

    private OrderRepository orderRepository;

    @Inject
    public CreateEmptyOrder(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public boolean execute(int id) {
        return orderRepository.createEmtyOrder(id);
    }
}

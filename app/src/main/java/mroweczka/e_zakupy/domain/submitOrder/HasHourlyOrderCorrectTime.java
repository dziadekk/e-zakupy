package mroweczka.e_zakupy.domain.submitOrder;


import android.support.annotation.Nullable;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import java.util.List;

import javax.inject.Inject;

import mroweczka.e_zakupy.domain.facility.GetFacility;
import mroweczka.e_zakupy.entity.Facility;
import mroweczka.e_zakupy.entity.WorkDay;
import mroweczka.e_zakupy.utils.DateUtils;

/**
 * Klasa sprawdzjąca poprawność zamówienia godzinowego
 */
public class HasHourlyOrderCorrectTime {
    public static final int HOURLY_ORDER_MINUTES_FROM_NOW = 30;
    public static final int HOURLY_ORDER_AVAILABILITY_OF_DAYS_INTO_FUTURE = 7;
    public static final String ERROR_7_DAYS = "Nie można złożyć zamówienia na więcej niż 7 dni do przodu";
    public static final String ERROR_PAST_DAY = "Nie można złożyć zamówienia w przeszłości";
    public static final String ERROR_30_MINUTES_FROM_NOW = "Zamówienie godzinowe musi być złożone na przynajmniej 30 minut przed datą odbioru";
    public static final String ERROR_WORKING_HOURS = "W dniu %s godziny otwarcia placówki to %s-%s";
    public static final String ERROR_FACILITY_NOT_OPEN = "W tym dniu placówka nie jest otwarta";

    private GetFacility getFacility;

    private Facility orderFacility;

    @Inject
    public HasHourlyOrderCorrectTime(GetFacility getFacility) {
        this.getFacility = getFacility;
    }

    @Nullable
    public String execute(int facilityId, LocalDate pickedDay, LocalTime pickedTime) {
        String error = null;
        if (orderFacility == null) {
            orderFacility = getFacility.execute(facilityId);
        }
        if (pickedDay.isAfter(LocalDate.now().plusDays(HOURLY_ORDER_AVAILABILITY_OF_DAYS_INTO_FUTURE))) {
            error = ERROR_7_DAYS;
        } else if (pickedDay.isBefore(LocalDate.now())) {
            error = ERROR_PAST_DAY;
        } else {
            WorkDay workDay = findWorkDay(orderFacility.getWorkDays(), pickedDay.getDayOfWeek());
            if (workDay == null) {
                error = ERROR_FACILITY_NOT_OPEN;
            } else if (pickedTime.isAfter(workDay.getOpenTime()) && pickedTime.isBefore(workDay.getCloseTime())) {
                if (LocalDate.now().isEqual(pickedDay) &&
                        pickedTime.isBefore(LocalTime.now().plusMinutes(HOURLY_ORDER_MINUTES_FROM_NOW))) {
                    error = ERROR_30_MINUTES_FROM_NOW;
                }
            } else {
                error = String.format(ERROR_WORKING_HOURS,
                        pickedDay.toString(DateUtils.COMMON_DATE_PATTERN),
                        workDay.getOpenTime().toString(DateUtils.COMMON_TIME_PATTERN),
                        workDay.getCloseTime().toString(DateUtils.COMMON_TIME_PATTERN)
                );
            }
        }

        return error;
    }

    @Nullable
    private WorkDay findWorkDay(List<WorkDay> workDays, int dayOfWeek) {
        for (WorkDay workDay : workDays) {
            if (workDay.getWeekDay().getValue() == dayOfWeek) {
                return workDay;
            }
        }
        return null;
    }
}

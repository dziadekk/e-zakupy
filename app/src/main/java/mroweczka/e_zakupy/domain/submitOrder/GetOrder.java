package mroweczka.e_zakupy.domain.submitOrder;

import javax.inject.Inject;

import mroweczka.e_zakupy.dataAccess.repositories.order.OrderRepository;
import mroweczka.e_zakupy.entity.Order;

/**
 * Klasa zwracająca zamówienie
 */
public class GetOrder {

    private OrderRepository orderRepository;

    @Inject
    public GetOrder(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public Order execute(int orderId) {
        return orderRepository.getOrder(orderId);
    }
}

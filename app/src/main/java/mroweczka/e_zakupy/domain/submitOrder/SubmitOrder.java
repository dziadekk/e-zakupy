package mroweczka.e_zakupy.domain.submitOrder;

import javax.inject.Inject;

import mroweczka.e_zakupy.dataAccess.repositories.order.OrderRepository;
import mroweczka.e_zakupy.entity.orderFlowModels.OrderFlow;

/**
 * Klasa zapisująca zamówienie na serwerze
 */
public class SubmitOrder {

    private OrderRepository orderRepository;

    @Inject
    public SubmitOrder(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public boolean execute(OrderFlow orderFlow) {
        return orderRepository.submitOrder(orderFlow);
    }
}

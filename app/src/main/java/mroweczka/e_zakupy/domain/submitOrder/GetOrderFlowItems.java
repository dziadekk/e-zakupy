package mroweczka.e_zakupy.domain.submitOrder;

import java.util.List;

import javax.inject.Inject;

import mroweczka.e_zakupy.dataAccess.repositories.order.OrderRepository;
import mroweczka.e_zakupy.entity.orderFlowModels.OrderFlowItem;

/**
 * Klasa zwracająca tymczasowe elementy zamówienia
 */
public class GetOrderFlowItems {

    private OrderRepository orderRepository;

    @Inject
    public GetOrderFlowItems(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public List<OrderFlowItem> execute() {
        return orderRepository.getOrderFlowItems();
    }
}

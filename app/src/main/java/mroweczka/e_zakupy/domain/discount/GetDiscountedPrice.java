package mroweczka.e_zakupy.domain.discount;


import java.util.List;

import javax.inject.Inject;

import mroweczka.e_zakupy.entity.Category;
import mroweczka.e_zakupy.entity.Discount;

/**
 * Klasa zwracjąca cenę po obniżce
 */
public class GetDiscountedPrice {
    private static final String TAG = GetDiscountedPrice.class.getSimpleName();
    public static final double HUNDRED_PERCENT = 100d;

    @Inject
    public GetDiscountedPrice() {
    }

    public double execute(List<mroweczka.e_zakupy.entity.orderFlowModels.OrderFlowItem> orderFlowItems, Discount discount, double oldPrice) {
        Category discountedCategory = discount.getDiscountedCategory();
        double newPrice = 0;
        double oldItemsPrice = 0;
        for (mroweczka.e_zakupy.entity.orderFlowModels.OrderFlowItem orderFlowItem : orderFlowItems) {
            double oldPriceTemp = orderFlowItem.getPrice() * orderFlowItem.getQuantity();
            oldItemsPrice += oldPriceTemp;
            Integer productCategoryId = orderFlowItem.getProductCategoryId();
            if (discountedCategory == null ||
                    (productCategoryId != null && discountedCategory != null && productCategoryId == discountedCategory.getCategoryId())) {
                newPrice += getDiscountedPriceOfItem(discount, oldPriceTemp);
            } else {
                newPrice += orderFlowItem.getPrice();
            }
        }
        return oldPrice - oldItemsPrice + newPrice;
    }


    private double getDiscountedPriceOfItem(Discount discount, double oldItemPrice) {
        return oldItemPrice * (HUNDRED_PERCENT - discount.getPercentOfDiscount()) / HUNDRED_PERCENT;
    }
}
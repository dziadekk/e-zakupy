package mroweczka.e_zakupy.domain.discount;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import mroweczka.e_zakupy.dataAccess.repositories.discount.DiscountRepository;
import mroweczka.e_zakupy.entity.Discount;
import mroweczka.e_zakupy.entity.orderFlowModels.OrderFlowItem;

/**
 * Klasa poberająca zniżki z serwera
 */
public class GetDiscounts {

    private DiscountRepository discountRepository;

    @Inject
    public GetDiscounts(DiscountRepository discountRepository) {
        this.discountRepository = discountRepository;
    }

    public List<Discount> execute(List<OrderFlowItem> orderFlowItems) {
        return filterAvailableDiscounts(orderFlowItems, discountRepository.getDiscountsForCustomer());
    }

    private List<Discount> filterAvailableDiscounts(List<OrderFlowItem> orderFlowItems, List<Discount> discounts) {
        Set<Integer> allCategoriesId = getAllCategories(orderFlowItems);
        List<Discount> filteredDiscounts = new ArrayList<>();
        for (Discount discount : discounts) {
            if (discount.getOrder() == null
                    && (discount.getDiscountedCategory() == null
                    || allCategoriesId.contains(discount.getDiscountedCategory().getCategoryId()))) {
                filteredDiscounts.add(discount);
            }
        }
        return discounts;
    }

    private Set<Integer> getAllCategories(List<OrderFlowItem> orderFlowItems) {
        Set<Integer> setOfCategories = new HashSet<>();
        for (OrderFlowItem orderFlowItem : orderFlowItems) {
            Integer productCategoryId = orderFlowItem.getProductCategoryId();
            if (productCategoryId != null) {
                setOfCategories.add(productCategoryId);
            }
        }
        return setOfCategories;
    }
}

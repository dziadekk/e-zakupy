package mroweczka.e_zakupy.domain.customer;

import java.util.List;

import javax.inject.Inject;

import mroweczka.e_zakupy.dataAccess.repositories.customer.CustomerRepository;


/**
 * Klasa pobierająca listę klientów
 */
public class GetCustomers {
    private static final String TAG = GetCustomers.class.getSimpleName();

    private CustomerRepository customerRepository;

    @Inject
    public GetCustomers(CustomerRepository facilityRepository) {
        this.customerRepository = facilityRepository;
    }

    /**
     * Metoda zwracająca listę klientów
     *
     * @return lista klientów
     */
    public List<Integer> execute() {
        return customerRepository.downloadCustomers();
    }

}

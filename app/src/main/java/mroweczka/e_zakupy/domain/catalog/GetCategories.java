package mroweczka.e_zakupy.domain.catalog;

import java.util.List;

import javax.inject.Inject;

import mroweczka.e_zakupy.dataAccess.repositories.catalog.CatalogRepository;
import mroweczka.e_zakupy.domain.customer.GetCustomers;
import mroweczka.e_zakupy.entity.Category;

/**
 * Klasa pobierająca kategorie z serwera
 */
public class GetCategories {
    private static final String TAG = GetCustomers.class.getSimpleName();

    private CatalogRepository catalogRepository;

    @Inject
    public GetCategories(CatalogRepository catalogRepository) {
        this.catalogRepository = catalogRepository;
    }

    /**
     * Metoda zwaracająca liste kategorii
     *
     * @return lista kateogrii
     */
    public List<Category> execute() {
        return catalogRepository.downloadCategories();
    }

}

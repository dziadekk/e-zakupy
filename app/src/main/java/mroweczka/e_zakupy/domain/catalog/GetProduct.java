package mroweczka.e_zakupy.domain.catalog;

import javax.inject.Inject;

import mroweczka.e_zakupy.dataAccess.repositories.catalog.CatalogRepository;
import mroweczka.e_zakupy.entity.Product;

/**
 * Klasa pobierająca product z serwera
 */
public class GetProduct {
    private CatalogRepository catalogRepository;

    @Inject
    public GetProduct(CatalogRepository catalogRepository) {
        this.catalogRepository = catalogRepository;
    }

    /**
     * Metoda zwaracająca wybrany produkt
     *
     * @param productId identyfikator produktu
     * @return produkt
     */
    public Product execute(int productId) {
        return catalogRepository.downloadProduct(productId);
    }
}

package mroweczka.e_zakupy.domain.catalog;

import java.util.List;

import javax.inject.Inject;

import mroweczka.e_zakupy.dataAccess.repositories.catalog.CatalogRepository;
import mroweczka.e_zakupy.domain.customer.GetCustomers;
import mroweczka.e_zakupy.entity.Product;

/**
 * Klasa pobierająca produkty danej kategorii
 */
public class GetProductsCategory {
    private static final String TAG = GetCustomers.class.getSimpleName();

    private CatalogRepository catalogRepository;

    @Inject
    public GetProductsCategory(CatalogRepository catalogRepository) {
        this.catalogRepository = catalogRepository;
    }

    /**
     * Metoda zwracjąca liste produktów podanej kateogrii
     *
     * @param categoryId identyfikator kategorii
     * @return lista produktów
     */
    public List<Product> execute(int categoryId) {
        return catalogRepository.downloadProductsCategory(categoryId);
    }
}

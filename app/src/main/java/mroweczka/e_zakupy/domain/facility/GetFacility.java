package mroweczka.e_zakupy.domain.facility;

import android.support.annotation.Nullable;

import javax.inject.Inject;

import mroweczka.e_zakupy.dataAccess.repositories.facility.FacilityRepository;
import mroweczka.e_zakupy.entity.Facility;

/**
 * Klasa pobierająca placówki
 */
public class GetFacility {
    private static final String TAG = GetFacility.class.getSimpleName();

    private FacilityRepository facilityRepository;

    @Inject
    public GetFacility(FacilityRepository facilityRepository) {
        this.facilityRepository = facilityRepository;
    }

    @Nullable
    public Facility execute(int facilityId) {
        return facilityRepository.getFacility(facilityId);
    }
}

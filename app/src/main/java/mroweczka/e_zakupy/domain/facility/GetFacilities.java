package mroweczka.e_zakupy.domain.facility;

import java.util.List;

import javax.inject.Inject;

import mroweczka.e_zakupy.dataAccess.repositories.facility.FacilityRepository;
import mroweczka.e_zakupy.entity.Facility;

/**
 * Klasa pobierająca listę placówek z serwera
 */
public class GetFacilities {
    private static final String TAG = GetFacilities.class.getSimpleName();

    private FacilityRepository facilityRepository;

    @Inject
    public GetFacilities(FacilityRepository facilityRepository) {
        this.facilityRepository = facilityRepository;
    }

    /**
     * Metoda zwracajaca listę placówek
     *
     * @return lista placówek
     */
    public List<Facility> execute() {
        return facilityRepository.downloadFacilities();
    }
}

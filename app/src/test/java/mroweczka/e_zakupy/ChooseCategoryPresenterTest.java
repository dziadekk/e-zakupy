package mroweczka.e_zakupy;


import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import mroweczka.e_zakupy.domain.catalog.GetCategories;
import mroweczka.e_zakupy.presentation.presenter.addProduct.ChooseCategoryPresenter;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ChooseCategoryPresenterTest {

    ChooseCategoryPresenter presenter;
    @Mock
    GetCategories getCategories;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        presenter = new ChooseCategoryPresenter(getCategories);

    }

    @Test
    public void checkThatListOfCategoryIsNotEmpty() {
        when(getCategories.execute()).thenReturn(null);
        Assert.assertEquals(presenter.getSavedCategories(), null);
    }
}

package mroweczka.e_zakupy;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import mroweczka.e_zakupy.domain.catalog.GetProduct;
import mroweczka.e_zakupy.domain.submitOrder.SaveOrderFlowItem;
import mroweczka.e_zakupy.entity.Country;
import mroweczka.e_zakupy.entity.Manufacturer;
import mroweczka.e_zakupy.entity.Product;
import mroweczka.e_zakupy.entity.PromotionalPrice;
import mroweczka.e_zakupy.presentation.presenter.addProduct.ProductDetailsPresenter;
import mroweczka.e_zakupy.utils.DateUtils;


@RunWith(PowerMockRunner.class)
public class ProductDetailsPresenterTest {
    private static final String PATTERN = "^[0-9]+(\\.[0-9]{1,2})?$[ ][z][ł]";
    ProductDetailsPresenter presenter;
    @Mock
    GetProduct getProduct;
    @Mock
    SaveOrderFlowItem saveOrderFlowItem;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        presenter = new ProductDetailsPresenter(getProduct, saveOrderFlowItem);
        List<PromotionalPrice> promotionalPrices = new ArrayList<PromotionalPrice>();
        promotionalPrices.add(new PromotionalPrice(1, DateUtils.getDateInCommonFormat("2015-10-22"), DateUtils.getDateInCommonFormat("2015-10-23"), 0, 3.00d));
        promotionalPrices.add(new PromotionalPrice(2, DateUtils.getDateInCommonFormat("2015-10-24"), DateUtils.getDateInCommonFormat("2018-10-22"), 0, 3.00d));
        promotionalPrices.add(new PromotionalPrice(3, DateUtils.getDateInCommonFormat("2019-10-22"), DateUtils.getDateInCommonFormat("2020-10-22"), 0, 3.00d));

        presenter.setProduct(new Product(1, "2345673543", "wczesne", "Jabłka Wczesne",
                "Świeżo zebrane jabłka prosto z posiadłości lorda Somersby", 1.20d, R.drawable.apple, Product.Unit.Kilogram,
                new Manufacturer(1, "Somersby"), new Country(1, "Polska"), null, null, promotionalPrices));
    }


    @Test
    public void checkThatUserCantTypeInvalidQuantity() {
        Assert.assertTrue(!presenter.checkAvability(10));
        Assert.assertTrue(!presenter.checkAvability(-1));
        Assert.assertTrue(!presenter.checkAvability(-5));
        Assert.assertTrue(!presenter.checkAvability(15));
    }

    @Test
    public void checkThatUserCanTypeValidQuantity() {
        Assert.assertTrue(presenter.checkAvability(1));
        Assert.assertTrue(presenter.checkAvability(2));
        Assert.assertTrue(presenter.checkAvability(8));
        Assert.assertTrue(presenter.checkAvability(7));
    }

    @Test
    public void checkThatUnitPriceWillBeTyped() {
        List<PromotionalPrice> promotionalPrices = new ArrayList<PromotionalPrice>();
        presenter.setProduct(new Product(1, "2345673543", "wczesne", "Jabłka Wczesne",
                "Świeżo zebrane jabłka prosto z posiadłości lorda Somersby", 1.20d, R.drawable.apple, Product.Unit.Kilogram,
                new Manufacturer(1, "Somersby"), new Country(1, "Polska"), null, null, promotionalPrices));
        Assert.assertEquals("1.20 zł", presenter.getProductPriceString());
    }

    @Test
    public void checkThatCountryIsDisplayed() {
        Assert.assertEquals("Polska", presenter.getProductCountry());
    }

    @Test
    public void checkThatManufacturerIsDisplayed() {
        Assert.assertEquals("Somersby", presenter.getProductManufacturer());
    }

    @Test
    public void checkThatGetProductPriceStringHaveValidFormat() {
        presenter.getProductPriceString().matches(PATTERN);
    }

    @Test
    public void checkThatAfterAddingQuantityPriceWillGoHigher() {

        Assert.assertEquals("6.00 zł", presenter.updatePrice(2));
    }

    @Test
    public void checkThatAfterAddingToMuchQuantityPriceWillGoHigher() {
        if (presenter.checkAvability(16)) {
            Assert.assertEquals("3.00 zł", presenter.updatePrice(16));
        }
    }


}















package mroweczka.e_zakupy.domain.submitOrder;

import junit.framework.Assert;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import mroweczka.e_zakupy.domain.facility.GetFacility;
import mroweczka.e_zakupy.entity.Facility;
import mroweczka.e_zakupy.entity.WorkDay;
import mroweczka.e_zakupy.utils.DateUtils;

import static mroweczka.e_zakupy.domain.submitOrder.HasHourlyOrderCorrectTime.ERROR_30_MINUTES_FROM_NOW;
import static mroweczka.e_zakupy.domain.submitOrder.HasHourlyOrderCorrectTime.ERROR_7_DAYS;
import static mroweczka.e_zakupy.domain.submitOrder.HasHourlyOrderCorrectTime.ERROR_FACILITY_NOT_OPEN;
import static mroweczka.e_zakupy.domain.submitOrder.HasHourlyOrderCorrectTime.ERROR_PAST_DAY;
import static mroweczka.e_zakupy.domain.submitOrder.HasHourlyOrderCorrectTime.ERROR_WORKING_HOURS;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest({LocalDate.class, LocalTime.class})
public class HasHourlyOrderCorrectTimeTest {
    private static final int FACILITY_ID = 1;

    private static final LocalDate MONDAY = DateUtils.getDateInCommonFormat("2017-01-16");
    private static final LocalTime MIDNIGHT = DateUtils.getTimeInCommonFormat("00:00");

    @Mock
    GetFacility getFacility;

    private HasHourlyOrderCorrectTime hasHourlyOrderCorrectTime;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        hasHourlyOrderCorrectTime = new HasHourlyOrderCorrectTime(getFacility);

        List<WorkDay> workDays = new ArrayList<>(6);
        workDays.add(new WorkDay(0, DateUtils.getTimeInCommonFormat("08:00"), DateUtils.getTimeInCommonFormat("22:00"), WorkDay.WeekDay.Monday));

        mockStatic(LocalDate.class);
        mockStatic(LocalTime.class);
        Facility facility = new Facility(0);
        facility.setWorkDays(workDays);

        when(getFacility.execute(FACILITY_ID)).thenReturn(facility);
    }

    @Test
    public void testCantMakeOrder7DaysIntoFuture() throws Exception {
        //given
        PowerMockito.when(LocalDate.now()).thenReturn(MONDAY);
        PowerMockito.when(LocalTime.now()).thenReturn(MIDNIGHT);
        //when
        String error = hasHourlyOrderCorrectTime.execute(FACILITY_ID,
                MONDAY.plusDays(8),
                null);
        //then
        Assert.assertEquals(error, ERROR_7_DAYS);
    }

    @Test
    public void testCantMakeOrderInPast() throws Exception {
        //given
        PowerMockito.when(LocalDate.now()).thenReturn(MONDAY);
        PowerMockito.when(LocalTime.now()).thenReturn(MIDNIGHT);
        //when
        String error = hasHourlyOrderCorrectTime.execute(FACILITY_ID,
                MONDAY.minusDays(1),
                null);
        //then
        Assert.assertEquals(error, ERROR_PAST_DAY);
    }

    @Test
    public void testFacilityNotOpenedInThatDay() throws Exception {
        //given
        PowerMockito.when(LocalDate.now()).thenReturn(MONDAY);
        PowerMockito.when(LocalTime.now()).thenReturn(MIDNIGHT);
        //when
        String error = hasHourlyOrderCorrectTime.execute(FACILITY_ID,
                MONDAY.plusDays(6),
                null);
        //then
        Assert.assertEquals(error, ERROR_FACILITY_NOT_OPEN);
    }

    @Test
    public void testError30MinutesFromNow() throws Exception {
        //given
        PowerMockito.when(LocalDate.now()).thenReturn(MONDAY);
        PowerMockito.when(LocalTime.now()).thenReturn(MIDNIGHT.plusHours(12));
        //when
        String error = hasHourlyOrderCorrectTime.execute(FACILITY_ID,
                MONDAY,
                MIDNIGHT.plusHours(12).plusMinutes(5));
        //then
        Assert.assertEquals(error, ERROR_30_MINUTES_FROM_NOW);
    }

    @Test
    public void testNotWithingWorkingHours() throws Exception {
        //given
        PowerMockito.when(LocalDate.now()).thenReturn(MONDAY);
        PowerMockito.when(LocalTime.now()).thenReturn(MIDNIGHT.plusHours(12));
        //when
        String error = hasHourlyOrderCorrectTime.execute(FACILITY_ID,
                MONDAY,
                MIDNIGHT.plusHours(24));
        //then
        Assert.assertEquals(error, String.format(ERROR_WORKING_HOURS,
                MONDAY.toString(DateUtils.COMMON_DATE_PATTERN),
                MIDNIGHT.plusHours(8).toString(DateUtils.COMMON_TIME_PATTERN),
                MIDNIGHT.plusHours(22).toString(DateUtils.COMMON_TIME_PATTERN)));
    }

    @Test
    public void testCorrect() throws Exception {
        //given
        PowerMockito.when(LocalDate.now()).thenReturn(MONDAY);
        PowerMockito.when(LocalTime.now()).thenReturn(MIDNIGHT.plusHours(10));
        //when
        String error = hasHourlyOrderCorrectTime.execute(FACILITY_ID,
                MONDAY,
                MIDNIGHT.plusHours(12));
        //then
        Assert.assertEquals(error, null);
    }


}
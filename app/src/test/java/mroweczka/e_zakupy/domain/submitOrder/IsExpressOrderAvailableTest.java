package mroweczka.e_zakupy.domain.submitOrder;

import junit.framework.Assert;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import mroweczka.e_zakupy.domain.facility.GetFacility;
import mroweczka.e_zakupy.entity.Facility;
import mroweczka.e_zakupy.entity.WorkDay;
import mroweczka.e_zakupy.utils.DateUtils;

import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest({LocalDate.class, LocalTime.class})
public class IsExpressOrderAvailableTest {

    private static final int FACILITY_ID = 1;

    private static final LocalDate MONDAY = DateUtils.getDateInCommonFormat("2017-01-16");
    private static final LocalTime MIDNIGHT = DateUtils.getTimeInCommonFormat("00:00");

    @Mock
    GetFacility getFacility;

    private IsExpressOrderAvailable isExpressOrderAvailable;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        isExpressOrderAvailable = new IsExpressOrderAvailable(getFacility);

        List<WorkDay> workDays = new ArrayList<>(6);
        workDays.add(new WorkDay(0, DateUtils.getTimeInCommonFormat("08:00"), DateUtils.getTimeInCommonFormat("22:00"), WorkDay.WeekDay.Monday));
        workDays.add(new WorkDay(1, DateUtils.getTimeInCommonFormat("08:00"), DateUtils.getTimeInCommonFormat("22:00"), WorkDay.WeekDay.Tuesday));
        workDays.add(new WorkDay(2, DateUtils.getTimeInCommonFormat("08:00"), DateUtils.getTimeInCommonFormat("22:00"), WorkDay.WeekDay.Wednesday));
        workDays.add(new WorkDay(3, DateUtils.getTimeInCommonFormat("08:00"), DateUtils.getTimeInCommonFormat("22:00"), WorkDay.WeekDay.Thursday));
        workDays.add(new WorkDay(4, DateUtils.getTimeInCommonFormat("08:00"), DateUtils.getTimeInCommonFormat("22:00"), WorkDay.WeekDay.Friday));
        workDays.add(new WorkDay(5, DateUtils.getTimeInCommonFormat("09:00"), DateUtils.getTimeInCommonFormat("21:00"), WorkDay.WeekDay.Saturday));

        mockStatic(LocalDate.class);
        mockStatic(LocalTime.class);
        Facility facility = new Facility(0);
        facility.setWorkDays(workDays);

        when(getFacility.execute(FACILITY_ID)).thenReturn(facility);
    }

    @Test
    public void testFacilityBeforeOpened() throws Exception {
        //given
        PowerMockito.when(LocalDate.now()).thenReturn(MONDAY);
        PowerMockito.when(LocalTime.now()).thenReturn(MIDNIGHT);
        //when
        boolean availability = isExpressOrderAvailable.execute(FACILITY_ID);
        //then
        Assert.assertTrue(availability);
    }

    @Test
    public void testFacilityOpened() throws Exception {
        //given
        PowerMockito.when(LocalDate.now()).thenReturn(MONDAY);
        PowerMockito.when(LocalTime.now()).thenReturn(MIDNIGHT.plusHours(12));
        //when
        boolean availability = isExpressOrderAvailable.execute(FACILITY_ID);
        //then
        Assert.assertTrue(availability);
    }

    @Test
    public void testFacilityClosed() throws Exception {
        //given
        PowerMockito.when(LocalDate.now()).thenReturn(MONDAY);
        PowerMockito.when(LocalTime.now()).thenReturn(MIDNIGHT.plusHours(23));
        //when
        boolean availability = isExpressOrderAvailable.execute(FACILITY_ID);
        //then
        Assert.assertFalse(availability);
    }

    @Test
    public void testFacilityClosingIn30minutes() throws Exception {
        //given
        PowerMockito.when(LocalDate.now()).thenReturn(MONDAY);
        PowerMockito.when(LocalTime.now())
                .thenReturn(MIDNIGHT
                        .plusHours(21)
                        .plusMinutes(45));
        //when
        boolean availability = isExpressOrderAvailable.execute(FACILITY_ID);
        //then
        Assert.assertFalse(availability);
    }

    @Test
    public void testFacilityNotOpenedAtAll() throws Exception {
        //given
        PowerMockito.when(LocalDate.now()).thenReturn(MONDAY.plusDays(6));
        PowerMockito.when(LocalTime.now()).thenReturn(MIDNIGHT.plusHours(12));
        //when
        boolean availability = isExpressOrderAvailable.execute(FACILITY_ID);
        //then
        Assert.assertFalse(availability);
    }

}